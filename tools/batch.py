#!/usr/bin/env python -o


import glob,os,sys
files_2be_run = sys.argv[1:] #produced by unix split command usually as filename.m5.XXX where XXX are either letters or numbers

modules = 'module load python py_packages\n'
paths = 'export PYTHONPATH=$PYTHONPATH:~/git_repos/pacmonstr/\n'
print files_2be_run
for readFile in files_2be_run:
	name = '%s_project.sh' % readFile.split('.')[0]
	#first, start PBS file by giving it a name
	PBS = open(name,'w')
	PBS.write('#!/bin/bash\n')
	PBS.write('\n')
	PBS.write('cd /scratch/pendlm02/chm1/\n')
	PBS.write(modules)
	PBS.write(paths)
	PBS.write('python ~/git_repos/pacmonstr/classes/PEG_pipeline.py %s\n' % readFile)
	PBS.close()
        command = 'qsub -N PACMonSTR_Chr20_CHM1 -S /bin/bash -q small_24hr -A scavenger -l nodes=1:ppn=1,mem=40gb,walltime=4:00:00 %s' % name
        print command
        os.system(command)


