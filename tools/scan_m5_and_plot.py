#!/usr/bin/env python -o

import os,sys
import trFinderFuncs as trf
#from matplotlib import pyplot as plt

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt 
import re
#from matplotlib.patches import Rectangle
import matplotlib.cm as cm
from classes.model.Sequence import revcomp as rc
import numpy as np

fileIn = os.path.abspath(sys.argv[1])
start = int(sys.argv[2])
finish = int(sys.argv[3])

def compressMatrix (m, scale=10):
    s = m.shape
    newM = np.zeros((s[0]/scale, s[1]/scale))
    s2 = newM.shape
    for i in range (s2[0]):
        for j in range (s2[1]):
            newM[i][j] = np.max(m[i*scale:i*scale+scale,j*scale:j*scale+scale])
    return newM


###Custom matplotlib cmap

cdict = {'green': ((0.0, 1.0, 1.0),
                 (0.5, 0.0, 0.0),
                 (1.0, 0.0, 0.0)),
         'red': ((0.0, 1.0, 1.0),
                   (0.5, 1.0, 1.0),
                   (1.0, 0.0, 0.0)),
         'blue': ((0.0, 1.0, 1.0),
                  (0.5, 0.0, 0.0),
                  (1.0, 1.0, 1.0))}
my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)

####
#make new folder for all images for desired event
chromosome = re.search('chr[0123456789X]+',fileIn).group()
newFolder = '%s_%s_%s' % (chromosome,start,finish)
try:
    os.mkdir(newFolder)
except OSError:
    print "folder already exists. Adding images to existing folder."
os.chdir(newFolder)

for line in open(fileIn):
    sys.stderr.write('\rread: %s' % (line.split()[0]))
    m5Fields = ['qname','qseqlength','qstart','qend','qstrand','tname','tseqlength','tstart','tend','tstrand','score','nMatch','nMismatch','nIns','nDel','MapQV','seq1','bars','seq2']
    lineSplit = line.split()
    entry = dict(zip(m5Fields,lineSplit))
    if len(entry) != 19:
        continue
    entry['qname'] = entry['qname'].replace('/','-')
    entry['tstart'] = int(entry['tstart'])
    entry['tend'] = int(entry['tend'])
    entry['qstart'] = int(entry['qstart'])
    entry['qend'] = int(entry['qend'])
    seq1 = entry['seq1']
    seq2 = entry['seq2']
    if start - 200 > entry['tstart'] and finish + 200 < entry['tend']: #test whether this read covers the reference area in question?
        print " Found a read!"
        if entry['tstrand']=='-': #must traverse the reference from start to indicated position. Plotting will be done -/+ if tstrand = -
            seq1 = rc(seq1)
            seq2 = rc(seq2)
            print "Negative qstrand. Flipping..."
            entry['qend'],entry['qstart'] = entry['qstart'] , entry['qend']
#            entry['tend'],entry['tstart'] = entry['tstart'] , entry['tend']
        print " First, plotting full read..."
        strippedQuery = seq1.replace('-','')
        strippedRef = seq2.replace('-','')
        #for_1_vs_for_2
        matrix1a = trf.CompareSeqPairRef(strippedQuery, strippedRef, len(strippedQuery), len(strippedRef),20,1,10,2)
        #rc_1_vs_rc_2
        matrix1b = trf.CompareSeqPairRef(rc(strippedQuery), rc(strippedRef), len(strippedQuery), len(strippedRef),20,1,10,2)[::-1,::-1]
        matrix1 = np.minimum(matrix1a,matrix1b)>0 
        #for_1_vs_for_2
        matrix2a = trf.CompareSeqPairRef(rc(strippedQuery), strippedRef, len(strippedQuery), len(strippedRef),20,1,10,2)
        #rc_1_vs_rc_2
        matrix2b = trf.CompareSeqPairRef(strippedQuery, rc(strippedRef), len(strippedQuery), len(strippedRef),20,1,10,2)[::-1,::-1]
        matrix2 = 0.5 * (np.minimum(matrix2a,matrix2b)[::-1]>0)
        matrix3 = matrix1 + matrix2
        plt.clf()
        plt.imshow(compressMatrix(matrix3,5), cmap = my_cmap,extent=[entry['tstart'],entry['tend'],entry['qend'],entry['qstart']],interpolation="None" )
        plt.colorbar()
#        plt.show()
#        for_1_vs_for_2 = trf.CompareSeqPairRef(fullseq1, fullseq2, len(fullseq1), len(fullseq2),10,2,10,2)
#        rc_1_vs_rc_2 = trf.CompareSeqPairRef(rc(fullseq1), rc(fullseq2), len(fullseq1), len(fullseq2),10,2,10,2)[::-1,::-1]
#        matrix = (for_1_vs_for_2 + rc_1_vs_rc_2)/2.0
#        plt.imshow(compressMatrix(matrix,5)>0, cmap = cm.binary,interpolation='nearest', extent=[entry['tstart'],entry['tend'],entry['qend'],entry['qstart']] )
#        plt.imshow(np.array(np.array(matrix,dtype=bool),dtype='i'), cmap = cm.hot,interpolation='bicubic', extent=[entry['tstart'],entry['tend'],entry['qend'],entry['qstart']] )
        plt.savefig('%s_%s_%i-%i_full.png' % (entry['qname'], entry['tname'],entry['tstart'],entry['tend']),dpi = 1900)
        plt.clf()
#################parsing and plotting details of TR
        print "Now plotting details."
        qbases = 0
        tbases = 0
 #       fseq1 = ''
 #       fseq2 = ''
        qITDstart = 0
        qITDend = 0
        tITDstart = 0
        tITDend = 0
        for position in range(len(seq1)):#because they are aligned, len(seq1)==len(seq2), even if the ungapped seqs aren't
            if tbases + entry['tstart'] < start -200: #traverse the alignment until you hit the indicated position in the ungapped target sequence.
                if seq1[position] != '-':
                    qbases += 1
                if seq2[position] != '-':
                    tbases += 1
                continue
            if tbases + entry['tstart'] > finish + 200: #traverse the alignment until you hit the end of the indicated substring in the ungapped sequence.
                qITDend = qbases
                tITDend = tbases                
                break
            if seq2[position] != '-': #if a position in tseq is not a gap, count it 
#                fseq2 += seq2[position]
                if tITDstart == 0 :
                    tITDstart = tbases
                tbases += 1
            if seq1[position] != '-': #if a position in qseq is not a gap, don't count it 
#                fseq1 += seq1[position]
                if qITDstart == 0 :
                    qITDstart = qbases
                qbases += 1
#        if entry['tstrand'] == '-': #flip so that negative strand queries are numbered in reverse
#            qITDstart, qITDend = qITDend, qITDstart 
#            tITDstart, tITDend = tITDend, tITDstart
        print "query substring:",qITDstart, qITDend 
        print "target substring:",tITDstart, tITDend,'->', entry['tstart'] + tITDstart , entry['tstart'] + tITDend
        plt.imshow(matrix3[qITDstart:qITDend,tITDstart:tITDend], cmap = my_cmap, interpolation='None',extent=[entry['tstart'] + tITDstart, entry['tstart'] + tITDend,qITDend,qITDstart])
        plt.colorbar()
#        plt.show()
#        for_1_vs_for_2 = trf.CompareSeqPairRef(fseq1, fseq2, len(fseq1), len(fseq2),10,2,10,2)
#        rc_1_vs_rc_2 = trf.CompareSeqPairRef(rc(fseq1), rc(fseq2), len(fseq1), len(fseq2),10,2,10,2)[::-1,::-1]
#        matrix = (for_1_vs_for_2 + rc_1_vs_rc_2)/2.0
#        plt.imshow(matrix, cmap = cm.binary, interpolation='bicubic',extent=[entry['tstart'] + tITDstart, entry['tstart'] + tITDend,qITDend, qITDstart])
        plt.savefig('%s_%s_%i-%i_ITDdetail.png' % (entry['qname'], entry['tname'],entry['tstart'],entry['tend']),dpi=1000)
        #plt.show()

os.chdir('..')
            
