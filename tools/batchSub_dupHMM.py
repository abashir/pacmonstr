#!/usr/bin/env python -o

import glob,os,sys,subprocess,shlex,time
import glob

modules = 'module load gcc python py_packages\n'
paths = 'export PYTHONPATH=$PYTHONPATH:~/git_repos/pacmonstr/\n'
#paths += 'export PYTHONPATH=$PYTHONPATH:/hpc/users/pendlm02/git_repos/pacmonstr/cython/\n'
#paths += 'export PYTHONPATH=$PYTHONPATH:/hpc/users/pendlm02/git_repos/rrna/scripts/\n'
#^These should be taken care of by new, careful folder naming and proper addition of __init__.py

#chromosome = str(sys.argv[1])
files = sys.argv[1:]
#jobTotal = len(files)
jobsCount = 1

for readFile in files:
#	print readFile
#	print  readFile.split('/')[2].split('_')
	chromosome, maxGB, count = readFile.split('/')[2].split('_')
#	chromosome = readFile.split('/')[1]
#	cmd = 'bjobs -u pendlm02 | grep \'PEND\' -c '
#	cmd = shlex.split(cmd)
#	stdout,stderr = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True).communicate()
#	jobCount = len(stdout.strip().split('\n'))
#	sys.stderr.write('\r%i jobs in queue' % jobCount)
#	while jobCount > 20000:
#		time.sleep(30) #if too many jobs in queue, wait 5 minutes, then submit the next job
#		cmd = 'qselect -u pendlm02 -s Q'
#		cmd = shlex.split(cmd)
#		stdout,stderr = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
#		jobCount = len(stdout.split('\n'))
#		sys.stderr.write('\r%i jobs in queue' % jobCount)
	mem = (1000 * (int(maxGB) + 4))
	name = '%s_%iGB_%s_project.sh' % (chromosome,mem/1000, jobsCount)
	#first, start LSF file by giving it a name
	LSF = open(name,'w')
	fullFN = readFile.replace('./',os.getcwd()+'/')
	directory = '/'.join(fullFN.split('/')[:-1])
	LSF.write('#!/bin/bash -l \n')# name job
	LSF.write('#BSUB -L /bin/bash \n')# name job
	LSF.write('#BSUB -J PACMonSTR_%s_na12878\n' % chromosome)# name job
	LSF.write('#BSUB -q scavenger\n')# indicate queue
	LSF.write('#BSUB -W 5:00\n')# memory
	LSF.write('#BSUB -n 1\n')# number of cores
	LSF.write('#BSUB -M %i\n'% (mem*1000))
	LSF.write('#BSUB -R "rusage[mem=%i]"\n' % mem)
	LSF.write('#BSUB -o PACMonSTR_%i_%s_%s_na12878.out\n' % (jobsCount,chromosome,readFile.split('.')[-1]))# stdout
	LSF.write('#BSUB -e PACMonSTR_%i_%s_%s_na12878.err\n' % (jobsCount,chromosome,readFile.split('.')[-1]))# stderr
	
	LSF.write('\n')
	LSF.write('cd %s\n' % directory)
	#determine whether a ./read/folder has been made for this chromosome. If not, make one
	if not os.path.exists('%s/HMMreads' % directory):
		LSF.write('mkdir HMMreads\n')
	LSF.write(modules)
	LSF.write(paths)
	LSF.write('python ~/git_repos/pacmonstr/classes/PEG_pipeline_dupHMM.py %s ./HMMreads/ %s\n' % (chromosome , fullFN))
	LSF.close()
	
	command = 'bsub < %s' % name
#	print command, 'job',jobsCount,'of',jobTotal
	jobsCount += 1
	os.system(command)



