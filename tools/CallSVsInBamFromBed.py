import os
import sys
from pbcore.io.FastaIO import FastaReader
from classes.ProjectEventGeneral import EventDetector
import pysam 
from multiprocessing import Pool

event_type = sys.argv[1]
match_len = int(sys.argv[2])
mismatch_tol = int(sys.argv[3])
poolsize = int(sys.argv[4])
covcut = sys.argv[5]
fafn  = sys.argv[6]
regionfn = sys.argv[7] #bed of locations of interest
bamfns = sys.argv[8:]

#Deletion,Insertion, Inversion,Duplication
allowed_types = ["D", "I", "V", "T", "DO"]
if not event_type in allowed_types:
    print >>sys.stderr, "invalid event type %s - must be one of %s" %(event_type, allowed_types)
    raise 

def makeDotPlot (seqtup, chrom, ev_start, ev_end, seqDict, fout=sys.stdout):
    global event_type
    global match_len
    global mismatch_tol
    seqname, seq, start, end, strand, qs, qe = seqtup
    refSeq = seqDict[chrom][start:end].upper()
    #print >>sys.stderr, "%s at: %s:%i-%i (%i bases)" %(seqname, chrom, start, end, (end-start))
    ed = EventDetector(seq, refSeq, window=match_len, mismatch=mismatch_tol)
    if event_type == "I":
        states = ed.InsertionPredict ( hmmFlag = True)
        intervals = readStatesToIntervals (states)
        for interval in intervals:
            istart, iend = interval[0], interval[1]
            if (qs < iend and qe > istart):
                interval_len = interval[1]-interval[0]
                print >>sys.stderr, "FOUND!!!! %s at: %s:%i-%i (%i bases), with query indices: %i %i" %(seqname, chrom, start, end, (end-start), istart, iend)
                print >>fout, chrom, ev_start, ev_end, start, end, qs, qe, seqname, " ".join(map(str, interval)), interval_len

    elif event_type == "D":
        states = ed.DeletionPredict ( hmmFlag = True)
        intervals = readStatesToIntervals (states)
        e_start = ev_start - start
        e_end = ev_end - start
        detected = False
        for interval in intervals:
            istart, iend = interval[0], interval[1]
            istart += start
            iend += start
            # overlap between our call and event prediction
            overlap = min(iend, ev_end)-max(istart, ev_start)            
            #if (e_start < iend and e_end > istart):
            if (ev_start < iend and ev_end > istart):
                detected = True
                interval_len = interval[1]-interval[0]
                print >>sys.stderr, "FOUND!!!! %s at: %s:%i-%i (%i bases), with query indices: %i %i" %(seqname, chrom, start, end, (end-start), istart, iend)
                #print >>fout, chrom, ev_start, ev_end, start, end, qs, qe, seqname, " ".join(map(str, interval)), interval_len
                # oerlap with original call
                overlap_frac1 = float(overlap)/(ev_end-ev_start)
                # oerlap with our call
                overlap_frac2 = float(overlap)/(iend-istart)
                print >>fout, chrom, ev_start, ev_end, start, end, qs, qe, seqname, istart, iend, interval_len, overlap_frac1, overlap_frac2
        
            fout.flush()
        if False:#detected:
            print >>sys.stderr, "building sequnce projection image"
            ed.buildSeqPairProjection (ed.querySeq, ed.refSeq, "%s_%i_%i_%s" %(chrom, ev_start, ev_end, seqname))
            print >>sys.stderr, "finished building sequence projection image"

    elif event_type == "DO":
        states = ed.DeletionPredict ( hmmFlag = True)
        intervals = readStatesToIntervals (states)
        e_start = ev_start - start
        e_end = ev_end - start
        for interval in intervals:
            istart, iend = interval[0], interval[1]
            if (e_start < iend and e_end > istart):
                interval_len = interval[1]-interval[0]
                print >>sys.stderr, "FOUND!!!! %s at: %s:%i-%i (%i bases), with query indices: %i %i" %(seqname, chrom, start, end, (end-start), istart, iend)
                print >>fout, chrom, ev_start, ev_end, start, end, qs, qe, seqname, " ".join(map(str, interval)), interval_len
            fout.flush()

    elif event_type == "T":
        states = ed.DuplicationPredict ( hmmFlag = True)
        intervals = readStatesToIntervals (states)
        e_start = ev_start - start
        e_end = ev_end - start
        for interval in intervals:
            istart, iend = interval[0], interval[1]
            if (e_start < iend and e_end > istart):
                interval_len = interval[1]-interval[0]
                print >>sys.stderr, "FOUND!!!! %s at: %s:%i-%i (%i bases), with query indices: %i %i" %(seqname, chrom, start, end, (end-start), istart, iend)
                print >>fout, chrom, ev_start, ev_end, start, end, qs, qe, seqname, " ".join(map(str, interval)), interval_len
            fout.flush()

    elif event_type == "V":
        states = ed.InversionPredict ( hmmFlag = True)
        intervals = readStatesToIntervals (states)
        e_start = ev_start - start
        e_end = ev_end - start
        for interval in intervals:
            istart, iend = interval[0], interval[1]
            if (e_start < iend and e_end > istart):
                interval_len = interval[1]-interval[0]
                print >>sys.stderr, "FOUND!!!! %s at: %s:%i-%i (%i bases), with query indices: %i %i" %(seqname, chrom, start, end, (end-start), istart, iend)
                print >>fout, chrom, ev_start, ev_end, start, end, qs, qe, seqname, " ".join(map(str, interval)), interval_len
            fout.flush()

def makeDotPlotForPooling (seqtup_str):
    global seqDict
    seqname, seq, start, end, strand, qs, qe = seqtup_str.split("_:_")
    refSeq = seqDict[chrom][start:end].upper()
    print >>sys.stderr, "%s at: %s:%i-%i (%i bases)" %(seqname, chrom, start, end, (end-start))
    ed = EventDetector(seq, refSeq)
    states = ed.InsertionPredict ( hmmFlag = True)
    intervals = readStatesToIntervals (states)
    for interval in intervals:
        istart, iend = interval[0], interval[1]
        if (qs < iend and qe > istart):
            interval_len = interval[1]-interval[0]
            print chrom, ev_start, ev_end, start, end, seqname, " ".join(map(str, interval)), interval_len
    

def readStatesToIntervals (states):
    oneBlock = False
    eventEnd = -1
    eventStart = -1
    intervals = []
    for i in range (len(states)):
        s = states[i]
        if s == 1:
           if not oneBlock:
               eventStart = i
               oneBlock = True
           eventEnd = i
        elif s == 0 and oneBlock:
            oneBlock = False
            if eventStart != 0:
                intervals.append((eventStart, eventEnd))
    return intervals

def getCoordinates (ar, spos, epos, flank=200):
    spos, epos = spos-flank, epos+flank
    q_spos = -1
    q_epos = -1
    for ap in ar.aligned_pairs:
        qpos, rpos = ap
        if rpos == spos:
            q_spos = qpos
        if rpos == epos:
            q_epos = qpos
    return q_spos, q_epos
    

def readBamToSeqPosTuples (reads, estart, eend):
    seqPosTuples = []
    #for bamIter in bamIters:
    for ar in reads:
        #for ar in bamIter: 
        if not ar.is_secondary:
            seq = ar.seq 
            strand = "+"
            if ar.is_reverse:
                strand = "-"
            aend = ar.aend
            astart = aend-ar.alen
            q_spos, q_epos = getCoordinates (ar, estart, eend)
            seqPosTuples.append((ar.qname, seq[ar.qstart:ar.qend], astart, aend, strand, q_spos, q_epos))            
    return seqPosTuples

def readBedToRegions (bedfn):
    regions = []
    with open(bedfn) as f:
        for l in f:
            ll = l.strip().split()
            regions.append((ll[0], int(ll[1]), int(ll[2])))
    return regions


def iterateThroughRegions (regions):
    global samfiles
    global fastaDict
    # iterate through regions
    for region in regions:
        chrom, start, end = region
    #bamIters = []
        reads = []
        for samfile in samfiles:
            tempreads = samfile.fetch(chrom, start-1, start)
        #tempbam = pysam.Samfile("temp.bam", "wb", template=samfile)
            for read in tempreads:
        #    tempbam.write(read)
                if read.aend > end:
                    reads.append(read)
        #tempbam.close()
        #os.system("samtools index temp.bam")
        #tempsam = pysam.Samfile( "temp.bam" , "rb" )
        #bamIter = tempsam.fetch(chrom, end, end+1)
        
        #bamIters.append(bamIter)
    #seqTuples = readBamToSeqPosTuples (bamIters)    
        seqTuples = readBamToSeqPosTuples (reads, start, end)    
        print >>sys.stderr, "at region %s, %i reads observed" %(region, len(seqTuples))
        for seqtup in seqTuples:
            makeDotPlot(seqtup, chrom, start, end, fastaDict)

def iterateThroughRegion (region):
    global samfiles
    global fastaDict
    global covcut
    flank = 200
    # iterate through regions
    if True:
        chrom, start, end = region.split("_")
        start, end = int(start), int(end)
        reads = []
        for samfile in samfiles:
            tempreads = samfile.fetch(chrom, start-flank, start)
            for read in tempreads:
                if read.aend > end+flank:
                    reads.append(read)

        seqTuples = readBamToSeqPosTuples (reads, start, end)    
        print >>sys.stderr, "at region %s, %i reads observed" %(region, len(seqTuples))
        fout = open("temp_%s_%i_%i.txt" %(chrom, start, end), 'w')
        ferr = open("temp_%s_%i_%i.err" %(chrom, start, end), 'w')
        ferr.write("%s %i\n" %(region, len(seqTuples)))
        if len(seqTuples) < covcut:
            for seqtup in seqTuples:
                makeDotPlot(seqtup, chrom, start, end, fastaDict, fout)
        else:
            ferr.write("Too many sequences for region %s: %i\n" %(region, len(seqTuples)))
        fout.close()
        ferr.close()

        
# read regions
regions = readBedToRegions (regionfn)
print >>sys.stderr, "read regions"
sys.stderr.flush()

# read fasta to dict
fastaDict = {}
for entry in FastaReader(fafn):
    print >>sys.stderr, entry.name
    fastaDict[entry.name.split(" ")[0]] = entry.sequence
print >>sys.stderr, "read seq to dict"
sys.stderr.flush()

# load sam files
samfiles = []
for bamfn in bamfns:
    samfile = pysam.Samfile( bamfn, "rb" )
    samfiles.append(samfile)
print >>sys.stderr, "loaded %i samfiles" %(len(samfiles))
sys.stderr.flush()


#iterateThroughRegions (regions)
myPool = Pool(poolsize)
regionstrs = map(lambda x: "_".join(map(str, x)), regions)
myPool.map(iterateThroughRegion, regionstrs)
#for regionstr in regionstrs:
#    iterateThroughRegion (regionstr)

