#!/usr/bin/env python -o

import os,sys,re, glob
from collections import Counter
import math as m

#each will have a memsize sum of 15e9
#upperLims = [40,20,15,13,11,9,7,6,5,2]

#a file containing reads, all from the same chromosome
chromosome = re.search('chr(M|X|[0-9]+)',' '.join(map(str,sys.argv[1:]))).group()
start = int(sys.argv[2])
stop = int(sys.argv[3])
longestRead = float(sys.argv[4])
fragments = int( (stop-start) / (2. * longestRead))

refSizes = '''chr1 249250621
chr2 243199373
chr3 198022430
chr4 191154276
chr5 180915260
chr6 171115067
chr7 159138663
chr8 146364022
chr9 141213431
chr10 135534747
chr11 135006516
chr12 133851895
chr13 115169878
chr14 107349540
chr15 102531392
chr16 90354753
chr17 81195210
chr18 78077248
chr19 59128983
chr20 63025520
chr21 48129895
chr22 51304566
chrX 155270560
chrY 59373566
chrmtDNA 16569'''.split('\n')

for line in refSizes:
    lineSplit = line.split()
    if chromosome == lineSplit[0]:
        maxChrLen = int(lineSplit[1])

print start, maxChrLen

if stop > maxChrLen:
	print 'using %i as max chromosome length for %s instead of %i' % (maxChrLen, chromosome,stop)
	start = maxChrLen

jobs = [ (x * (stop-start)/fragments,((1+x) * (stop-start)/fragments)) for x in range(fragments)]
jobsOut = [open('%s_%i_%i.m5' % (chromosome, x, x % 2),'w') for x in range(len(jobs))]

###fileMemSizes = [0 for x in range(10)]
###filesOutCounters = [0 for x in range(10)]
#filesOut = [open('%s_%i_%i.m5' % (chromosome, upperLims[x],filesOutCounters[x]),'w') for x in range(10)]
###filesOut = [0 for x in range(10)]

m5Fields = ['qname','qseqlength','qstart','qend','qstrand','tname','tseqlength','tstart','tend','tstrand','score','nMatch','nMismatch','nIns','nDel','MapQV','seq1','bars','seq2']

#approximate size of required numpy arrays for dotplot in GB

#NB - using the O(m n) as an indicator for mem|time complexity is good for the inexact matching method in ProjectEventGeneral.py, but a poor indicator for ProjectEventExact.py.
#PEE's time|mem complexity correlates (i think) instead to the product of the kolmagorov complexity of m and n. Sequences in alpha satellites that are highly repetitive will have a reference
#   sequence dict that contains many many keys per sequence, whereas a pair of completely non-repetitive sequences would approach linear time evaluation. At some later point,
#   this can be taken into account when splitting sequences.
#def size(m,n):
#	return 5*(16*m*n) / 10**9

print jobs

with open(sys.argv[1]) as f:
	for line in f:
		lineSplit = line.strip().split()
		entry = dict(zip(m5Fields,lineSplit))
		if len(entry) != 19:
			continue
		entry['qstart'] = int(entry['qstart'])
		entry['qend'] = int(entry['qend'])
		entry['tstart'] = int(entry['tstart'])
		entry['tend'] = int(entry['tend'])
		qlen = entry['qend'] - entry['qstart']
		tlen = entry['tend'] - entry['tstart']
		for num, x in enumerate(jobs):
			if x[0]<entry['tstart'] <= x[1]:
				jobsOut[num].write('%s\n' % line.strip())
				break

for fn in glob.iglob('chr*_*_*.m5'):
	if not os.path.getsize(fn):
		os.remove(fn)
