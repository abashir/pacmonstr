#!/usr/bin/env python -o

import os,sys
import numpy as np

fileIn = sys.argv[1]
start = int(sys.argv[2])
finish = int(sys.argv[3])

for line in open(fileIn):
    m5Fields = ['qname','qseqlength','qstart','qend','qstrand','tname','tseqlength','tstart','tend','tstrand','score','nMatch','nMismatch','nIns','nDel','MapQV','seq1','bars','seq2']
    lineSplit = line.split()
    entry = dict(zip(m5Fields,lineSplit))
    if len(entry) != 19:
        continue
    entry['qname'] = entry['qname'].replace('/','-')
    entry['tstart'] = int(entry['tstart'])
    entry['tend'] = int(entry['tend'])
    entry['qstart'] = int(entry['qstart'])
    entry['qend'] = int(entry['qend'])
    seq1 = entry['seq1']
    seq2 = entry['seq2']
    if start - 200 > entry['tstart'] and finish + 200 < entry['tend']: #test whether this read covers the reference area in question?
        print line
            
