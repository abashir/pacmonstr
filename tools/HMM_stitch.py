#!/usr/bin/env python -o

import sys, os
from collections import Counter
import numpy as np

files = sys.argv[2:]
maxChrLen = 255000000
DUP = np.zeros(maxChrLen,dtype='uint16')
INV = np.zeros(maxChrLen,dtype='uint16')
DEL = np.zeros(maxChrLen,dtype='uint16')
COV = np.zeros(maxChrLen,dtype='uint16')

for filename in files:
    print "incorporating %s" % filename
    fileIn = open(filename)
    for line in fileIn:
        if ' 0 0 0 0' in line:
            continue
        lineSplit = line.split()
        try:
            pos = int(lineSplit[0].split(':')[1])
            DUP[pos]+= int(lineSplit[1]) # HMM score stack
            INV[pos]+= int(lineSplit[2]) # HMM score stack
            DEL[pos]+= int(lineSplit[3]) # HMM score stack
            COV[pos]+= int(lineSplit[4]) # coverage count
        except IndexError:
            sys.stderr.write("Line was not parsed: %s" % line)
	except ValueError:
            sys.stderr.write("Line was not parsed: %s" % line)
    fileIn.close()

fileOut = open(sys.argv[1],'w')
print "writing to %s..." % sys.argv[1]
chromosome = lineSplit[0].split(':')[0]
for pos in xrange(maxChrLen):
    if COV[pos] == 0:
        continue
    fileOut.write('%s:%i %i %i\n' % ( chromosome, pos, DUP[pos],INV[pos],DEL[pos], COV[pos]))

fileOut.close()
