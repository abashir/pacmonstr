#!/usr/bin/env python -o

import sys, os
from collections import Counter

fileOut = open(sys.argv[1],'w')
files = sys.argv[2:]

positions = {}

for filename in files:
    if 'EOF' not in os.popen('tail -n 1 %s' % filename).read():
        DBZ = 1
    else:
        DBZ = 0
    print "incorporating %s" % filename
    fileIn = open(filename)
    for line in fileIn:
        if 'EOF' in line:
            continue
        lineSplit = line.split()
#        if int(lineSplit[0].split(':')[1]) > 62435965: #length of chromosome 20
#            continue
        if lineSplit[0] not in positions:
            positions[lineSplit[0]] = [0,0,0]
        positions[lineSplit[0]][0]+= int(lineSplit[1]) # queries v. refs projection raw counts
        positions[lineSplit[0]][1]+= int(lineSplit[2]) # ref v. ref projection raw counts
        positions[lineSplit[0]][2]+= int(lineSplit[3]) # coverage count
    fileIn.close()

sortedData = positions.items()
sortedData.sort(key=lambda x: int(x[0].split(':')[1]))

for pos, vals in sortedData:
    fileOut.write('%s %i %i %i\n' % ( pos, vals[0], DBZ + vals[1], vals[2]))#pseudocount of 1 to prevent DBZ.

fileOut.close()
