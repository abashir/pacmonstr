#!/usr/bin/env python -o

import sys, os, time,re
from collections import Counter
import numpy as np
import subprocess
files = sys.argv[2:]
chromosome = re.search('chr(M|X|[0-9]+)',' '.join(map(str,sys.argv[1:]))).group()
print chromosome
refSizes = '''chr1 249250621
chr2 243199373
chr3 198022430
chr4 191154276
chr5 180915260
chr6 171115067
chr7 159138663
chr8 146364022
chr9 141213431
chr10 135534747
chr11 135006516
chr12 133851895
chr13 115169878
chr14 107349540
chr15 102531392
chr16 90354753
chr17 81195210
chr18 78077248
chr19 59128983
chr20 63025520
chr21 48129895
chr22 51304566
chrX 155270560
chrY 59373566
chrmtDNA 16569'''.split('\n')

for line in refSizes:
    lineSplit = line.split()
    if chromosome == lineSplit[0]:
        maxChrLen = int(lineSplit[1])

if len(files) == 1:
    cmd = 'cp %s %s' % (sys.argv[2],sys.argv[1])
    os.system(cmd)
    exit()

#maxChrLen = 250000000
DUP = np.zeros(maxChrLen,dtype='uint16')
INV = np.zeros(maxChrLen,dtype='uint16')
DEL = np.zeros(maxChrLen,dtype='uint16')
DUPINV = np.zeros(maxChrLen,dtype='uint16')
COV = np.zeros(maxChrLen,dtype='uint16')
start = time.time()
for filename in files:
    if os.path.getsize(filename)==0:
        print "skipping %s. File contains no data." % filename
        continue
    print "incorporating %s" % filename
    loc = np.genfromtxt(filename,delimiter=' ',usecols=1,dtype = np.uint32,unpack= True,invalid_raise=False) 
    dup,inv,dele,dupinv,cov = np.genfromtxt(filename,delimiter=' ',usecols= (2,3,4,5,6),dtype = np.uint16, unpack = True,invalid_raise=False,filling_values = 0)
    for pos in xrange(len(loc)):
        base= loc[pos]
        DUP[base] += dup[pos]
	DEL[base] += dele[pos]
	INV[base] += inv[pos]
        DUPINV[base] += dupinv[pos]
	COV[base] += cov[pos]

    print 'finished reading and folding %s in %f seconds.' % (filename, time.time() - start)
    print '-=Array Sizes in Memory=-'
    for ar in (loc, dup, inv, dele, dupinv, cov):
        print ar.nbytes / 10**6., 'MB'

start = time.time()
fileOut = open(sys.argv[1],'w')
print "writing to %s..." % sys.argv[1]
chromosome = re.search('chr(M|X|[0-9]+)',' '.join(map(str,sys.argv[1:]))).group()

touched = np.array(np.nonzero(COV)[0],dtype="uint32") #int 32 is large enough for 4B bases

for pos in touched:
    fileOut.write('%s %i %i %i %i %i %i\n' % ( chromosome, pos, DUP[pos],INV[pos],DEL[pos],DUPINV[pos], COV[pos]))
fileOut.close()
print 'completed writing in %f seconds.' % (time.time()- start)

