import os
import sys

try:
    from pbcore.io.FastaIO import SimpleFastaReader as sfr
except:
    from pbcore.io.FastaIO import FastaReader as sfr

from classes.model.Sequence import revcomp as rc

def readFastaToDict (fn):
    fastaDict = {}
    for entry in sfr(fn):
        fastaDict[entry.name] = entry.sequence
    return fastaDict

ref = sys.argv[1]
for fn in sys.argv[2:]:
    fDict = readFastaToDict(fn)
    fout = open(".".join(fn.split(".")[0:-1]) + "_oriented.fa", 'w')
    tmp = "temp.m4"
    cmd = "blasr -noSplitSubreads -m 4 -bestn 1 %s %s > %s" %(fn, ref, tmp)
    os.system(cmd)
    for line in open(tmp).xreadlines():
        ll = line.split(" ")
        strand = int(ll[8])
        qname = ll[0]
        seq = fDict[qname]
        outSeq = seq
        if strand == 1:
            outSeq = rc(outSeq)
        fout.write(">%s\n%s\n" %(qname, outSeq))


