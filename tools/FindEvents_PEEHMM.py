#!/usr/bin/env python -o

import sys,re
import numpy as np
from collections import deque

fileIn = sys.argv[1]
minEventSize = int(sys.argv[2])

chromosome = re.search('chr[X0-9]+',fileIn).group()

with open(fileIn) as f:
    events = set()
    startpos = -1
    endpos = -1
    eventflag = False
    prevpos = 0
    maxVal = 0
    for line in f:
        try:
            pos,DUP,INV,DEL,DUPINV,INS,COV = map(int,line.split()[1:])
            if any([COV > x for x in (INV,DEL,DUPINV,INS)]) :
                print >>sys.stderr, "warning. For at least one event type, you have more events than coverage."
                continue
            curr_events = set()
            if prevpos == pos:
                continue
            if prevpos != pos-1:
                if endpos-startpos > minEventSize:
                        print '\t'.join(map(str,[chromosome,startpos, endpos, endpos-startpos,",".join(list(events))]))
                events = set()
                maxVal = 0
                startpos = -1
                endpos = -1
                eventflag = False
            prevpos = pos 
            if DUP > 1:
                curr_events.add("DUP")
            if INV > 1 and DUPINV <= 1:
                curr_events.add("INV")
            if float(DEL)/COV > 0.4:
                curr_events.add("DEL")
            if DUPINV > 1 and INV > 1:
                curr_events.add("DUPINV")
            if INS > 1:
                curr_events.add("INS")
            if len(curr_events) > 0:
                if not eventflag:
                    eventflag = True
                    startpos = pos
                endpos = pos
                events = events.union(curr_events)
            else:
                if eventflag:
                    if endpos-startpos > minEventSize:
                        print '\t'.join(map(str,[chromosome,startpos, endpos, endpos-startpos,",".join(list(events))]))
                    eventflag = False
                    events = set()
                    maxVal = 0
                    startpos, endpos = -1,-1
#            print pos
        except IndexError:            
            print 'Index Error happening'
            continue
        except ValueError:
            print 'Line is damaged:',line
            continue
#woof. here we go.
        
        
