#!/usr/bin/env python -o

#this version uses an unweighted moving average
#this version uses a deque so that it can generate a moving average on the fly.

import sys
import numpy as np
from collections import deque

fileIn = sys.argv[1]
minEventSize = int(sys.argv[2])
threshold = float(sys.argv[3])
kernelSize=int(sys.argv[4])
halfKernelSize = int(kernelSize/2)
eps = np.finfo(np.float).eps

data = deque(maxlen = kernelSize)
smoothedData=deque(maxlen = kernelSize)
chromosome = fileIn.replace('.out','').split('/')[-1]

windowState = 0
windows = []
with open(fileIn) as f:
    for line in f:
        try:
            pos,QVR,RVR = map(int,line.split(':')[1].split())[:-1]
            try:
                lastPos
            except NameError:
                lastPos = pos
            if pos - lastPos > minEventSize:
                data = deque(maxlen = kernelSize)
                smoothedData=deque(maxlen = kernelSize)
                windowState = 0
            data.append( QVR / (float(RVR)+eps))
            smoothedData.append(np.mean(np.array(data)))
        except IndexError:            
            print 'Index Error happening'
            continue
        except ValueError:
            print 'Line is damaged:',line
            continue
        finally:
            lastPos = pos       
        if smoothedData[-1] > threshold:
            if windowState == 0:
                windowState = 1
                currentStart = pos - halfKernelSize
        if smoothedData[-1] <= 1.0 and windowState == 1:
            windowState = 0
            if pos - currentStart - halfKernelSize > minEventSize:
                windows.append([currentStart,pos])
                print '\t'.join(map(str,[chromosome,currentStart,pos - halfKernelSize ,pos-currentStart-halfKernelSize]))


