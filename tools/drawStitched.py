#!/usr/bin/env python -o

from matplotlib import pyplot as plt
import numpy as np
import sys, os
fileIn = sys.argv[1]
start = int(sys.argv[2])
finish = int(sys.argv[3])

windowStart = int(start - ( .1*( finish - start)))
windowFinish = int(finish + ( .1*( finish - start)))

normArray = np.zeros(windowFinish-windowStart+1)
for line in open(fileIn):
    pos,counts,refcounts ,coverage = line.split()
    chrm = pos.split(':')[0]
    base = int(pos.split(':')[1])
    counts = float(counts)
    refcounts = float(refcounts)
    if base > finish:
        break
    if coverage < 5 or base < start: # the area wasn't covered by at least 3 reference reads or isn't in the area of interest
        continue
    normArray[base-windowStart] = counts / refcounts

plt.plot(range(windowStart,windowFinish+1), normArray, marker=',')
plt.axis([windowStart,windowFinish, -0.1, max(normArray)*1.1])
plt.show()
#plt.savefig('chr20.png', dpi = 500)
