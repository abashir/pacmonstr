#!/usr/bin/env python -o

import glob,os,sys,subprocess,shlex,time

modules = 'module load gcc python py_packages\n' #pbcore used to be here, but is not actually needed when using .m5 fileIns
#paths = 'export PYTHONPATH=$PYTHONPATH:/hpc/users/pendlm02/git_repos/pacmonstr/\n'

def createStitchingJob(fileJobID_tuple, chromosome, iteration=1):#takes in a list of files to stitch OR a list of dependency tuples. If iteration = 1, treat as a simple list of file names, otherwise treat as a dependency list, where each tuple is (file2BStitched.out,jobID)
	if len(fileJobID_tuple) == 1:
		name = '%s_final.sh' % chromosome
#first, start LSF file by giving it a name
		LSF = open(name,'w')
		LSF.write('#!/bin/bash -l \n')# invoke bash interpreter
		LSF.write('#BSUB -L /bin/bash \n')
		LSF.write('#BSUB -J PM_%s_finalstitch\n' % (chromosome))
		LSF.write('#BSUB -q scavenger\n')# indicate queue
#		LSF.write('#BSUB -P acc_18\n')# allocation account
#			LSF.write('#BSUB -q scavenger\n')# indicate queue
		LSF.write('#BSUB -W 1:00\n')# wall time
		LSF.write('#BSUB -n 1\n')# number of cores
		LSF.write('#BSUB -Q 137\n')# exit codes which trigger a job to be requeued
		LSF.write('#BSUB -o PACMonSTR_%s_finalstitch.out\n' % (chromosome))
		LSF.write('#BSUB -e PACMonSTR_%s_finalstitch.err\n' % (chromosome))
		LSF.write('#BSUB -w \'done(%s)\'\n' % fileJobID_tuple[0][1])
		LSF.write('\n')
		LSF.write(modules)
#			LSF.write(paths)
		LSF.write('cd /sc/orga/scratch/pendlm02/na12878_PEE/%s/PEEHMMreads/\n' % chromosome)
		cmd = 'mv %s %s_final.out' % (fileJobID_tuple[0][0], str(sys.argv[1]))
		LSF.write('mv %s %s_final.out\n' % (fileJobID_tuple[0][0], str(sys.argv[1])))
		LSF.close()		
		cmd = 'bsub < %s' % name
		print cmd
		os.system(cmd)
		exit() # pairwise iterative stitching is complete. Last job created should be fully stitched and named with suffix _full.out
	else:
		#first cat files together so that they are 10GB in size
		pairingNumber = 2
		jobPairs = [fileJobID_tuple[n:n+pairingNumber] for n in range(0,len(fileJobID_tuple),pairingNumber)]
		print jobPairs
		dependencies = {}
		mem = 100000
		for num, pair in enumerate(jobPairs):
			name = '%s_%i_t%i_stitch.sh' % (chromosome,num,iteration)
#first, start LSF file by giving it a name
			LSF = open(name,'w')
			LSF.write('#!/bin/bash -l \n')# invoke bash interpreter
			LSF.write('#BSUB -L /bin/bash \n')
			LSF.write('#BSUB -J PM_na12878_stitch_%s_%i_t%i_HMM\n' % (chromosome,num,iteration))# name job
			LSF.write('#BSUB -q alloc\n')# indicate queue
#			LSF.write('#BSUB -P acc_18\n')# allocation account
			LSF.write('#BSUB -Q "137"\n')# exit codes which trigger a job to be requeued
#			LSF.write('#BSUB -m mothra\n')# machine 
			LSF.write('#BSUB -q scavenger\n')# indicate queue
			LSF.write('#BSUB -W 20:00\n')# wall time
			LSF.write('#BSUB -n 1\n')# number of cores
			LSF.write('#BSUB -M %i\n' % (mem*1000))# mem_limit in kb
			LSF.write('#BSUB -R "rusage[mem=%i]"\n' % mem)
			LSF.write('#BSUB -o PACMonSTR_%s_%i_t%i_stitch_na12878_HMM.out\n' % (chromosome,num,iteration))# stdout
			LSF.write('#BSUB -e PACMonSTR_%s_%i_t%i_stitch_na12878_HMM.err\n' % (chromosome,num,iteration))# stderr
			if iteration > 1: #the first round has no dependencies
				LSF.write('#BSUB -w \'done(%s)\'' % (') && done('.join([x[1] for x in pair])))
		
			LSF.write('\n')
			LSF.write(modules)
#			LSF.write(paths)
			LSF.write('cd /sc/orga/scratch/pendlm02/na12878_PEE/%s/PEEHMMreads/\n' % chromosome)
			if iteration > 1:
				LSF.write('python ~/git_repos/pacmonstr/tools/PEE_stitch.py %s_%i_t%i.out %s \n' % (chromosome , num,iteration,' '.join([x[0] for x in pair])))
			else:
				LSF.write('python ~/git_repos/pacmonstr/tools/PEE_stitch.py %s_%i_t%i.out %s \n' % (chromosome , num,iteration,' '.join([x for x in pair])))
			LSF.close()
			stout,sterr = subprocess.Popen('bsub < %s' % name,stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True).communicate()
			print stout,sterr
			jobIDIn = stout.split('>')[0].split('<')[1]
			print '%s_%i_t%i.out' % (chromosome,num,iteration), jobIDIn

			#create a second, dependent LSF job that compresses the two files that were used
#			if iteration > 1:
#				for x in pair:
#					xzCommand = 'python ~/git_repos/pacmonstr/tools/xz_job.py %s %s' % (x[0] ,jobIDIn)
#			else:
#				for x in pair:
#					xzCommand = 'python ~/git_repos/pacmonstr/tools/xz_job.py %s %s' % (x ,jobIDIn)
#			os.system(xzCommand)

			dependencies['%s_%i_t%i.out' % (chromosome,num,iteration)] = jobIDIn
	iteration += 1
	print dependencies
	createStitchingJob(dependencies.items(),chromosome,iteration)

chromosome = str(sys.argv[1])
files = map(lambda x: x.split('/')[-1],sys.argv[2:])

createStitchingJob(files,chromosome)



