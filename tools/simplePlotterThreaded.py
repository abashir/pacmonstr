    #!/usr/bin/env python -o

import sys, os
import pacmonstr_cython.trfinderFuncs as trf
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.cm as cm
from classes.model.Sequence import revcomp as rc
from classes.custom_io.FastaIO import FastaReader
from sklearn import hmm
from matplotlib.transforms import Affine2D
import mpl_toolkits.axisartist.floating_axes as floating_axes
from multiprocessing import Pool

try:
    add_rectangles = sys.argv[3]
#    add_rectangles = True
    import matplotlib.patches as mpatches
    print "adding rectangles to plots"
except:
    add_rectangles = False
try:
    plotDetail = sys.argv[4]
#    plotDetail = True
    print 'Generating detailed DotPlots with rectangles added.'
except:
    plotDetail = False

eps = np.finfo(np.float).eps
np.set_printoptions(linewidth=200)
np.set_printoptions(threshold=np.nan)


#strippedQuery = sys.argv[1].upper().replace('-','')
#strippedRef = sys.argv[2].upper().replace('-','')

qfn = sys.argv[1]
rfn = sys.argv[2]

#cfact = 30
cdiv = 400
queries = []
refs = []
ids = []
qDict = {}
rDict = {}
#with open qf as qfofn:
#    for l in f:
#        qfn = l.strip()
#        chrom, start, end = qfn.split("_")[1:4]
#        qDict["%s_%s_%s" %(chrom, start, end)] = []
#        for entry in FastaReader(qfn):
#            qid = entry.name.split(" ")
#            queries.append(entry.sequence)
#with open (rfofn) as f:
#    for l in f:
#        rfn = l.strip()
#        chrom, start, end = rfn.split("_")[1:4]
#        rDict["%s_%s_%s" %(chrom, start, end)] = []
#        
#        for entry in FastaReader(rfn): 
#            refs.append(entry.sequence)
for entry in FastaReader(qfn):
    qid = entry.name.split(" ")[0]
    queries.append(entry.sequence)
    ids.append(qid)
counter = 0
for entry in FastaReader(rfn):
#    print "sequence count:",counter
    rid = entry.name.split(" ")[0]
    refs.append(entry.sequence)
    ids[counter] = rid + "_" + ids[counter] 
    counter += 1

print "sequence count:",counter

windowSize = 10
windowSquared = windowSize * windowSize
misMatch = 4
prevWindowSum = 100
prevWindowSize = 5

def swap(line2d, xdata, ydata):
    line2d.set_xdata(ydata)
    line2d.set_ydata(xdata)

def blockFinderHelper(hmmIn, scale):
    start = -1
    end = 0
    position = 0
    events = []
    for x in hmmIn:
        if x == 1 and start == -1:
            start = position
        if x == 0 and start != -1:
            end = position
            events.append((int(start/scale),int(end/scale)))
            start = -1
        position += 1
    if start != -1 and len(events) > 0:
        events.append((int(start/scale),int(position/scale)))
    return events

def hmmBlock ( Xraw ,normRatio,eventRatio):
    #X = Xraw > 0.75
    ###problem arises when one and only one of normRatio and eventRatio is negative
    minRatio = min(normRatio, eventRatio)
    maxRatio = max(normRatio, eventRatio)
    onesMatrix = np.ones(Xraw.shape)
    minMat = minRatio*onesMatrix
    maxMat = maxRatio*onesMatrix
    X = np.minimum(Xraw, maxMat)
    X = np.maximum(X, minMat)
    startprob = np.array([0.99, 0.01])
    transmat = np.array([[0.9999999995, 0.0000000005], [0.0005, .9995]])
    
    model = hmm.GaussianHMM (2, 'spherical', startprob, transmat)
#        m_means = np.array([[.5], [-.5]])
    m_means = np.array([[normRatio], [eventRatio]])
    m_covars = np.array([[1],[1]])
        #m_covars = np.tile(np.identity(1), (2, 1, 1))
    model.means_ = m_means
    model.covars_ = m_covars
    revX = np.array([X]).transpose()
#        print revX.shape
    hidden_states = model.predict(revX)
        
#        print " ".join(map(str, X))
#        print "".join(map(str, hidden_states))
    return hidden_states

###Custom matplotlib cmap
"""
cdict = {'green': ((0.0, 1.0, 1.0),
                   (0.5, 0.0, 0.0),
                   (1.0, 0.0, 0.0)),
         'red': ((0.0, 1.0, 1.0),
                 (0.5, 1.0, 1.0),
                 (1.0, 0.0, 0.0)),
         'blue': ((0.0, 1.0, 1.0),
                  (0.5, 0.0, 0.0),
                  (1.0, 1.0, 1.0))}
my_cmap = col.LinearSegmentedColormap('my_colormap',cdict,256)
"""
########################
#for x in range(len(queries)):
def makeGridPlots (temp_seq):
    global cdiv
    global windowSize
    global misMatch
    global prevWindowSum
    global prevWindowSize

#    read_id, strippedQuery, strippedRef = temp_seq.split("____")
    read_id, strippedQuery, strippedRef = temp_seq

    subid = "_".join(read_id.split("_")[3:])
    if add_rectangles:
        chrom_loc = "_".join(read_id.split("_")[0:3]) + '_rect'
    else:
        chrom_loc = "_".join(read_id.split("_")[0:3])

    #comment this argument out if you'd like to write over previously generated images
    if os.path.isfile("%s_%s.png"  %(chrom_loc, subid.replace('/','-'))):
        print "this plot has already been generated. Skipping..."
        return 1
    
    #strippedRef = refs[x]
    rlen = len(strippedRef)
    #strippedQuery = queries[x]
    qlen = len(strippedQuery)
    cfact = int(rlen/cdiv)
#    print >>sys.stderr, "starting matrix 1"
    matrix1a = trf.CompareSeqPairRef(strippedRef, strippedRef, len(strippedRef), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
#    print >>sys.stderr, "starting matrix 1b"
    matrix1b = trf.CompareSeqPairRef(rc(strippedRef), rc(strippedRef), len(strippedRef), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix1 = np.minimum(matrix1a,matrix1b) 

#    print >>sys.stderr, "starting matrix 2"
    matrix2a = trf.CompareSeqPairRef(rc(strippedRef), strippedRef, len(strippedRef), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
#    print >>sys.stderr, "starting matrix 2b"
    matrix2b = trf.CompareSeqPairRef(strippedRef, rc(strippedRef), len(strippedRef), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix2 = np.minimum(matrix2a,matrix2b)[::-1]

#    print >>sys.stderr, "starting matrix 3"
    matrix3a = trf.CompareSeqPairRef(strippedQuery, strippedRef, len(strippedQuery), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_
#    print >>sys.stderr, "starting matrix 3b"
    matrix3b = trf.CompareSeqPairRef(rc(strippedQuery), rc(strippedRef), len(strippedQuery), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix3 = np.minimum(matrix3a,matrix3b) 

    #for_1_vs_for_2
#    print >>sys.stderr, "starting matrix 4"
    matrix4a = trf.CompareSeqPairRef(rc(strippedQuery), strippedRef, len(strippedQuery), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
#    print >>sys.stderr, "starting matrix 4b"
    matrix4b = trf.CompareSeqPairRef(strippedQuery, rc(strippedRef), len(strippedQuery), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix4 = np.minimum(matrix4a,matrix4b)[::-1]
#    print matrix4.dtype

#    print >>sys.stderr, "starting matrix 5"
    matrix5a = trf.CompareSeqPairRef(strippedQuery, strippedQuery, qlen, qlen, windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
#    print >>sys.stderr, "starting matrix 5b"
    matrix5b = trf.CompareSeqPairRef(rc(strippedQuery), rc(strippedQuery), qlen, qlen, windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix5 = np.minimum(matrix5a,matrix5b) 

#    print >>sys.stderr, "starting matrix 5"
    matrix6a = trf.CompareSeqPairRef(rc(strippedQuery), strippedQuery, qlen, qlen, windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
#    print >>sys.stderr, "starting matrix 5b"
    matrix6b = trf.CompareSeqPairRef(strippedQuery, rc(strippedQuery), qlen, qlen, windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix6 = np.minimum(matrix6a,matrix6b)[::-1] 
#AHA! ^^^this matrix needed to be inverted ^^!!^^

    REF = np.array(matrix1.sum(axis=0),dtype='f8')/(windowSquared)
    DUP = np.array(matrix3.sum(axis=0),dtype='f8')/(windowSquared)
    REFrc = np.array(matrix2.sum(axis=0),dtype='f8')/(windowSquared) #RTFT
    INV = np.array(matrix4.sum(axis=0),dtype='f8')/(windowSquared) #RQFT

    QUERY = np.array(matrix5.sum(axis=1),dtype='f8')/(windowSquared)
    INS = np.array(matrix3.sum(axis=1),dtype='f8')/(windowSquared)
    Qrc = np.array(matrix6.sum(axis=0),dtype='f8')/(windowSquared)
    QINV = np.array(matrix4.sum(axis=1),dtype='f8')/(windowSquared) 

    normDup = DUP - REF
    normInv = INV - REFrc
    normDel = INV + DUP
    normIns = (INS -QUERY) + (QINV - Qrc)

    hmmDUP = hmmBlock(normDup,0,1)
    hmmDEL = hmmBlock(normDel,0,-1)
    hmmINV = hmmBlock(normInv,0,1)
    hmmDUPINV = hmmBlock(normInv - normDel,-1,1)
    hmmINS = hmmBlock(normIns,0,-1)

    # divsion based metrics
#   normDup1 = (np.array(matrix3.sum(axis=0),dtype='f8') / np.array(matrix1.sum(axis=0),dtype='f8'))
    normDup1 = (( 1+DUP) / (1+REF)) - 1
    normInv1 = ((1+INV) / (1+REFrc)) -1
    #normInv1 /= (windowSquared)
    normDel1 = normDup1 + normInv1

    normDupQ1 = (np.array(matrix3.sum(axis=1),dtype='f8')/np.array(matrix5.sum(axis=1),dtype='f8'))

#   normInvQ1 = ((1+np.array(matrix4.sum(axis=1),dtype='f8')) /(1+ np.array(matrix6.sum(axis=1),dtype='f8'))) - 1
    normInvQ1 = np.array(matrix4.sum(axis=1),dtype='f8') / (windowSquared)
#    normInvQ1 /= (windowSquared)
    normIns1 = normDupQ1 + normInvQ1
#    print normIns1
    #to double check that the QFQF and QRQF matrices are being generated correctly...which they weren't.
#    h,l = map(int,matrix5.shape)
#    plt.imshow(trf.compressMatrix(matrix5,cfact,h,l),cmap = cm.Blues,alpha=0.5)

#    matrix6 = np.masked_where(comp4 == 0, comp4)
#    plt.imshow(trf.compressMatrix(matrix6,cfact,h,l),cmap = cm.Reds,alpha=0.5)
#    plt.savefig('QVQ_matrix_%s.png' % read_id)

##############################EVENT FINDING##################################
############EVENTS IN THE REFERENCE DOMAIN
    hmmDUP1 = hmmBlock(4*normDup1,0,1) 
    hmmDEL1 = hmmBlock(4*normDel1,0,-1) 
    hmmINV1 = hmmBlock(4*normInv1,0,1) 
    #hmmDUPINVpre = hmmBlock(normDup1,-0.25,0)
    #hmmDUPINVpre = hmmDUP1 < 1
    hmmSIMPLEDEL1 = hmmBlock(4*normDup1,0,-1)
    hmmDUPINVpre = hmmSIMPLEDEL1 < 1
    hmmDUPINV1 = np.bitwise_and(hmmDUPINVpre, hmmINV1) #inserted proximal substring


###########EVENTS IN THE QUERY DOMAIN
#    print normDupQ1,
    
    hmmINVQ1 = hmmBlock(normInvQ1,0,1) #required for identifying DPS
#hmmINS1 is equivalent to a hmmDEL signal in the query domain. 
#Shouldn't pick up sequences that are simply inverted. 
#Can't disambiguate a simple inversion from an inserted proximal inverted substring, so that is left to the DUPINV and DUP tracks.
    hmmINS1 = hmmBlock(normIns1,1,0) #Final track
    hmmDELINVpre = hmmBlock(normDupQ1,0,1)
#deleted proximal substring
#    print hmmDELINVpre,
#    print hmmINVQ1,
    hmmDELINV1 = np.bitwise_and(hmmDELINVpre, hmmINVQ1) #Final track
#    print hmmDELINV1
#    print >>sys.stderr, "compressing1 - cython"
#############################################################################

###########Compression

    h,l =  matrix1.shape
    matrix1 = np.ma.masked_where(matrix1 < .5 * windowSquared, matrix1).filled(0)
    comp1 = trf.compressMatrix(matrix1,cfact,int(h),int(l))
#    print >>sys.stderr, "compressed!"

#    print >>sys.stderr, "compressing2"
    h,l =  matrix2.shape
    matrix2 = np.ma.masked_where(matrix2 < .5 * windowSquared, matrix2).filled(0)
    comp2 = trf.compressMatrix(matrix2,cfact,int(h),int(l))

#    print >>sys.stderr, "compressing3"
    h,l =  matrix3.shape
    matrix3 = np.ma.masked_where(matrix3 < .5 * windowSquared, matrix3).filled(0)
    comp3 = trf.compressMatrix(matrix3,cfact,int(h),int(l))

#    print >>sys.stderr, "compressing4"
    h,l =  matrix4.shape
    matrix4 = np.ma.masked_where(matrix4 < .5 * windowSquared, matrix4).filled(0)
    comp4 = trf.compressMatrix(matrix4,cfact,int(h),int(l))

    if add_rectangles:
        chrom_loc = "_".join(read_id.split("_")[0:3]) + '_rect'
        if any(hmmDUP1):
            DupEvents = blockFinderHelper(hmmDUP1,1)
            print chrom_loc, "Dups:",DupEvents
        if any(hmmDEL1):
            DelEvents = blockFinderHelper(hmmDEL1,1)
            print chrom_loc,"Dels:",DelEvents
        if any(hmmINV1):
            InvEvents = blockFinderHelper(hmmINV1,1)
            print chrom_loc,'Invs:', InvEvents
        if any(hmmDUPINV1):
            DupInvEvents = blockFinderHelper(hmmDUPINV1,1)
            print chrom_loc,'DupInvs:', DupInvEvents
        if any(hmmINS1):
            InsEvents = blockFinderHelper(hmmINS1,1)
            print chrom_loc,'Ins:', InsEvents
        if any(hmmDELINV1):
            DelInvEvents = blockFinderHelper(hmmDELINV1,1)
            print chrom_loc,'DelIns:', DelInvEvents

#### New plot format
    gs0 = gridspec.GridSpec(1,3,width_ratios=[1,6,12])
    gs00a = gridspec.GridSpecFromSubplotSpec(13,1,subplot_spec= gs0[0])
    gs00 = gridspec.GridSpecFromSubplotSpec(13,10,subplot_spec= gs0[1])
    gs01 = gridspec.GridSpecFromSubplotSpec(4,2, subplot_spec=gs0[2])
    gs0.update(left=0.05, right=0.95,top=0.95,bottom=0.05, hspace=0.15)
#    gs01.update(left=0.05, right=0.95,top=0.95,bottom=0.05, hspace=0.15)

    ax00n1 =  plt.subplot(gs00a[0:5,:]) # FTFT + RTFT matrix
    ax00n2 =  plt.subplot(gs00a[8:13,:]) # FTFT + RTFT matrix
    #trRef = Affine2D().scale(2, 1).rotate_deg(90)
    #trQuery = Affine2D().scale(2, 1).rotate_deg(90)

    yRefRef = matrix1.sum(axis=1)
    yRefrcRef = matrix2.sum(axis=1)    
    maxyRefRef = max(max(yRefRef), max(yRefrcRef))
    #line2db, = ax00n1.plot(range(len(strippedRef)),yRefRef, 'b',alpha = 0.5)
    #line2dr, = ax00n1.plot(range(len(strippedRef)),yRefrcRef, 'r',alpha = 0.5)
    #line2db, = ax00n1.plot(range(qlen),hmmINS1, 'b',alpha = 0.5)
    #line2dr, = ax00n1.plot(range(qlen),yRefrcRef, 'r',alpha = 0.5)
    line2db, = ax00n1.plot(range(qlen), hmmINS1, 'b', alpha=0.5)
#    line2dr, = ax00n1.plot(range(qlen), hmmINS, 'r', alpha=0.5)
#    line2do, = ax00n1.plot(range(qlen), normIns, 'm', alpha=0.5)
    normIns1 = normDupQ1+normInvQ1
    tempnormIns1 = normIns1
    minRatio = min(1, 0)
    maxRatio = max(1, 0)
    onesMatrix = np.ones(tempnormIns1.shape)
    minMat = minRatio*onesMatrix
    maxMat = maxRatio*onesMatrix
    tempIns = np.minimum(tempnormIns1, maxMat)
    tempIns = np.maximum(tempIns, minMat)
    line2dg, = ax00n1.plot(range(qlen), tempIns, 'g', alpha=0.3)
#    swap(line2dr, line2dr.get_xdata(), line2dr.get_ydata())
    swap(line2db, line2db.get_xdata(), line2db.get_ydata())
    swap(line2dg, line2dg.get_xdata(), line2dg.get_ydata())
#    swap(line2do, line2do.get_xdata(), line2do.get_ydata())
    #swap(line2dr, line2dr.get_xdata(), line2dr.get_ydata())
#not here
    ax00n1.set_xlim(-1, 1.5)
    ax00n1.set_ylim(0, qlen)
    ax00n1.invert_yaxis()
    ax00n1.tick_params('y',labelsize=6)
    ax00n1.tick_params('x',labelsize=4)
    for tick in ax00n1.xaxis.get_major_ticks():
        tick.label.set_rotation(-90)

#    ax00n1.set_xticklabels([_.get_text() for _ in ax00n1.get_xticklabels()], rotation=45 )

    #ax00n1.set_yticklabels(ax00n1.get_yticklabels()[::-1])
    
    yQRef = matrix3.sum(axis=1)
    yQrcRef = matrix4.sum(axis=1)    
    maxyQRef = max(max(yQRef), max(yQrcRef))
#not here
    #ax00n1.setp(line2db, color='b', alpha=0.5)
    #ax00n1.plot(range(len(strippedRef)),matrix2.sum(axis=1), 'r',alpha = 0.5)
    #aux_ax_ref = ax00n1.get_aux_axes(trRef)
#    qline2db, = ax00n2.plot(range(len(strippedQuery)),matrix3.sum(axis=1), 'b',alpha = 0.5)
#    qline2dr, = ax00n2.plot(range(len(strippedQuery)),matrix4.sum(axis=1), 'r',alpha = 0.5)
    qline2db, = ax00n2.plot(range(len(strippedQuery)),yQRef, 'b',alpha = 0.5)
    qline2dr, = ax00n2.plot(range(len(strippedQuery)),yQrcRef, 'r',alpha = 0.5)
    #aux_a%x_query = ax00n2.get_aux_axes(trQuery)
    swap(qline2db, qline2db.get_xdata(), qline2db.get_ydata())
    swap(qline2dr, qline2dr.get_xdata(), qline2dr.get_ydata())

    ax00n2.set_xlim(0, maxyQRef)
    ax00n2.set_ylim(0, qlen)
    ax00n2.invert_yaxis()
    ax00n2.tick_params('y',labelsize=6)
    ax00n2.tick_params('x',labelsize=4)
    for tick in ax00n2.xaxis.get_major_ticks():
        tick.label.set_rotation(-90)
    #ax00n2.set_yticklabels(ax00n2.get_yticklabels()[::-1])

#left side     
    ax001 = plt.subplot(gs00[0:5,:]) # FTFT + RTFT matrix
    ax002 = plt.subplot(gs00[6,1:-1]) # FTFT and RTFT projections
    ax003 = plt.subplot(gs00[7,1:-1]) # FQFT and RQFT projections
    ax004 = plt.subplot(gs00[8:13,:]) # FQFT + RQFT matrxi

#   ax001.imshow(comp1, cmap = cm.Blues, alpha=0.5, extent= [0,len(strippedRef),len(strippedRef),0])
#   ax001.imshow(comp2, cmap = cm.Reds, alpha=0.5, extent= [0,len(strippedRef),len(strippedRef),0])

    ax001.imshow(comp1, cmap = cm.Blues, extent= [0,len(strippedRef),len(strippedRef),0])
    comp2 = np.ma.masked_where(comp2 < windowSize, comp2)
    ax001.imshow(comp2, cmap = cm.Reds, extent= [0,len(strippedRef),len(strippedRef),0])

    ax002.plot(matrix1.sum(axis=0), 'b',alpha = 0.5)
    ax002.plot(matrix2.sum(axis=0), 'r',alpha = 0.5)
    ax002.set_xlim(0,rlen)
    ax002.tick_params('both',labelsize=6)

    ax003.plot(matrix3.sum(axis=0), 'b',alpha = 0.5)
    ax003.plot(matrix4.sum(axis=0), 'r',alpha = 0.5)
    ax003.set_xlim(0,rlen)
    ax004.tick_params('both',labelsize=6)

#   ax004.imshow(comp3, cmap = cm.Blues, alpha=0.5, extent= [0,len(strippedRef),len(strippedQuery),0])
#   ax004.imshow(comp4, cmap = cm.Reds, alpha=0.5, extent= [0,len(strippedRef),len(strippedQuery),0])

    ax004.imshow(comp3, cmap = cm.Blues, extent= [0,len(strippedRef),len(strippedQuery),0])
    comp4 = np.ma.masked_where(comp4 < windowSize, comp4)
    ax004.imshow(comp4, cmap = cm.Reds, extent= [0,len(strippedRef),len(strippedQuery),0])

    if add_rectangles:
        if any(hmmDUP1):
            for event in DupEvents:
                rect = mpatches.Rectangle((event[0],0),event[1]-event[0],len(strippedQuery),color='blue', linewidth=0, alpha=0.2)
                ax004.add_patch(rect)
        if any(hmmDEL1):
            for event in DelEvents:
                rect = mpatches.Rectangle((event[0],0),event[1]-event[0],len(strippedQuery),color='red', linewidth=0, alpha=0.2)
                ax004.add_patch(rect)
        if any(hmmINV1):
            for event in InvEvents:
                rect = mpatches.Rectangle((event[0],0),event[1]-event[0],len(strippedQuery),color='black',linewidth=0, alpha=0.2)
                ax004.add_patch(rect)
        if any(hmmINS1):
            for event in InsEvents:
                rect = mpatches.Rectangle((0,event[0]),len(strippedRef),event[1]-event[0],color='yellow',linewidth=0, alpha=0.2)
                ax004.add_patch(rect)
        if any(hmmDUPINV1):
            for event in DupInvEvents:
                rect = mpatches.Rectangle((event[0],0),event[1]-event[0],len(strippedQuery),color='green',linewidth=0, alpha=0.2)
                ax004.add_patch(rect)
        if any(hmmDELINV1):
            for event in DelInvEvents:
                rect = mpatches.Rectangle((0,event[0]),len(strippedRef),event[1]-event[0],color='magenta',linewidth=0, alpha=0.2)
                ax004.add_patch(rect)

#right side
    ax011 = plt.subplot(gs01[0]) # hmmDup
    ax012 = plt.subplot(gs01[1]) # hmmInv
    ax013 = plt.subplot(gs01[2]) # hmmDel
    ax014 = plt.subplot(gs01[3]) # hmm DupInv

    ax015 = plt.subplot(gs01[4]) # hmmDup
    ax016 = plt.subplot(gs01[5]) # hmmInv
    ax017 = plt.subplot(gs01[6]) # hmmDel
    ax018 = plt.subplot(gs01[7]) # hmm DupInv

    ax011.plot(hmmDUP)
    ax011.plot(normDup, alpha=0.3)
    ax011.set_title('hmmDUP hyper1 vector')
    ax011.set_ylim(-2,5)
    ax011.set_xlim(0,rlen)
    ax011.tick_params('both',labelsize=6)

    ax013.plot(hmmINV)
    ax013.plot(normInv, alpha=0.3)
#   ax013.set_set_ylim(-0.5,1.5)
    ax013.set_title('hmmINV hyper1 vector')
    ax013.set_ylim(-2,5)
    ax013.set_xlim(0,rlen)
    ax013.tick_params('both',labelsize=6)

    ax015.plot(hmmDEL)
    ax015.plot(normDel, alpha=0.3)
#   ax015.set_set_ylim(-0.5,1.5)
    ax015.set_title('hmmDEL hyper1 vector')
    ax015.set_ylim(-2,5)
    ax015.set_xlim(0,rlen)
    ax015.tick_params('both',labelsize=6)

    ax017.plot(hmmDUPINV)
    ax017.plot(normDel, alpha=0.3)
#   ax017.set_set_ylim(-0.5,1.5)
    ax017.set_title('hmmDUPINV hyper1 vector')
    ax017.set_ylim(-2,5)
    ax017.set_xlim(0,rlen)
    ax017.tick_params('both',labelsize=6)

    ax012.plot(hmmDUP1)
    ax012.plot(normDup1, alpha=0.3)
#   ax012.set_set_ylim(-0.5,1.5)
    ax012.set_title('hmmDUP hyper2 vector')
    ax012.set_ylim(-2,5)
    ax012.set_xlim(0,rlen)
    ax012.tick_params('both',labelsize=6)

    ax014.plot(hmmINV1)
    ax014.plot(normInv1, alpha=0.3)
#   ax014.set_set_ylim(-0.5,1.5)
    ax014.set_title('hmmINV hyper2 vector')
    ax014.set_ylim(-2,5)
    ax014.set_xlim(0,rlen)
    ax014.tick_params('both',labelsize=6)

    ax016.plot(hmmDEL1)
    ax016.plot(normDel1, alpha=0.3)
#   ax016.set_set_ylim(-0.5,1.5)
    ax016.set_title('hmmDEL hyper2 vector')
    ax016.set_ylim(-2,5)
    ax016.set_xlim(0,rlen)
    ax016.tick_params('both',labelsize=6)

    ax018.plot(hmmDUPINV1)
    ax018.plot(normInv1 - normDel1, alpha=0.3)
#   ax018.set_set_ylim(-0.5,1.5)
    ax018.set_title('hmmDUPINV hyper2 vector')
    ax018.set_ylim(-2,5)
    ax018.set_xlim(0,rlen)
    ax018.tick_params('both',labelsize=6)
#    plt.show()

#    try:
#        os.mkdir(chrom_loc)
#    except:
#        print >>sys.stderr, "directory %s already exists" %(chrom_loc)
#    os.chdir(chrom_loc)
    plt.labelsize = 12
    plt.savefig("%s_%s.png"  %(chrom_loc, subid.replace('/','-')),dpi = 400,format = 'png')

    
    detailPlot = plt.figure()
    fig = detailPlot.add_subplot(111)
    fig.labelsize = 12
    fig.imshow(comp3, cmap = cm.Blues, extent= [0,len(strippedRef),len(strippedQuery),0])
#    comp4 = np.ma.masked_where(comp4 < windowSize , comp4) #########already masked in control panel view plotting step
    fig.imshow(comp4, cmap = cm.Reds, extent= [0,len(strippedRef),len(strippedQuery),0])
    if add_rectangles: 
        if any(hmmDUP1):
            for event in DupEvents:
                rect = mpatches.Rectangle((event[0],0),event[1]-event[0],len(strippedQuery),color='blue', linewidth=0, alpha=0.2)
                fig.add_patch(rect)
        if any(hmmDEL1):
            for event in DelEvents:
                rect = mpatches.Rectangle((event[0],0),event[1]-event[0],len(strippedQuery),color='red', linewidth=0, alpha=0.2)
                fig.add_patch(rect)
        if any(hmmINV1):
            for event in InvEvents:
                rect = mpatches.Rectangle((event[0],0),event[1]-event[0],len(strippedQuery),color='black',linewidth=0, alpha=0.2)
                fig.add_patch(rect)
        if any(hmmINS1):
            for event in InsEvents:
                rect = mpatches.Rectangle((0,event[0]),len(strippedRef),event[1]-event[0],color='yellow',linewidth=0, alpha=0.2)
                fig.add_patch(rect)
        if any(hmmDUPINV1):
            for event in DupInvEvents:
                rect = mpatches.Rectangle((event[0],0),event[1]-event[0],len(strippedQuery),color='green',linewidth=0, alpha=0.2)
                fig.add_patch(rect)
        if any(hmmDELINV1):
            for event in DelInvEvents:
                rect = mpatches.Rectangle((0,event[0]),len(strippedRef),event[1]-event[0],color='magenta',linewidth=0, alpha=0.2)
                fig.add_patch(rect)
    detailPlot.savefig("%s_%s_DP.png"  %(chrom_loc, subid.replace('/','-')),dpi = 400,format = 'png')
#    detailPlot.savefig("%s_%s_DP.svg"  %(chrom_loc, subid),dpi = 1200,format = 'svg')
#    detailPlot.savefig("%s_%s_DP.pdf"  %(chrom_loc, subid),dpi = 1200,format = 'pdf')

tempseqs = []
for x in range(len(queries)):
#for k in query
    qseq,rseq,sid = queries[x], refs[x], ids[x]
#    print sid
    tempseq = "%s____%s____%s" %(sid, qseq, rseq)
    tempseq = (sid, qseq, rseq)
    subid = "_".join(sid.split("_")[3:])
    if add_rectangles:
        chrom_loc = "_".join(sid.split("_")[0:3]) + '_rect'
    else:
        chrom_loc = "_".join(sid.split("_")[0:3])

    #comment this argument out if you'd like to write over previously generated images
    if os.path.isfile("%s_%s.png"  %(chrom_loc, subid.replace('/','-'))):
        continue#print "this plot has already been generated. Skipping..."
    else:
        tempseqs.append(tempseq)

#makeGridPlots (tempseq)
mypool = Pool(12)
mypool.map(makeGridPlots, tempseqs)


