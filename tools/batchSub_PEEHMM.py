#!/usr/bin/env python -o

import glob,os,sys,subprocess,shlex,time,re

modules = 'module load gcc python py_packages\n'
paths = 'export PYTHONPATH=$PYTHONPATH:~/git_repos/pacmonstr/\n'
#paths += 'export PYTHONPATH=$PYTHONPATH:/hpc/users/pendlm02/git_repos/pacmonstr/cython/\n'
#paths += 'export PYTHONPATH=$PYTHONPATH:/hpc/users/pendlm02/git_repos/rrna/scripts/\n'
#^Should be taken care of with a single path modification now.

#chromosome = str(sys.argv[1])
files = sys.argv[1:]
jobsCount = 0
jobCount = 1

for readFile in files:
	chromosome = re.search('chr[MX0-9]+',readFile).group()
	maxGB = 3
#	cmd = 'bjobs -u pendlm02 | grep \'PEND\' -c '
#	cmd = shlex.split(cmd)
#	stdout,stderr = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True).communicate()
#	jobCount = len(stdout.strip().split('\n'))
#	sys.stderr.write('\r%i jobs in queue' % jobCount)
#	while jobCount > 20000:
#		time.sleep(30) #if too many jobs in queue, wait 5 minutes, then submit the next job
#		cmd = 'qselect -u pendlm02 -s Q'
#		cmd = shlex.split(cmd)
#		stdout,stderr = subprocess.Popen(cmd,stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
#		jobCount = len(stdout.split('\n'))
#		sys.stderr.write('\r%i jobs in queue' % jobCount)
	mem = (1000 * (int(maxGB) + 3))
	name = '%s_%iGB_%s_project.sh' % (chromosome,mem/1000, jobsCount)
	#first, start LSF file by giving it a name
	LSF = open(name,'w')
	fullFN = readFile.replace('./',os.getcwd()+'/')
	directory = '/'.join(fullFN.split('/')[:-1])
	LSF.write("#!/bin/bash -l\n")
	LSF.write('#BSUB -L /bin/sh \n')# name job
	LSF.write('#BSUB -J PACMonSTR_%s_na12878\n' % chromosome)# name job
#	LSF.write('#BSUB -q alloc\n')# indicate queue
	LSF.write('#BSUB -q scavenger\n')# indicate queue
	LSF.write('#BSUB -W 10:00\n')# time
	LSF.write('#BSUB -m mothra\n')# machine 
#	LSF.write('#BSUB -P acc_17\n')# allocation account
	LSF.write('#BSUB -n 1\n')# number of cores
	LSF.write('#BSUB -M %i\n'% (1000*mem)) #in kb I think?
	LSF.write('#BSUB -R "rusage[mem=%i]"\n' % mem)
	LSF.write('#BSUB -o PACMonSTR_%i_%s_%s_na12878.out\n' % (jobsCount,chromosome,readFile.split('.')[-1]))# stdout
	LSF.write('#BSUB -e PACMonSTR_%i_%s_%s_na12878.err\n' % (jobsCount,chromosome,readFile.split('.')[-1]))# stderr
	
	LSF.write('\n')
	LSF.write('cd %s\n' % directory)
	#determine whether a ./read/folder has been made for this chromosome. If not, make one
	if not os.path.exists('%s/PEEHMMreads' % directory):
		LSF.write('mkdir PEEHMMreads\n')
	LSF.write(modules)
	LSF.write(paths)
	LSF.write('python ~/git_repos/pacmonstr/classes/PEE_pipeline.py %s ./PEEHMMreads/ %s\n' % (chromosome , fullFN))
	LSF.close()
	command = 'bsub < %s' % name
#	print command
	jobsCount += 1
	os.system(command)



