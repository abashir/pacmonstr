#!/usr/bin/env python -o

import glob,os,sys,subprocess,shlex,time

modules = 'module load gcc python py_packages\n' #pbcore used to be here, but is not actually needed when using .m5 fileIns
paths = 'export PYTHONPATH=$PYTHONPATH:/hpc/users/pendlm02/git_repos/pacmonstr/classes/\n'
paths += 'export PYTHONPATH=$PYTHONPATH:/hpc/users/pendlm02/git_repos/pacmonstr/cython/\n'
paths += 'export PYTHONPATH=$PYTHONPATH:/hpc/users/pendlm02/git_repos/rrna/scripts/\n'

def createStitchingJob(fileJobID_tuple, chromosome, iteration=1):#takes in a list of files to stitch OR a list of dependency tuples. If iteration = 1, treat as a simple list of file names, otherwise treat as a dependency list, where each tuple is (file2BStitched.out,jobID)
	if len(fileJobID_tuple) == 1:
		exit() # pairwise iterative stitching is complete. Last job created should be fully stitched.
	else:
		jobPairs = [fileJobID_tuple[n:n+2] for n in range(0,len(fileJobID_tuple),2)]
		print jobPairs
		dependencies = {}
		for num, pair in enumerate(jobPairs):
			name = '%s_%i_t%i_stitch.sh' % (chromosome,num,iteration)
#first, start LSF file by giving it a name
			LSF = open(name,'w')
			LSF.write('#BSUB -L /bin/bash \n')# name job
			LSF.write('#BSUB -J PACMonSTR_%s_t%i_na12878_HMM\n' % (chromosome,iteration))# name job
			LSF.write('#BSUB -q scavenger\n')# indicate queue
			LSF.write('#BSUB -W 5:00\n')# memory
			LSF.write('#BSUB -n 1\n')# number of cores
			LSF.write('#BSUB -M 15000000\n')# mem_limit in kb
			LSF.write('#BSUB -o PACMonSTR_%s_%i_t%i_stitch_na12878_HMM.out\n' % (chromosome,num,iteration))# stdout
			LSF.write('#BSUB -e PACMonSTR_%s_%i_t%i_stitch_na12878_HMM.err\n' % (chromosome,num,iteration))# stderr
			if iteration > 1: #the first round has no dependencies
				LSF.write('#BSUB -w \'done(%s)\'' % (') && done('.join([x[1] for x in pair])))
		
			LSF.write('\n')
			LSF.write(modules)
			LSF.write(paths)
			LSF.write('cd /sc/orga/scratch/pendlm02/na12878/%s/reds/\n' % chromosome)
			if iteration > 1:
				LSF.write('python ~/git_repos/pacmonstr/tools/HMM_stitch.py %s_%i_t%i.out %s \n' % (chromosome , num,iteration,' '.join([x[0] for x in pair])))
			else:
				LSF.write('python ~/git_repos/pacmonstr/tools/HMM_stitch.py %s_%i_t%i.out %s \n' % (chromosome , num,iteration,' '.join([x for x in pair])))
			LSF.close()
			stout,sterr = subprocess.Popen('bsub < %s' % name,stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True).communicate()
			print stout
			print ('%s_%i_t%i.out' % (chromosome,num,iteration), stout.split('>')[0].split('<')[1])
			dependencies['%s_%i_t%i.out' % (chromosome,num,iteration)] = stout.split('>')[0].split('<')[1]
	iteration += 1
	print dependencies
	createStitchingJob(dependencies.items(),chromosome,iteration)



chromosome = str(sys.argv[1])
files = map(lambda x: x.split('/')[-1],sys.argv[2:])
#balance = int(len(files)**.5**.5)+1
#fpj = len(files) / balance
#pairs = [files[n:n+fpj] for n in range(0,len(files),fpj)]
#dependencytree1 = {}

####
createStitchingJob(files,chromosome)


####
'''
for num, pair in enumerate(pairs):
	dependencies = [dependencytree[x] for x in pair]
	name = '%s_%i_t1_stitch.sh' % (chromosome,num)
	#first, start LSF file by giving it a name
	LSF = open(name,'w')
	LSF.write('#BSUB -L /bin/bash \n')# name job
	LSF.write('#BSUB -J PACMonSTR_%s_t1_na12878_HMM\n' % chromosome)# name job
	LSF.write('#BSUB -q scavenger\n')# indicate queue
	LSF.write('#BSUB -W 5:00\n')# memory
	LSF.write('#BSUB -n 1\n')# number of cores
	LSF.write('#BSUB -M 15000000\n')# mem_limit in kb
	LSF.write('#BSUB -o PACMonSTR_%s_%i_t1_stitch_na12878_HMM.out\n' % (chromosome,num))# stdout
	LSF.write('#BSUB -e PACMonSTR_%s_%i_t1_stitch_na12878_HMM.err\n' % (chromosome,num))# stderr
	LSF.write('#BSUB -w \'done(%s)\'' % (') && done('.join(dependencies)))
	
	LSF.write('\n')
	LSF.write(modules)
	LSF.write(paths)
	LSF.write('cd /sc/orga/scratch/pendlm02/na12878/%s/HMMreads/\n' % chromosome)
	LSF.write('python ~/git_repos/pacmonstr/tools/HMM_stitch.py %s_%i_t1.out %s \n' % (chromosome , num, ' '.join(pair)))
	LSF.close()
	stout,sterr = subprocess.Popen('bsub < %s' % name,stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True).communicate()
	print stout
	dependencytree1['%s_%i_t1.out' % (chromosome,num)] = stout.split('>')[0].split('<')[1]
#os.system('bjobs -u pendlm02')

print dependencytree1
	
t1_files = dependencytree1.keys()
files = map(lambda x: x.split('/')[-1],t1_files)
balance = int(len(files)**.5)+1
fpj = len(files) / balance
pairs = [files[n:n+balance] for n in range(0,len(files),balance)]
dependencyList2 = []
for num, pair in enumerate(pairs):
	dependencies = [dependencytree1[x] for x in pair]

	name = '%s_%i_t2_stitch.sh' % (chromosome,num)
	print 'script',name,'depends on',dependencies
	#first, start LSF file by giving it a name
	LSF = open(name,'w')
	LSF.write('#BSUB -L /bin/bash \n')# name job
	LSF.write('#BSUB -J PACMonSTR_%s_t2_na12878_HMM\n' % chromosome)# name job
	LSF.write('#BSUB -q scavenger\n')# indicate queue
	LSF.write('#BSUB -W 7:00\n')# memory
	LSF.write('#BSUB -n 1\n')# number of cores
	LSF.write('#BSUB -M 15000000\n')# mem_limit in kb
	LSF.write('#BSUB -o PACMonSTR_%s_%i_t2_stitch_na12878_HMM.out\n' % (chromosome,num))# stdout
	LSF.write('#BSUB -e PACMonSTR_%s_%i_t2_stitch_na12878_HMM.err\n' % (chromosome,num))# stderr
	LSF.write('#BSUB -w \'done(%s)\'' % (') && done('.join(dependencies)))
	LSF.write('\n')
	LSF.write(modules)
	LSF.write(paths)
	LSF.write('cd /sc/orga/scratch/pendlm02/na12878/%s/HMMreads/\n' % chromosome)
	LSF.write('python ~/git_repos/pacmonstr/tools/HMM_stitch.py %s_%i_t2.out %s \n' % (chromosome , num, ' '.join(pair)))

	LSF.close()
	stout,sterr = subprocess.Popen('bsub < %s' % name ,stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()
	print stout
	dependencyList2.append(stout.split('>')[0].split('<')[1])

#os.system('bjobs -u pendlm02')
print dependencyList2

name = '%s_stitch.sh' % chromosome
LSF = open(name,'w')

LSF.write('#BSUB -L /bin/bash \n')# name job
LSF.write('#BSUB -J PACMonSTR_%s_na12878_HMM\n' % chromosome)# name job
LSF.write('#BSUB -q scavenger\n')# indicate queue
LSF.write('#BSUB -W 7:00\n')# wall time
LSF.write('#BSUB -n 1\n')# number of cores
LSF.write('#BSUB -M 15000000\n')# mem_limit in kb
LSF.write('#BSUB -o PACMonSTR_%s_stitch_na12878_HMM.out\n' % chromosome)# stdout
LSF.write('#BSUB -e PACMonSTR_%s_stitch_na12878_HMM.err\n' % chromosome)# stderr
LSF.write('#BSUB -w \'done(%s)\'' % (') && done('.join(dependencyList2)))
LSF.write('\n')
LSF.write(modules)
LSF.write(paths)
LSF.write('cd /sc/orga/scratch/pendlm02/na12878/%s/HMMreads/\n' % chromosome)
LSF.write('python ~/git_repos/pacmonstr/tools/HMM_stitch.py %s.out %s_*t2.out \n' % (chromosome , chromosome))

LSF.close()
command = 'bsub < %s' % name
print command
os.system(command)
'''
