#!/usr/bin/env python -o

import sys
fileIn = sys.argv[1]
start = int(sys.argv[2])
stop = int(sys.argv[3])

for line in open(fileIn):
    splitLine = line.split()
    chromosome, position = splitLine[0].split(':')
    position = int(position)
    if position > start:
        print line
    if position > stop:
        exit()
