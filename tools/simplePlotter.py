#!/usr/bin/env python -o

import sys
import pacmonstr_cython.trfinderFuncs as trf
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.cm as cm
from classes.model.Sequence import revcomp as rc
from classes.custom_io.FastaIO import FastaReader
from sklearn import hmm
from matplotlib.transforms import Affine2D
import mpl_toolkits.axisartist.floating_axes as floating_axes
from multiprocessing import Pool

eps = np.finfo(np.float).eps
np.set_printoptions(linewidth=200)
np.set_printoptions(threshold=np.nan)


#strippedQuery = sys.argv[1].upper().replace('-','')
#strippedRef = sys.argv[2].upper().replace('-','')

qfn = sys.argv[1]
rfn = sys.argv[2]

cfact = 30
queries = []
refs = []
for entry in FastaReader(qfn):
    queries.append(entry.sequence)
for entry in FastaReader(rfn): 
    refs.append(entry.sequence)

windowSize = 8
misMatch = 0
prevWindowSum = 1
prevWindowSize = 0

def swap(line2d, xdata, ydata):
    line2d.set_xdata(ydata[::-1])
    line2d.set_ydata(xdata[::-1])

def compressMatrix (m, scale=10):
    s = m.shape
    newM = np.zeros((s[0]/scale, s[1]/scale))
    s2 = newM.shape
    for i in range (s2[0]):
        for j in range (s2[1]):
            newM[i][j] = np.max(m[i*scale:i*scale+scale,j*scale:j*scale+scale])
    return newM

def hmmBlock ( Xraw ,normRatio,eventRatio):
    #X = Xraw > 0.75
    minRatio = min(normRatio, eventRatio)
    maxRatio = max(normRatio, eventRatio)
    onesMatrix = np.ones(Xraw.shape)
    minMat = minRatio*onesMatrix
    maxMat = maxRatio*onesMatrix
    X = np.minimum(Xraw, maxMat)
    X = np.maximum(X, minMat)
    startprob = np.array([0.99, 0.01])
    transmat = np.array([[0.9999999995, 0.0000000005], [0.00005, .99995]])
    
    model = hmm.GaussianHMM (2, 'spherical', startprob, transmat)
#        m_means = np.array([[.5], [-.5]])
    m_means = np.array([[normRatio], [eventRatio]])
    m_covars = np.array([[1],[1]])
        #m_covars = np.tile(np.identity(1), (2, 1, 1))
    model.means_ = m_means
    model.covars_ = m_covars
    revX = np.array([X]).transpose()
#        print revX.shape
    hidden_states = model.predict(revX)
        
#        print " ".join(map(str, X))
#        print "".join(map(str, hidden_states))
    return hidden_states

###Custom matplotlib cmap
"""
cdict = {'green': ((0.0, 1.0, 1.0),
                   (0.5, 0.0, 0.0),
                   (1.0, 0.0, 0.0)),
         'red': ((0.0, 1.0, 1.0),
                 (0.5, 1.0, 1.0),
                 (1.0, 0.0, 0.0)),
         'blue': ((0.0, 1.0, 1.0),
                  (0.5, 0.0, 0.0),
                  (1.0, 1.0, 1.0))}
my_cmap = col.LinearSegmentedColormap('my_colormap',cdict,256)
"""
########################
#for x in range(len(queries)):
def makeGridPlots (temp_seq):
    global cfact
    global windowSize
    global misMatch
    global prevWindowSum
    global prevWindowSize
    strippedQuery, strippedRef = temp_seq.split("_")
    #strippedRef = refs[x]
    rlen = len(strippedRef)
    #strippedQuery = queries[x]
    qlen = len(strippedQuery)
    print >>sys.stderr, "starting matrix 1"
    matrix1a = trf.CompareSeqPairRef(strippedRef, strippedRef, len(strippedRef), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
    print >>sys.stderr, "starting matrix 1b"
    matrix1b = trf.CompareSeqPairRef(rc(strippedRef), rc(strippedRef), len(strippedRef), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix1 = np.minimum(matrix1a,matrix1b) 
    #matrix1a = None
    #matrix1b = None

    print >>sys.stderr, "starting matrix 2"
    matrix2a = trf.CompareSeqPairRef(rc(strippedRef), strippedRef, len(strippedRef), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
    print >>sys.stderr, "starting matrix 2b"
    matrix2b = trf.CompareSeqPairRef(strippedRef, rc(strippedRef), len(strippedRef), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix2 = np.minimum(matrix2a,matrix2b)[::-1]
    #matrix2a = None
    #matrix2b = None

    print >>sys.stderr, "starting matrix 3"
    matrix3a = trf.CompareSeqPairRef(strippedQuery, strippedRef, len(strippedQuery), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_
    print >>sys.stderr, "starting matrix 3b"
    matrix3b = trf.CompareSeqPairRef(rc(strippedQuery), rc(strippedRef), len(strippedQuery), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix3 = np.minimum(matrix3a,matrix3b) 
    #matrix3a = None
    #matrix3b = None

    #for_1_vs_for_2
    print >>sys.stderr, "starting matrix 4"
    matrix4a = trf.CompareSeqPairRef(rc(strippedQuery), strippedRef, len(strippedQuery), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
    print >>sys.stderr, "starting matrix 4b"
    matrix4b = trf.CompareSeqPairRef(strippedQuery, rc(strippedRef), len(strippedQuery), len(strippedRef),windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix4 = np.minimum(matrix4a,matrix4b)[::-1]
    print matrix4.dtype
    #matrix4a = None
    #matrix4b = None

    print >>sys.stderr, "starting matrix 5"
    matrix5a = trf.CompareSeqPairRef(strippedQuery, strippedQuery, qlen, qlen, windowSize,misMatch,prevWindowSum,prevWindowSize)
    #rc_1_vs_rc_2
    print >>sys.stderr, "starting matrix 5b"
    matrix5b = trf.CompareSeqPairRef(rc(strippedQuery), rc(strippedQuery), qlen, qlen, windowSize,misMatch,prevWindowSum,prevWindowSize)[::-1,::-1]
    matrix5 = np.minimum(matrix5a,matrix5b) 



    REF = np.array(matrix1.sum(axis=0),dtype='f8')/(windowSize*windowSize)
    DUP = np.array(matrix2.sum(axis=0),dtype='f8')/(windowSize*windowSize)
    REFrc = np.array(matrix3.sum(axis=0),dtype='f8')/(windowSize*windowSize)
    INV = np.array(matrix4.sum(axis=0),dtype='f8')/(windowSize*windowSize)

    QUERY = np.array(matrix5.sum(axis=1),dtype='f8')/(windowSize*windowSize)
    INS = np.array(matrix2.sum(axis=1),dtype='f8')/(windowSize*windowSize)

    normDup = DUP - REF
    normInv = INV - REFrc
    normDel = INV + DUP

    normDup1 = np.array(matrix3.sum(axis=0),dtype='f8')/ np.array(matrix1.sum(axis=0),dtype='f8')
    normInv1 = ((1+np.array(matrix4.sum(axis=0),dtype='f8')) / (1+np.array(matrix2.sum(axis=0),dtype='f8'))) -1
    normDel1 = normDup1 + normInv1
    normIns1 =   np.array(matrix3.sum(axis=1),dtype='f8')/ np.array(matrix5.sum(axis=1),dtype='f8')
    
    hmmDUP = hmmBlock(normDup,0,1)
    hmmDEL = hmmBlock(normDel,0,-1)
    hmmINV = hmmBlock(normInv,0,1)
    hmmDUPINV = hmmBlock(normInv1 - normDel,-1,1)

    hmmDUP1 = hmmBlock(normDup1,1,2)
    hmmDEL1 = hmmBlock(normDel1,1,0)
    hmmINV1 = hmmBlock(normInv1,0,1)
    hmmDUPINV1 = hmmBlock(normInv1+normDup1 ,1,2)
    hmmINS1 = hmmBlock(normIns1,1,0)

    print >>sys.stderr, "compressing1 - cython"
    h,l =  matrix1.shape
    comp1 = trf.compressMatrix(matrix1,cfact,int(h),int(l))
    print >>sys.stderr, "compressed!"

    print >>sys.stderr, "compressing2"
    h,l =  matrix2.shape
    comp2 = trf.compressMatrix(matrix2,cfact,int(h),int(l))

    print >>sys.stderr, "compressing3"
    h,l =  matrix3.shape
    comp3 = trf.compressMatrix(matrix3,cfact,int(h),int(l))

    print >>sys.stderr, "compressing4"
    h,l =  matrix4.shape
    comp4 = trf.compressMatrix(matrix4,cfact,int(h),int(l))

#### New plot format
    
    gs0 = gridspec.GridSpec(1,3,width_ratios=[1,6,12])
    gs00a = gridspec.GridSpecFromSubplotSpec(13,1,subplot_spec= gs0[0])
    gs00 = gridspec.GridSpecFromSubplotSpec(13,10,subplot_spec= gs0[1])
    gs01 = gridspec.GridSpecFromSubplotSpec(4,2, subplot_spec=gs0[2])
    gs0.update(left=0.05, right=0.95,top=0.95,bottom=0.05, hspace=0.15)
#    gs01.update(left=0.05, right=0.95,top=0.95,bottom=0.05, hspace=0.15)

    ax00n1 =  plt.subplot(gs00a[0:5,:]) # FTFT + RTFT matrix
    ax00n2 =  plt.subplot(gs00a[8:13,:]) # FTFT + RTFT matrix

    #trRef = Affine2D().scale(2, 1).rotate_deg(90)
    #trQuery = Affine2D().scale(2, 1).rotate_deg(90)

    yRefRef = matrix1.sum(axis=1)
    yRefrcRef = matrix2.sum(axis=1)    
    maxyRefRef = max(max(yRefRef), max(yRefrcRef))
    #line2db, = ax00n1.plot(range(len(strippedRef)),yRefRef, 'b',alpha = 0.5)
    #line2dr, = ax00n1.plot(range(len(strippedRef)),yRefrcRef, 'r',alpha = 0.5)
    #line2db, = ax00n1.plot(range(qlen),hmmINS1, 'b',alpha = 0.5)
    #line2dr, = ax00n1.plot(range(qlen),yRefrcRef, 'r',alpha = 0.5)
    line2db, = ax00n1.plot(range(qlen), hmmINS1, 'b', alpha=0.5)

    swap(line2db, line2db.get_xdata(), line2db.get_ydata())
    #swap(line2dr, line2dr.get_xdata(), line2dr.get_ydata())

    ax00n1.set_xlim(-1, 2)
    ax00n1.set_ylim(0, rlen)
    #ax00n1.set_yticklabels(ax00n1.get_yticklabels()[::-1])

    
    yQRef = matrix3.sum(axis=1)
    yQrcRef = matrix4.sum(axis=1)    
    maxyQRef = max(max(yQRef), max(yQrcRef))
    
    #ax00n1.setp(line2db, color='b', alpha=0.5)
    #ax00n1.plot(range(len(strippedRef)),matrix2.sum(axis=1), 'r',alpha = 0.5)
    #aux_ax_ref = ax00n1.get_aux_axes(trRef)
#    qline2db, = ax00n2.plot(range(len(strippedQuery)),matrix3.sum(axis=1), 'b',alpha = 0.5)
#    qline2dr, = ax00n2.plot(range(len(strippedQuery)),matrix4.sum(axis=1), 'r',alpha = 0.5)
    qline2db, = ax00n2.plot(range(len(strippedQuery)),yQRef, 'b',alpha = 0.5)
    qline2dr, = ax00n2.plot(range(len(strippedQuery)),yQrcRef, 'r',alpha = 0.5)
    #aux_a%x_query = ax00n2.get_aux_axes(trQuery)
    swap(qline2db, qline2db.get_xdata(), qline2db.get_ydata())
    swap(qline2dr, qline2dr.get_xdata(), qline2dr.get_ydata())

    ax00n2.set_xlim(0, maxyQRef)
    ax00n2.set_ylim(0, rlen)
    #ax00n2.set_yticklabels(ax00n2.get_yticklabels()[::-1])

#left side     
    ax001 = plt.subplot(gs00[0:5,:]) # FTFT + RTFT matrix
    ax002 = plt.subplot(gs00[6,1:-1]) # FTFT and RTFT projections
    ax003 = plt.subplot(gs00[7,1:-1]) # FQFT and RQFT projections
    ax004 = plt.subplot(gs00[8:13,:]) # FQFT + RQFT matrxi

    ax001.imshow(comp1, cmap = cm.Blues, alpha=0.5, extent= [0,len(strippedRef),len(strippedRef),0])
    ax001.imshow(comp2, cmap = cm.Reds, alpha=0.5, extent= [0,len(strippedRef),len(strippedRef),0])

    ax002.plot(matrix1.sum(axis=0), 'b',alpha = 0.5)
    ax002.plot(matrix2.sum(axis=0), 'r',alpha = 0.5)
    ax002.set_xlim(0,rlen)


    ax003.plot(matrix3.sum(axis=0), 'b',alpha = 0.5)
    ax003.plot(matrix4.sum(axis=0), 'r',alpha = 0.5)
    ax003.set_xlim(0,rlen)

    ax004.imshow(comp3, cmap = cm.Blues, alpha=0.5, extent= [0,len(strippedRef),len(strippedQuery),0])
    ax004.imshow(comp4, cmap = cm.Reds, alpha=0.5, extent= [0,len(strippedRef),len(strippedQuery),0])

#right side

    ax011 = plt.subplot(gs01[0]) # hmmDup
    ax012 = plt.subplot(gs01[1]) # hmmInv
    ax013 = plt.subplot(gs01[2]) # hmmDel
    ax014 = plt.subplot(gs01[3]) # hmm DupInv

    ax015 = plt.subplot(gs01[4]) # hmmDup
    ax016 = plt.subplot(gs01[5]) # hmmInv
    ax017 = plt.subplot(gs01[6]) # hmmDel
    ax018 = plt.subplot(gs01[7]) # hmm DupInv

    ax011.plot(hmmDUP)
    ax011.plot(normDup, alpha=0.3)
    ax011.set_title('hmmDUP hyper1 vector')
    ax011.set_ylim(-2,5)
    ax011.set_xlim(0,rlen)

    ax013.plot(hmmINV)
    ax013.plot(normInv, alpha=0.3)
#   ax013.set_set_ylim(-0.5,1.5)
    ax013.set_title('hmmINV hyper1 vector')
    ax013.set_ylim(-2,5)
    ax013.set_xlim(0,rlen)

    ax015.plot(hmmDEL)
    ax015.plot(normDel, alpha=0.3)
#   ax015.set_set_ylim(-0.5,1.5)
    ax015.set_title('hmmDEL hyper1 vector')
    ax015.set_ylim(-2,5)
    ax015.set_xlim(0,rlen)

    ax017.plot(hmmDUPINV)
    ax017.plot(normDel, alpha=0.3)
#   ax017.set_set_ylim(-0.5,1.5)
    ax017.set_title('hmmDUPINV hyper1 vector')
    ax017.set_ylim(-2,5)
    ax017.set_xlim(0,rlen)

    ax012.plot(hmmDUP1)
    ax012.plot(normDup1, alpha=0.3)
#   ax012.set_set_ylim(-0.5,1.5)
    ax012.set_title('hmmDUP hyper2 vector')
    ax012.set_ylim(-2,5)
    ax012.set_xlim(0,rlen)

    ax014.plot(hmmINV1)
    ax014.plot(normInv1, alpha=0.3)
#   ax014.set_set_ylim(-0.5,1.5)
    ax014.set_title('hmmINV hyper2 vector')
    ax014.set_ylim(-2,5)
    ax014.set_xlim(0,rlen)

    ax016.plot(hmmDEL1)
    ax016.plot(normDel1, alpha=0.3)
#   ax016.set_set_ylim(-0.5,1.5)
    ax016.set_title('hmmDEL hyper2 vector')
    ax016.set_ylim(-2,5)
    ax016.set_xlim(0,rlen)

    ax018.plot(hmmDUPINV1)
    ax018.plot(normInv1 - normDel1, alpha=0.3)
#   ax018.set_set_ylim(-0.5,1.5)
    ax018.set_title('hmmDUPINV hyper2 vector')
    ax018.set_ylim(-2,5)
    ax018.set_xlim(0,rlen)



    plt.show()
    plt.savefig("%s_%s_%s_%s.png"  %(chrom, start, end, read_id))


for x in range(len(queries)):
    qseq,rseq = queries[x], refs[x]
    tempseq = "%s_%s" %(qseq, rseq)
    makeGridPlots (tempseq)




'''


##################################plot 1
    gs = gridspec.GridSpec(6,2,height_ratios=[2,10,10,2,2,2])
    gs.update(left=0.05, right=0.95,top=0.95,bottom=0.05, hspace=0.15)
    ax1 = plt.subplot(gs[0]) # FTFT projection (DUP ref)
    ax2 = plt.subplot(gs[1]) # RTFT projection (INV ref)
    ax3 = plt.subplot(gs[2]) # FTFT matrix
    ax4 = plt.subplot(gs[3]) # RTFT matrix
    ax5 = plt.subplot(gs[4]) # FQFT matrix
    ax6 = plt.subplot(gs[5]) # RQFT matrix
    ax7 = plt.subplot(gs[6]) # FQFT projection (DUP)
    ax8 = plt.subplot(gs[7]) # RQFT projection (INV)
    ax9 = plt.subplot(gs[8]) # FQFT projection (DUP)
    ax10 = plt.subplot(gs[9]) # RQFT projection (INV)
    ax11 = plt.subplot(gs[10]) # FQFT projection (DUP)
    ax12 = plt.subplot(gs[11]) # RQFT projection (INV)

    ax1.plot(matrix1.sum(axis=0))
    ax2.plot(matrix2.sum(axis=0))

    ax3.imshow(comp1, cmap = cm.Blues, alpha=0.5)
    ax3.imshow(comp2, cmap = cm.Reds, alpha=0.5)
    ax4.imshow(comp2, cmap = cm.Reds)
    ax5.imshow(comp3, cmap = cm.Blues, alpha=0.5)
    ax5.imshow(comp4, cmap = cm.Reds, alpha=0.5)
    ax6.imshow(comp4, cmap = cm.Reds)

    ax7.plot(matrix3.sum(axis=0))
    ax8.plot(matrix4.sum(axis=0))

    ax9.plot(matrix3.sum(axis=0)/( eps + matrix1.sum(axis=0)))
    ax10.plot(matrix4.sum(axis=0)/( eps + matrix2.sum(axis=0)))

    ax11.plot(normDup)
    ax12.plot(normInv)

    plt.show()
###########################start plot 2
    plt.clf()

    gs = gridspec.GridSpec(4,2,height_ratios=[1,1,1,1],width_ratios=[20,20])
    gs.update(left=0.05, right=0.95,top=0.95,bottom=0.05)
    ax1 = plt.subplot(gs[0]) # hmmDup
    ax2 = plt.subplot(gs[1]) # hmmInv
    ax3 = plt.subplot(gs[2]) # hmmDel
    ax4 = plt.subplot(gs[3]) # hmm DupInv

    ax5 = plt.subplot(gs[4]) # hmmDup
    ax6 = plt.subplot(gs[5]) # hmmInv
    ax7 = plt.subplot(gs[6]) # hmmDel
    ax8 = plt.subplot(gs[7]) # hmm DupInv

    ax1.plot(hmmDUP)
    ax1.plot(normDup, alpha=0.3)
#   ax1.set_set_ylim(-0.5,1.5)
    ax1.set_title('hmmDUP hyper1 vector')
    ax1.set_ylim(-2,5)
    ax3.plot(hmmINV)
    ax3.plot(normInv, alpha=0.3)
#   ax3.set_set_ylim(-0.5,1.5)
    ax3.set_title('hmmINV hyper1 vector')
    ax3.set_ylim(-2,5)
    ax5.plot(hmmDEL)
    ax5.plot(normDel, alpha=0.3)
#   ax5.set_set_ylim(-0.5,1.5)
    ax5.set_title('hmmDEL hyper1 vector')
    ax5.set_ylim(-2,5)

    ax7.plot(hmmDUPINV)
    ax7.plot(normDel, alpha=0.3)
#   ax7.set_set_ylim(-0.5,1.5)
    ax7.set_title('hmmDUPINV hyper1 vector')
    ax7.set_ylim(-2,5)

    ax2.plot(hmmDUP1)
    ax2.plot(normDup1, alpha=0.3)
#   ax2.set_set_ylim(-0.5,1.5)
    ax2.set_title('hmmDUP hyper2 vector')
    ax2.set_ylim(-2,5)

    ax4.plot(hmmINV1)
    ax4.plot(normInv1, alpha=0.3)
#   ax4.set_set_ylim(-0.5,1.5)
    ax4.set_title('hmmINV hyper2 vector')
    ax4.set_ylim(-2,5)

    ax6.plot(hmmDEL1)
    ax6.plot(normDel1, alpha=0.3)
#   ax6.set_set_ylim(-0.5,1.5)
    ax6.set_title('hmmDEL hyper2 vector')
    ax6.set_ylim(-2,5)

    ax8.plot(hmmDUPINV1)
    ax8.plot(normInv1 - normDel1, alpha=0.3)
#   ax8.set_set_ylim(-0.5,1.5)
    ax8.set_title('hmmDUPINV hyper2 vector')
    ax8.set_ylim(-2,5)

    plt.show()
'''
