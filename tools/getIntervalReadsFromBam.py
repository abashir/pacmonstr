import os, sys
import pysam
from pbcore.io.FastaIO import FastaReader


def ror_helper (ar, qseq, refseq):
    aligned_pairs = ar.aligned_pairs
            # look at first aligned base
    ap = aligned_pairs[0]
    qpos, rpos = ap
            # check to see it is a match/mismatch
    if rpos != None and qpos != None:
        if refseq[rpos] == qseq[qpos]:
            match = -1
        else:
            match = 3
    else:
        match = 3
            # set up DP initialization
    currMismatches = match
    maxMismatches = 0
    maxStartIndex = 0
    maxEndIndex   = 0
    currStartIndex = 0
    # iterate through alignment
    # calculate maximal mismatch interval
    # note, use prior on accuracy
    # - mismatches = 4, matches = -1
    # i.e. if a region has 25% error it will have 
    # positive score
    for i in range(1,len(aligned_pairs)):
        ap = aligned_pairs[i]
        qpos, rpos = ap
        if rpos != None and qpos != None:
            if refseq[rpos] == qseq[qpos]:
                match = -1
            else:
                match = 3
        else:
            match = 3
        if currMismatches > 0:
            currMismatches += match
        else:
            currMismatches = match
            currStartIndex = i
        if currMismatches > maxMismatches:
            maxMismatches = currMismatches
            maxStartIndex = currStartIndex
            maxEndIndex = i

    #print >>sys.stderr, maxEndIndex-maxStartIndex, len(aligned_pairs)
    return maxEndIndex - maxStartIndex
            # return largest block
            #print qseq
            #print rseq
  
def ror_helper2 (ar, refseq):
    qstart, qend = ar.qstart, ar.qend
    tstart, tend = ar.aend-ar.alen, ar.aend
    #for tpos in range(tstart, tend):
    #    results[5][tpos-start] += 1
            #fullqseq = ar.seq
            ### MP - these are the substrings for event detection
    qseq = ar.seq[qstart:qend+1]
    rseq = refseq[tstart:tend+1]
    return qstart, qend, tstart, tend, qseq, rseq

def getSubBam (bamfile, region, tag = "", eventFlag=False, spanningFlag=False, alignedOnlyFlag=False, edge=2000):
    global fastaDict
    chrom, start, end  = region
    tempreads = bamfile.fetch(chrom, start-1000, end+1000)
    refseq = fastaDict[chrom]
    allreadsDict = {}
    reads = []
    readSet = set()
    refs = []
    for ar in tempreads:
        if eventFlag:
            qstart, qend, tstart, tend, qseq, rseq = ror_helper2(ar, refseq)
            mismatchesWindow = ror_helper (ar, qseq, refseq)
            if mismatchesWindow < 100:
                continue
        print >>sys.stderr, "read %s" %(" ".join(map(str, [ar.qname, ar.aend, ar.aend-ar.alen,ar.alen,start,end])))
        allreadsDict[ar.qname] = ar.seq
        if not(spanningFlag) and ar.qend < len(ar.seq) - 100 or ar.qstart > 100:
            if alignedOnlyFlag:
                reads.append((ar.qname, ar.seq[ar.qstart:ar.qend]))
            else:
                reads.append((ar.qname, ar.seq))
            readSet.add(ar.qname)
            refs.append(("%s_%i_%i" %(chrom, tstart, tend), refseq[tstart:tend]))
        elif spanningFlag and ar.aend > end and ar.aend-ar.alen < start:
            if alignedOnlyFlag:
                reads.append((ar.qname, ar.seq[ar.qstart:ar.qend]))
            else:
                reads.append((ar.qname, ar.seq))
            readSet.add(ar.qname)
            refs.append(("%s_%i_%i" %(chrom, tstart, tend), refseq[tstart:tend]))
            print >>sys.stderr, "entered loop, should be appending sequence to reads"
        else:
            print >>sys.stderr, "is ar end %i > end %i: %s" %(ar.aend, end, ar.aend > end)
            print >>sys.stderr, "is ar start %i < start %i: %s" %(ar.aend-ar.alen, start, ar.aend-ar.alen < start)
            print >>sys.stderr, "is spanningFlag: %s" %(spanningFlag)



    read_fa = "reads_%s_%s.fa" %("_".join(map(str, region)), tag)
    rout = open(read_fa, 'w')
    outreadseq = "\n".join(map(lambda x: ">%s\n%s" %(x[0], x[1]), reads))
    #print "-"*90
    #print outreadseq
    #print "-"*90
    print >>rout, outreadseq
    rout.close()
        
    #reg = "%s:%i-%i" %(region[0], int(region[1])-edge, int(region[2])+edge) 
    ref_fa = "reg_%s.fa" %("_".join(map(str, region)))
    refout = open(ref_fa, 'w')
    refoutseq = "\n".join(map(lambda x: ">%s\n%s" %(x[0], x[1]), refs))
    print >>refout, refoutseq
    #faidxcmd = "samtools faidx /sc/orga/projects/HuPac/refs/ucsc.hg19.fasta %s > %s" %(reg, ref_fa)
    #os.system(faidxcmd)
    return allreadsDict.values(), reads

#def getReadsSpanning (bam, region):
#    outfn = 


#genomic_region = sys.argv[1]
#bamfns = sys.argv[2].split(",")
#regionfn = sys.argv[3]
#bam = pysam.Samfile(bamfn, "rb" )
flank_len = int(sys.argv[1]) 
eventFlag = int(sys.argv[2])
spanningFlag = int(sys.argv[3])
alignedOnlyFlag = int(sys.argv[4])

regionfn = sys.argv[5]
fafn = sys.argv[6]
bamfns = sys.argv[7:]

fastaDict = {}
for entry in FastaReader(fafn):
    ename = entry.name.split()[0]
    if "_" in ename:
        continue
    print >>sys.stderr, entry.name
    #refseq = entry.sequence
    fastaDict[ename] = entry.sequence.upper()
sys.stderr.flush()

if eventFlag > 0:
    eventFlag = True
if spanningFlag > 0:
    spanningFlag = True
if alignedOnlyFlag > 0:
    alignedOnlyFlag = True

bamfiles = []
for bamfn in bamfns:
    bamfiles.append( pysam.Samfile( bamfn, "rb" ))

print "flank_len: %s, spanningFlag: %s, eventFlag: %s, alignedOnlyFlag: %s" %(str(flank_len), str(spanningFlag), str(eventFlag), str(alignedOnlyFlag))

print regionfn
with open(regionfn) as f:
    for l in f:
        ll = l.strip().split()
        counter = 0
        rcount = []
        for bamfile in bamfiles:
            counter += 1
            readsfull, reads = getSubBam(bamfile, [ll[0], int(ll[1]), int(ll[2])], \
                                         "bam%i" %(counter), eventFlag, spanningFlag, \
                                         alignedOnlyFlag, flank_len)
            rcount.append(len(readsfull))
            rcount.append(len(reads))
        print "%s\t%s" %("\t".join(map(str, ll[0:3])), "\t".join(map(str, rcount)))

        #getSpanningReads (outbam, region)
    
