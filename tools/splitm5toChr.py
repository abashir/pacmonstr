#!/usr/bin/env python -o

import os,sys

fOuts = {}
fofn = open(sys.argv[1])
#pathOut = sys.argv[2]

for line in fofn:
	strippedfn = line.strip()
	fileIn = open(strippedfn)
	print 'reading',strippedfn
	for line in fileIn:
		splitLine = line.split()
		fOuts.setdefault(splitLine[5],{})[splitLine[1]] = ' '.join(splitLine)

for key, value in fOuts.items():
	fn = key.replace(' ','_')
	print 'writing', fn
	if not os.path.exists('./clr_%s' % fn):
		os.system('mkdir clr_%s' % fn)
	fileOut = open('./clr_%s/clr_%s.m5' % (fn,fn),'w')
	for entry in value:
		fileOut.write(entry)
	fileOut.close()

