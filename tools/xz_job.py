#!/usr/bin/env python -o

import os,sys

fnIn = sys.argv[1]
try:
    jobIDdependency = sys.argv[2]
except (SyntaxError,NameError,IndexError):
    print "No job depencies indicated. Submitting xz command to bsub..."
    jobIDdependency = -1
name = 'xz%s.sh' % os.path.abspath(fnIn).replace('/','_')

LSF = open(name,'w')
LSF.write("#!/bin/bash -l\n")
LSF.write('#BSUB -L /bin/sh \n')# name job
LSF.write('#BSUB -J xz_%s\n' % name)# name job
LSF.write('#BSUB -q scavenger\n')# indicate queue
#LSF.write('#BSUB -q alloc\n')# indicate queue
LSF.write('#BSUB -W 5:00\n')# time
LSF.write('#BSUB -n 1\n')# number of cores
LSF.write('#BSUB -o xz_%s.out\n' %  fnIn.replace('.out','_log'))
LSF.write('#BSUB -e xz_%s.err\n' % fnIn.replace('.out','_log'))
if jobIDdependency != -1:
    LSF.write('#BSUB -w \'done(%s)\'\n' % jobIDdependency)

LSF.write('\n')
LSF.write('xz -7 %s\n' % fnIn)
LSF.close()
command = 'bsub < %s' % name
print command
os.system(command)
