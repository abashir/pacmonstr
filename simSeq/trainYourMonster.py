#!/usr/bin/env python -o

#this will be an expanded version of BenchmarkITDFinder.py, using the new string
# representation of structural rearrangements and rather than simply assessing
# event identification accuracy, it will optimize that accuracy given a reference 
# genome.

#The idea to be tested is that certain settings are better in difference sequence
# contexts and for different event types. Some sort of combinatoric optimization 
# method may be useful in identifying the best approach for SV finding in troublesome
# in a provided genome.
