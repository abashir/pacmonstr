#!/usr/bin/env python -o

#This script generates n random sequences of provided length k and drops them into a fasta file, each with a serialized name.
#The reason for this script is to generate test sequences into which a tandem repeat will be introduced in the interest
# of benchmarking our tandem repeat finder.

# a simple container for collecting the history of a generated sequence through the SV and erroring steps
class sequence:
    def __init__(self):
        refSeq = ''
        curSeq = ''
        orientation = 0
        chromosome = ''
        start = -1
        end = -1
        eventTypeString = ''
    def setPosition(self,posString): #in format chr4:12-254
        self.posString = posString
        self.chromosome = posString.split(':')[0]
        self.start = int(posString.split(':')[1].split('-')[0])
        self.end = int(posString.split(':')[1].split('-')[1])
        if len(posString.split(':')) == 3:
            self.orientation = int(posString.split(':')[2])
        self.position = (self.chromosome,self.start,self.end,self.orientation)
        self.historyString = self.posString
    def setReference(self,seq):
        self.refSeq = seq.upper()
    def setCurrentSeq(self,seq):
        self.curSeq = seq
    def updateHistory(self,event):
        self.historyString = event

try:
    from classes.model.Sequence import revcomp as rc
except:
    print "pacmonstr not in $PYTHONPATH."
    exit()

indexed_hg19 = '/sc/orga/work/pendlm02/hg19/ucsc.hg19.fasta'

import pysam
targetIdx = pysam.Fastafile(indexed_hg19)

refSizes = dict(map(lambda x: x.split() ,'''chr1 249250621
chr2 243199373
chr3 198022430
chr4 191154276
chr5 180915260
chr6 171115067
chr7 159138663
chr8 146364022
chr9 141213431
chr10 135534747
chr11 135006516
chr12 133851895
chr13 115169878
chr14 107349540
chr15 102531392
chr16 90354753
chr17 81195210
chr18 78077248
chr19 59128983
chr20 63025520
chr21 48129895
chr22 51304566
chrX 155270560
chrY 59373566
chrmtDNA 16569'''.split('\n')))

from collections import Counter
import os, random , glob
import argparse,re
#uses current build of pbcore as of 2014-04-11
try:
	from pbcore.io.FastqIO import FastqReader
except ImportError:
	from pbpy.io.FastaIO import SimpleFastqReader as FastqReader

curdir = os.getcwd()

#returns a query-able string for faidx to hg19
#thought faidx requires a format chrN:start-stop, this function returns a slightly expanded format that includes orientation
def randPos_hg19(seqLen,outfmt='seq'):
    if outfmt not in ['pos','seq']:
        return -1
    import random
    if outfmt == 'pos':
        chromosome = 'chr' + str( random.choice(range(1,23)+['X']))
        start = random.randint(1,int(refSizes[chromosome])-seqLen)
        stop = start + int(seqLen)
        positionString = '%s:%i-%i' % (chromosome, start,stop)
        return positionString
    else:
        from collections import Counter
        #return a seqLen substring of the reference genome with an N content less than Nthresh
        Nthresh = 0.1
        newSeq = 'NNNNN'
        while Counter(newSeq)['N'] / float(len(newSeq)) > Nthresh:
            chromosome = 'chr' + str( random.choice(range(1,23)+['X']))
            start = random.randint(1,int(refSizes[chromosome])-seqLen)
            stop = start + int(seqLen)
            positionString = '%s:%i-%i' % (chromosome, start,stop)
            newSeq = targetIdx.fetch(region=positionString)
            newSeq = newSeq.upper()
            if random.choice([0,1]) ==0:
                newSeq = rc(newSeq)
                positionString += ':-1'
            else:
                positionString += ':1'
        return positionString, newSeq
    return -1
    
def uniqueFilename ( length):
    from uuid import uuid4
    return str(uuid4()).replace('-','')[:length]

#Necessary information to perform transform:
# seq
# type of transformation
# (start, stop), noting that if insertion then start == stop
# if additive (insertion, duplication, proximal duplication) origin sequence (start,stop)
# seqHist = string describing the transformations that have been applied to the reference.
# for this, a parsable syntax is required, and is described here.
# first, a chromosome location as ##:start-stop:1/-1, e.g. 14:106000000-106010000:-1
# then, each event is indicated by its eventType string, I, T, D, etc.
# thereafter, each string has coordinates relative the current state of the string passed to it (as opposed to the universal coordinates in the genome) that describe the transformation.
# Because each type of transformation is slightly different, the meaning of those coordinates differ depending on eventType.
# returns a transformed sequence and a parsable string describing the transformation

def stochasticSequenceTransform(seq,seqHist, eventType, minEventSize = 0, maxEventSize = 50):
    L = len(seq)
    locusRE = re.compile('(?P<ch>chr[X,M,1-9][0-9]*):(?P<start>[1-9][0-9]*)-(?P<end>[1-9][0-9]*):(?P<strand>-{0,1}1)')
    locus = locusRE.search(seqHist)
    # Insertion format: I(size,position), e.g. I(string,15433) would insert string at position 15433 in seq
    if eventType == 'I':
        position = int(len(seq)/2)
        #position = random.randint(0,L - maxEventSize)
        insertion = ''.join([random.choice('ACGT') for _ in range(random.randint(minEventSize,maxEventSize))])
        seq = seq[:position] + insertion + seq[position:]
        return seqHist+'.I(%s,%i)' % (insertion,position),seq, '%s\t%i\t%i\tINS\t%i\n' % (locus.group('ch') , int(locus.group('start')) + position, int(locus.group('start')) + position + 1, size) 
    # TandemDuplication format: T(start,end), e.g. T(1540,1600) is a duplication of the substring 1540-1600 into position 1601. 
    if eventType == 'T':
        size = random.randint(minEventSize,maxEventSize)
        start = int( start - size / 2 )
        end = int( start + size / 2 )
        seq = seq[:end] + seq[start:]
        return seqHist+'.T(%i,%i)' % (start,end),seq, '%s\t%i\t%i\tDUP\t%i\n' % (locus.group('ch') , int(locus.group('start')) + start, int(locus.group('start')) + end, size) 
    # Deletion format: D(start,end), e.g. D(1540,1600) is a deletion of the substring 1540-1600 in
    elif eventType == 'D':
        size = random.randint(minEventSize,maxEventSize)
        start = int( len(seq)/2 - size / 2 )
        end = int( len(seq)/2 + size / 2 )
        seq = seq[:start] + seq[end:]
        return seqHist + '.D(%i,%i)' % (start,end) , seq , '%s\t%i\t%i\tDEL\t%i\n' % (locus.group('ch') , int(locus.group('start')) + start, int(locus.group('start')) + end, size) 
    #proximally duplicated substring
    elif eventType == 'Pt':
        size = random.randint(minEventSize,maxEventSize)
        start = int(len(seq)/2 - size / 2 )
        end = int(len(seq)/2  + size / 2 )
        newDeltaPosition = int(random.expovariate(1/200.)) #biased strongly to be a more proximal duplication
        if random.choice([True,False]):
            newPosition = max([start - newDeltaPosition,0])
        else:
            newPosition = min([L, end + newDeltaPosition])
#        newPosition = random.randint(0,L)
        seq = seq[:newPosition] + seq[start:end] + seq[newPosition:]
        return seqHist + '.Pt(%i,%i,%i)' % (start,end,newPosition) , seq , '%s\t%i\t%i\tPt\t%i\n' % (locus.group('ch') , int(locus.group('start')) + start, int(locus.group('start')) + end, size) 
    #proximally duplicated inverted substring
    elif eventType == 'Pti':
        size = random.randint(minEventSize,maxEventSize)
        start = int(len(seq)/2  - size / 2 )
        end = int(len(seq)/2 + size / 2 )
        newDeltaPosition = int(random.expovariate(1/200.)) #biased strongly to be a more proximal duplication
        if random.choice([True,False]):
            newPosition = max([start - newDeltaPosition,0])
        else:
            newPosition = min([L, end + newDeltaPosition])
#        newPosition = random.randint(0,L)
        seq = seq[:newPosition] + rc(seq[start:end]) + seq[newPosition:]
        return seqHist + '.Pti(%i,%i,%i)' % (start,end,newPosition) , seq, '%s\t%i\t%i\tPti\t%i\n' % (locus.group('ch') , int(locus.group('start')) + start, int(locus.group('start')) + end, size) 
    #The Pd and Pdi functions require the ability to reliably identify sequences that are duplicated 
    # in the reference seq. As such, they will have a more complicated implementation and won't 
    # initially be tested.
    elif eventType == 'Pd':
        pass
    elif eventType == 'Pdi':
        pass
    # Inversion format: V(start,end), e.g. V(1540,1600) is an inversion of the substring 1540-1600.
    elif eventType == 'V':
        size = random.randint(minEventSize,maxEventSize)
        start = len(seq) / 2 - size/2 
        end = len(seq) / 2 + size/2 
        seq = seq[:start] + rc(seq[start:end]) + seq[end:]
        return seqHist + '.V(%i,%i)' % (start,end) , seq, '%s\t%i\t%i\tINV\t%i\n' % (locus.group('ch') , int(locus.group('start')) + start, int(locus.group('start')) + end , size) 
    # substition format: S(105,'A') changing whatever is at position 105 to an A
    elif eventType == 'S':
        position = random.randint(0,L)
        base = random.choice('ACGT')
        return seqHist + '.S(%i,%s)' % (position,base) , seq
    # Translocation format t(14:
    # replacement of sequence from random position to end with equivalent length substring from elsewhere in genome
    elif eventType == 'Ct': 
        position = random.randint(0,L)
        if random.choice([0,1]):
            newPos , newSeq =randPos_hg19(L-position,'seq')
            seq = seq[:position] + newSeq
            return seqHist + '.Ct(3prime,%s)' %newPos , seq
        else:
            newPos , newSeq =randPos_hg19(position,'seq')
            seq =  newSeq + seq[position:]
            return seqHist + '.Ct(5prime,%s)' %newPos , seq
    # Translocated duplication (non-local): 
    elif eventType == 'Td':
        position = int( len(seq) / 2)
        position = random.randint(0,L - maxEventSize)
        newPos, newSeq =randPos_hg19(random.randint(minEventSize,maxEventSize),'seq')
        seq = seq[:position] + newSeq + seq[position:]
        return seqHist + '.Td(%i,%s)' % (position,newPos), seq, '%s\t%i\t%i\tTD\t%i\n' % (locus.group('ch') , int(locus.group('start')) + position, int(locus.group('start')) + len(newSeq) + position, len(newSeq)) 
    return -1


parser = argparse.ArgumentParser(description='calculate kmer pairs for genera in a list')
parser.add_argument('-l', type=int,default = 200, help='length of random sequences to be generated in bases.')
parser.add_argument('-n', type=int,default = 10**4, help='number of random sequences to be generated. Max is 9999.')
parser.add_argument('-minSize', type=int, help='Minimum event size to be generated.')
parser.add_argument('-maxSize', type=int, help='Maximum event size to be generated.')
parser.add_argument('-o', type=str, default = uniqueFilename(8) , help='File out prefix. If no name is provided a \
 random one will be generated. Files outputted will be a reference .fasta, a .fasta with tandem duplications and a \
.dat file with the format seqName,itdStart,itdEnd, a scrambled .fasta corresponding to the tandem duplication fasta,\
 but subjected to pbsim sequence degenerator.')
parser.add_argument('-t', type=str, default = 'T' , help='A string indicating which kinds of events to include in each sequence.Separate different kinds of events with a comma.')
parser.add_argument('-c', type=int, default = 1 , help='The maximum number of events permitted in a sequence. If event\
 type string includes more than one type, each event, its size and position will be chosen from among the permitted types at random\
 as per other parameters. In a set of reads, event count will be uniformly distributed between 1 and -c value.')

#Td = non-local translocated duplication
#S = point substition
#Ct = chromosomal translocation
#I=insertion
#D=deletion
#T=duplication in tandem
#Pt=proximally duplicated substring ---subsumed into duplication
#Pti = Pt inverted
#Pd = proximally deleted substring
#Pdi = Pd inverted
#V = Inversion

def deterministicSequenceTransform(seq, operationString):
    pass

args = parser.parse_args()

#a set of available event types to introduce into sequences
#Accepted input format is catted event types, as : TdSCtDITPtPtiPdPdiV
types = set()
if args.t == 'all':
    types = set(['Td','S','Ct','I','D','T','Pt','V']) #leaving out Pd and Pdi, because they're hard
else:
    for event in args.t.split(','):
        types.add(event.strip())

print types

# in the future this may be updated to reflect the exponential read length distribution seen with PB

seqs= []
for x in range(min(args.n, 9999)):
    seq = sequence()
    seq.setReference('NNNNN')
    if x % 100 == 0:
        print 'generated %i of %s sequences' % (x , args.n)
    while Counter(seq.refSeq)['N'] / float(len(seq.refSeq)) > .1:
        output = list(randPos_hg19(2* args.l,'seq'))
        seq.setReference(output[1])
        seq.setCurrentSeq(output[1])
        seq.setPosition(output[0])
    seqs.append(seq)

[bedFN, samFN, refFN , SVFN , datFN, degFN] = [args.o + x for x in ['.bed','.sam','_ref.fasta','_SV.fasta','.dat','_deg.fastq']]
bedFileOut = open(bedFN, 'w')
refFileOut = open(refFN,'w')
ITDFileOut = open(SVFN,'w')
datFileOut = open(datFN,'w')
degFileOut = open(degFN,'w')
#datfile format: >seqName\nSV_string\n
#datFileOut.write('sequence name,ITD start,ITD end,ITD length,inter-ITD distance,ITD sequence\n') #header for dat file

baseFileNames = []

for x in range( len(seqs)):
    fname = 'testSequence_%s|%s' % (str(1 + x),seqs[x].posString)
    baseFileNames.append(fname)
    refFileOut.write( '>%s\n%s\n' % (fname , seqs[x].refSeq) )
    for event in range(random.randint(0,args.c)):
        nextType = random.choice(list(types))
        print nextType
        output = stochasticSequenceTransform(seqs[x].refSeq, seqs[x].historyString, nextType,minEventSize = args.minSize,maxEventSize = args.maxSize)
#        print output
        seqs[x].setCurrentSeq(output[1])
        seqs[x].updateHistory(output[0])
        bedFileOut.write(output[2])
        print seqs[x].historyString
    ITDFileOut.write( '>%s_SV\n%s\n' % (fname , seqs[x].curSeq) )    
    datFileOut.write('>%s\n%s\n' %(fname,seqs[x].historyString))

bedFileOut.close()
refFileOut.close()
ITDFileOut.close()
datFileOut.close()
tempDir = os.path.expanduser('/sc/orga/scratch/pendlm02/tmp/')
if os.path.exists(tempDir) != True:
    os.mkdir(tempDir)

os.system('cp %s %s' % (SVFN , tempDir))
os.chdir(tempDir)
cmd = 'pbsim --data-type CLR --depth 50 --model_qc ~/ThirdParty/pbsim-1.0.3-Linux-amd64/data/model_qc_clr --length-min %s --length-max 100000 --length-mean %s --length-sd 100000.0 --prefix testSequence_ %s' % (args.maxSize + args.l, args.l + args.maxSize, SVFN)
print cmd
os.system(cmd)

#this will be keyed on 
degSeqs = {}
seqNumber = 0

for num, fn in enumerate ( glob.iglob('testSequence_*maf')):
    originName = seqs[0]
    baseName = fn.replace('.maf','')
    largest = 1
    read = ''
    Lread_name =''
    Lread = ''
    Lref = ''
    for l in open(fn):
        ll = re.sub('\s+',' ',l.strip()).split(' ')
        if len(ll) != 7:
            continue
        if ll[1] == 'ref':
            ref = ll[6].replace('-','') 
        else:
            read = ll[6].replace('-','')
        if len(read) > largest:
            Lread = read
            Lref = ref
            Lread_name = ll[1]
            largest = len(Lread)

    for entry in FastqReader(baseName + '.fastq'):
        if entry.name == Lread_name:
            degFileOut.write('@%s\n%s\n+%s\n%s\n' % ('_'.join(entry.name.split('_')[:2]), entry.sequence,'_'.join(entry.name.split('_')[:2]),entry.quality))

degFileOut.close()
cmd = 'blasr %s %s -sa %s.sa -bestn 1 -nCandidates 15 -sdpTupleSize 6 -affineAlign -noSplitSubreads -nproc 8 -sam -clipping soft -out %s' % (degFN, indexed_hg19, indexed_hg19, samFN)
print cmd
os.system(cmd)
bamFN = samFN.replace('.sam','.sorted.bam')
cmd = 'samtools view -bS %s | samtools sort - %s' % (samFN, bamFN)
print cmd
os.system(cmd)
cmd = 'samtools index %s' % bamFN
print cmd
os.system(cmd)

#os.chdir(tempDir)
#os.system('rm *')



        
