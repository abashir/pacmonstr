import os
import sys
import numpy as np
import matplotlib.pyplot as plt

f = open(sys.argv[1])

trDict = {}
f.readline()
for line in f.xreadlines():
    ll = line.strip().split("\t")
    #print line
    #print ll
    chrom, start, end = ll[0:3]
    #freq = float(ll[5])
    freq = ll[5]
    seq = ll[-1]
    #regionTup = (chrom, int(start), int(end), float(freq))
    regionTup = (chrom, start, end, freq)
    trDict.setdefault(seq, []).append(regionTup)

periodDiffs = {}
maxperiod = 40
maxfreqdiff = 100
heatmap = np.zeros((40, 100))
counter = 0
for tr, regs in trDict.items():
    if len(regs) < 2:
        continue
    counter +=1
    if counter % 100 == 0:
        print >>sys.stderr, counter
    print "%s,%s" %(tr, ",".join(map(lambda x: " ".join(x), regs)))
    period = len(tr)
    if period >= maxperiod:
        continue
        
    currDiffs = periodDiffs.setdefault(period, [])
    freqs = map(lambda x: float(x[3]), regs)
    for i in range (min(len(freqs)-1, 500)):
        for j in range(i+1, min(len(freqs), 500)):
            diff = abs(freqs[j]-freqs[i])
            currDiffs.append(diff)
            if diff < maxfreqdiff:
                heatmap[period][int(diff)] += 1

heatmap = np.log(heatmap)
plt.imshow(heatmap)
plt.colorbar()
plt.ylabel("Period Size")
plt.xlabel("Multiplicity Difference")
plt.title("TR period size vs. multiplicity difference:\n Regions with the same TR element") 
plt.savefig("tr_mult_diff.png")
plt.savefig("tr_mult_diff.pdf")
plt.savefig("tr_mult_diff.svg")
plt.show()
            


            

