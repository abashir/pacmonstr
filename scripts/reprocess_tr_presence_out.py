import os
import sys
import numpy as np

# take in a summary line with header roughly of format:
#TR_Start TR_End TR_Period TR_RefNum RefTR_minus_RefTR 3dpTR_minus_RefTR NaiveTR_minus_RefTR pHmmLocalTR_minus_RefTR pHmmGlobalTR_minus_RefTR tandemRepeatFinderTR_minus_RefTR ...
#18250809 18250896 4 22.0 0.00 -2.25 -2.00 -2.91 -2.94 -7.80
#18250809 18250896 4 22.0 0.00 -3.50 -5.00 -3.08 -5.44 -13.30
#...

f = open (sys.argv[1])

#header = f.readline()
#hlist = header.strip().split()
#print "TR_Start TR_End TR_Period TR_RefNum Divergence Method"
periodFreqDict = {}
for line in f.xreadlines():
    ll = line.strip().split()
    #start, end, period, freq = int(ll[0]), int(ll[1]), int(ll[2]), float(ll[3])
    start, end, period, freq, trfpres, totalreads= ll[0], ll[1], ll[2], ll[3], ll[4], ll[5]
    #print ll
    #print presence
    period = int(period)
    p = period
    f = int(float(freq))
    periodFreqDict.setdefault(period, {})
    trfpres = int(trfpres)
    totalreads = int(totalreads)
    for i in range(totalreads):
        if i < trfpres:
            periodFreqDict[p].setdefault(f, []).append(1)
        else:
            periodFreqDict[p].setdefault(f, []).append(0)
    #if presence == "0":
    #    periodFreqDict[p].setdefault(f, []).append(0)
    #else:
    #    periodFreqDict[p].setdefault(f, []).append(1)

print "Period Multiplicity Presence"
for p in periodFreqDict:
    for f in periodFreqDict[p]:
        print p, f, np.mean(periodFreqDict[p][f])
