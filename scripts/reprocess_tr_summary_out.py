import os
import sys
import numpy as np

# take in a summary line with header roughly of format:
#TR_Start TR_End TR_Period TR_RefNum RefTR_minus_RefTR 3dpTR_minus_RefTR NaiveTR_minus_RefTR pHmmLocalTR_minus_RefTR pHmmGlobalTR_minus_RefTR tandemRepeatFinderTR_minus_RefTR ...
#18250809 18250896 4 22.0 0.00 -2.25 -2.00 -2.91 -2.94 -7.80
#18250809 18250896 4 22.0 0.00 -3.50 -5.00 -3.08 -5.44 -13.30
#...

f = open (sys.argv[1])

header = f.readline()
hlist = header.strip().split()
#print "TR_Start TR_End TR_Period TR_RefNum Divergence Method"
periodFreqMethodDict = {}
for line in f.xreadlines():
    ll = line.strip().split()
    #start, end, period, freq = int(ll[0]), int(ll[1]), int(ll[2]), float(ll[3])
    start, end, period, freq = ll[0], ll[1], ll[2], ll[3]
    period = int(period)
    f = float(freq)
    if period < 15:
        #print "yo"
        p_temp = (period-1)/5
        p = "%i-%i" %(p_temp*5+1, 5*(p_temp+1))
        f = (int(f)-1)/10
        f = f *10
        periodFreqMethodDict.setdefault(p, {})
        periodFreqMethodDict[p].setdefault(f, {})
        for i in range(4, len(ll)):
            method = hlist[i].split("_")[0]
            periodFreqMethodDict[p][f].setdefault(method, [])
            periodFreqMethodDict[p][f][method].append(abs(float(ll[i])))
    #for i in range(4, len(ll)):
    #    print "%s %s %s %s %s %s" %(start, end, period, freq, ll[i], hlist[i])

print "period freq Method mean_divergence median_divergence"        
for p in periodFreqMethodDict:
    for f in periodFreqMethodDict[p]:
        for m in periodFreqMethodDict[p][f]:
            print p, f, m, np.mean(periodFreqMethodDict[p][f][m]), np.median(periodFreqMethodDict[p][f][m])
