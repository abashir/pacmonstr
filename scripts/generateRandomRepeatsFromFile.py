import os
import sys
import numpy as np
import bisect

pstart, pend, pstep = map(int, sys.argv[1:4])
fstart, fend, fstep = map(int, sys.argv[4:7])
randstart, randend, randstep = map(int, sys.argv[7:10])
replicates = int(sys.argv[10])

ps = range(pstart, pend, pstep)
fs = range(fstart, fend, fstep)
svs = range(randstart, randend, randstep)

f_in = open(sys.argv[11])

nucs = "ACTG"

counter = 0

repDict = {}
for p in range(pstart, pend+1, pstep):
    repDict.setdefault(p, {})
    for f in range (fstart, fend+1, fstep):
        #repDict[p].setdefault(f, {})
        #for sv in range(randstart, randend, randstep):
        repDict[p][f] = 0
            

for line in f_in.xreadlines():
    ll = line.strip().split()
    #q, t, start, end, rep = ll[
    rep = ll[4]
    fstr = ll[6]
    pstr = ll[15]
    currf = float(ll[6])
    refseq = ll[11]
    qseq = ll[12]
    currp = float(ll[15])
    #print refseq
    #print qseq
    #print rep, currf, currp
    pind, find = bisect.bisect(ps, currp), bisect.bisect(fs, currf)
    #print pind, find            
    #print rep, currf, currp
    #print ps
    #print fs
    if pind < len(ps) and find < len(fs):
        p, f = ps[pind], fs[find]
        #print "if1"
        if repDict[p][f] < replicates:
            q = "".join(qseq.split("-"))
            for sv in range(randstart, randend, randstep):
                counter += 1
                seqTag = "seq_%i_p_%i_f_%i_sv_%i_r_%i_truep_%s_truef_%s" %(counter, p, f, sv, repDict[p][f], pstr, fstr)
                svseq = "".join(map(lambda x: nucs[x], np.random.randint(0, 4, sv)))
                print "%s %s%s %s" %(seqTag, q, svseq, rep)
            repDict[p][f] += 1
    
                
    
