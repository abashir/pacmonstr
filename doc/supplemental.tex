\documentclass[a4paper,11pt]{article} \usepackage[left=1in, right=1in,
  top=1in, bottom=1in]{geometry} \usepackage{amsmath}
\usepackage{color} \usepackage{graphicx} \usepackage{amsfonts}
\usepackage{algorithm} \usepackage{algorithmic}

\usepackage{lscape} \usepackage[dvipsnames]{xcolor}
\renewcommand{\algorithmiccomment}[1]{// \textit{#1}}
\newcommand{\comment}[1]{\textcolor{red}{[#1]}}

\newenvironment{packed_enumerate}{
\begin{enumerate}
  \setlength{\itemsep}{1pt} \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt} }{\end{enumerate}}

\newenvironment{packed_trivlist}{
\begin{trivlist}
  \setlength{\itemsep}{1pt} \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt} }{\end{trivlist}}

\newcommand{\ali}{\textcolor{blue}}
\newcommand{\ben}{\textcolor{green}}
\newcommand{\anna}{\textcolor{purple}}
\newcommand{\suzanne}{\textcolor{orange}}
\newcommand{\alicom}{\textcolor{red}} \mathchardef\mhyphen="2D
% Begin ...
\title{Supplemental Materials} \author{Ajay Ummat, Ali Bashir}
\date{\today}

\begin{document}
\maketitle

\section{Methods}
The method begins with alignment of a set of reads $\mathcal{Q}$ to
the reference.  Figure~\ref{fig:tr_aln} shows an example of this
process.  Reads are first aligned via blasr~\cite{chaisson2012mapping}
to a known reference, here hg19.  The reads are then filtered so as to
only accept reads with a single unique mapping location to the
reference (this avoids potential mismappings due to segmental
duplications and mitigates confounding effects from proximally located
structural variants).  The known set of TR regions are then overlayed
on top of this to identify potential reads spanning events, an example
read $Q_i$ is shown in Figure~\ref{fig:tr_aln}B spanning a tandem
repeat, $R$, that occurs from positions $\alpha$ to $\beta$ in the
reference genome $G$.  By using the full alignment information, we can
then get a coarse boundary of the reads, $a,b$ correspond to the
alignment to reference position $\alpha, \beta$.  Identifying this
coarse boundary prior to re-estimation serves 2 key purposes.  First,
it substantially speeds up subsequent quadratic time steps by reducing
the amount of sequence passed in for full alignment.  Second, by
partitioning the reads at each TR a single read is able to potentially
predict multiple TR events (as opposed to being skewed by a single
large TR event) by separating TR assignment into multiple subproblems
{\bf (XXX Ajay does this make sense/is this true?  It seems to me that
  by splitting around the flanking intervals, we decouple
  non-overlapping TRs- to generalize this we should split the reads by
  all non-overlapping TR positions and then take either the min of the
  flanking sequence cutoff length OR the gaps between TRs as our
  flanking sequence)}.

Let the amount of sequence required to flank a given TR interval be
denoted by $\epsilon$.  We define (via the initial pairwise alignment)
a pair of alignment positions in the query $a-e_{u}, b+e_{d}$
corresponding to $\alpha-\epsilon,beta+\epsilon$.  The spanning query
interval, $Q_i[a-e_{u},b-e_{d}]$, along with $G[\alpha-\epsilon,
  \alpha], r, G[\beta, \beta+\epsilon]$ are passed into a TR specific
dynamic programming algorithm to improve boundary resolution.

\begin{figure}[htbp]
\centering
\includegraphics[width=.8\textwidth]{figures/alignment_overview.png}
\caption{Alignment of PacBio to known TR interval}
\label{fig:tr_aln}
\end{figure}

\subsection{Identifying TR intervals}
\label{sec:trdp}
The substring $Q_i[a,b]$ In order to bound the TR to a specific
interval we break up the alignment problem into a prefix, repeat,
suffix alignment problem.  For simplicity, let $p$, $r$, and $s$,
correspond to the prefix, repeat and suffix sequences and $P$, $R$,
and $S$ correspond to their alignement to the query, $q$.  A schematic
of this alignment strategy is shown in
Figure~\ref{fig:prefix_suffix_dp}.

We begin with alignment of query to the prefix utilizing a variant of
the Needleman-Wunsch algorithm~\cite{needleman1970}:
\begin{equation*}
  P_{i,j} = \max
  \begin{cases}
    P_{i-1, j-1} + m\\ P_{i-1, j} + a\\ P_{i, j-1} + a\\
  \end{cases} 
\end{equation*}
In our application we are not solely interested in the position in
which the entire query aligns to the prefix, rather we are interested
in {\em any} position in the query for which a high-quality alignment
reaches the end prefix sequence. This position can serve as a
corresponding entry point into the repeat matrix.  Thus, any position
in the repeat matrix, $R_{i,j}$ must consider the posibility of having
just entered from the prefix, $ P(i-1,|p|) + m$.  Note, that we cannot
assume a priori that the repeat starts at the beginning of the
predicted consensus copy.  The key distinguishing feature of the
repeat matrix is its ability to traverse the repeat unit multiple
times.  For any $R_{i,j}$ we capture this by allowing a move from the
last index of the the repeat matrix, $|r|$, into any position in the
following row, $R_{i-1, |r|} + (j-1)a + m $, penalizing by any
insertion gaps that this movement induces.  In addition to enabling
unbiased expansion and contraction of the repeat, it also
substantially reduces the time complexity in the case of very highly
expanded repeats (the most challenging types of TRs to resolve).
Combining this with the standard Needleman-Wunsch recurrence yields:
\begin{equation*}
  R_{i,j} = \max
  \begin{cases}
    P_{i-1,|p|} + m\\ R_{i-1, j-1} + m\\ R_{i-1, j} + a\\ R_{i, j-1} +
    a\\ R_{i-1, |r|} + (j-1)a + m\\
  \end{cases}
\end{equation*}

Lastly, we must resolve the suffix sequence.  Under the assumption
that no sequence was lost at the junction we have a greatly simplified
recurrence: we only need a single special case to consider entries
into the first column of the suffix matrix from the repeat matrix:
\begin{equation*}
S_{i,1} = \max_{0 \leq k \leq r} {R(i-1,k) + m)}\\.
\end{equation*}
Once this column has been filled we can reuse the standard recurrence
for the remainder of values, $S_{i,j}$:
\begin{equation*}
  S_{i,j} = \max
  \begin{cases}
    S_{i-1, j-1} + m\\ S_{i-1, j} + a\\ S_{i, j-1} + a\\
  \end{cases}
\end{equation*}

To obtain our predicted TR sequence, in $q$, we simply retrace the
optimal path through $P$, $R$, and $S$ and retrun the positions in $q$
correspondings to the entries points in $R$ and $S$, yielding our new
query repeat sequence $q_r$.  Additionally, the multiplicity of the
repeat $r$ in $q$ is given directly by this traversal; it is roughly
one more than the number of times the last case of the repeat
recurrence is used in the optimal traversal path.  The time complexity
of the method is given by the $O(qp) + O(qs) + O(qr)$.  As the prefix
and suffix lengths are constrained by the inputted flanking criteria
they are constants, leading to time complexity $O(pr)$.  In practice,
$r << p$ so this will be substantially faster than $p^2$.

\begin{figure}[htbp]
\centering
\includegraphics[width=.8\textwidth]{figures/prefix_tr_suffix_dp.png}
\caption{Three-stage dynamic programming strategy to resolve repeat
  boundaries.}
\label{fig:prefix_suffix_dp}
\end{figure}

\subsection{Probabilistic estimation of TR counts}

\subsubsection{{\em pair}HMM structure}
This revised estimation from Section~\ref{sec:trdp}, though more
precise in terms of the boundaries of the TR element, gives wide
estimates in predicted TR lengths.  The previous approach is limited
in two ways.
\begin{enumerate}
\item{It does not allow for sequence context specific error-modes
  uniques to the sequencer, such as cognate-sampling~\cite{Eid2009,
    ono2013}.}
\item{It does provide probabilities for predicting alternative TR
  frequencies.}
\end{enumerate}
In order to better estimate these we propose a {\em pair}HMM approach
that does not simply return the optimal path, but rather considers the
{\em sum} of all alignment paths between the sequence from the query
TR interval and a putative repeat array in the reference.


The complete parameter set for the pairHMM for global alignment is
defined as follows (in the notations of Durbin
et. al~\cite{durbin1998}):
\begin{eqnarray*}
M &=& \hbox {match state}\\ X &=& \hbox{insertion state}\\ Y &=&
\hbox{deletion state}
\end{eqnarray*}
Where each observation, $o_i$, corresponds to an aligned pair of
symbols from the set of symbols $V= \{A, C, T, G, \mhyphen\}$, thus,
$o_i \in \{a:b \; \forall \; a \in V, b \in V\} \setminus
\{\mhyphen:\mhyphen\}$ as shown in Figure~\ref{fig:obs_seq}.  The
model structure is shown in Figure~\ref{fig:globalPairHmm}.

\begin{figure}[htbp]
\centering \includegraphics[width=.8\textwidth]{figures/Oseq.png}
\caption{An observation sequence }
\label{fig:obs_seq}
\end{figure}
Figure~\ref{fig:pairHMM} provides a schematic of how we estimate the
probability of observing a particular TR multiplicity.
\begin{figure}[htbp]
\centering
\includegraphics[width=.8\textwidth]{figures/pairHmm_tr_estimation.png}
\caption{Assignment of TR copy-numbers from pairHMM.}
\label{fig:pairHMM}
\end{figure}


\begin{figure}[htbp]
\centering
\includegraphics[width=.8\textwidth]{figures/modelStructure.png}
\caption{A 3-state pairHMM for global alignment }
\label{fig:globalPairHmm}
\end{figure}
Transition probabilities reflect the error-profile of the sequencer.
Often the transition (and emission) probabilities can be estimated
using the Baum-Welch algorithm~\cite{}, provided we have a reliable
observation sequence.  However, here we do not know the `truth'
sequence (i.e. the actual number of copies for the tandem repeat) and
instead estimate these probabilities from the globally from all
alignments generate by Blasr {\bf XXX, Ajay, modify this base on what
  we finally do}.  In practice these values are given by
Table~\ref{tab:transprob}

\begin{center}
  \begin{tabular}{| l | c | c | c | }
    \hline {\bf State} & {\bf $M$ } & {\bf $X$} & {\bf $Y$} \\ \hline
           {\bf $M$} & 0 & 0 & 0 \\ {\bf $X$} & 0 & 0 & 0 \\ {\bf $Y$}
           & 0 & 0 & 0\\ \hline
    
  \end{tabular}
  \label{tab:transprob}
\end{center}

\subsubsection{Emission dependencies}
In SMRT sequencing data, Eid et al. observed the issue of `cognate
sampling', whereby the emitted base has a dependence on the last
incorporated nucleotide~\cite{Eid2009}.  We handle this in our model
by incorporating conditional dependence in emission probabilities when
calculating the forward probability values (for the insertion state,
X).  Let $S=s_1,...s_T$ be a state sequence and $P(O|\lambda)$ be the
probability of observing a sequence $O=o_1,...o_T$ given the model
parameters $\lambda={A, B, \pi}$.  It is calculated by:
\begin{eqnarray*}
P ( O \mid S, \lambda ) &=& P ( o_T, o_{T-1},...,o_1 \mid S, \lambda )
\\ &=& P(o_T \mid o_{T-1},...,o_1, S, \lambda) ...P(o_{i} \mid
o_{i-1},...,o_1, S, \lambda)..P(o_{1} \mid S, \lambda)
\end{eqnarray*}  
Assuming first order dependencies in the observation sequences we can
simplify this expression:
\begin{eqnarray*}
P ( O \mid S, \lambda ) &=& P(o_T \mid o_{T-1}, S, \lambda) ...P(o_{i}
\mid o_{i-1}, S, \lambda)..P(o_{1} \mid S, \lambda)
\end{eqnarray*}  
Which is further simplified conditioning only on the current state:
\begin{eqnarray*}
P ( O \mid S, \lambda ) &=& P(o_T \mid o_{T-1}, S_T, \lambda)
...P(o_{i} \mid o_{i-1}, S_i, \lambda)..P(o_{1} \mid S_1, \lambda)
\\ &=& e_{S_T}(o_T \mid o_{T-1}) ...e_{S_i}(o_{i} \mid
o_{i-1})..e_{S_1}(o_{1})
\end{eqnarray*}  

We can now calculate $P(O \mid \lambda)$:

\begin{eqnarray*}
P ( O \mid \lambda ) &=& \sum_{\forall S} {P(O \mid S, \lambda) P(S
  \mid \lambda)} \\ &=& \sum_{s_1, s_2,..s_T}{ \pi_{b \rightarrow s_1}
  e_{s_1} (o_1) a_{s_1 \rightarrow s_2} e_{s_2} (o_2 | o_1)
  ... a_{s_T-1 \rightarrow s_T} e_{s_T} (o_T | o_{T-1})}
\end{eqnarray*}  

We restrict the conditional dependence in emission probabilities when
$S=X$ (insertion state).  We can now apply the forward algorithm to
calculate $P(O \mid \lambda)$.

\subsubsection{{\em pair}HMM traversal}

We use a slightly modified form of the standard recurrence for pair
HMMs given by Durbin et al. For forward calculations we incorporate
transitions between the insertion and deletion states ($X$ and
$Y$). These transitions account for a particular error mode of the
sequencer, where in, a nucleotide insertion event is followed
immediately by a deletion (or vice versa). In the recurrsion relations
for the forward algorithm for pairHMMs below, $f_S(i ,j)$ represents
the sum of probabilities of all the alignments in the observation
sequence up to $(i, j)$ ending in the state $S$ (here, $S=M, X, Y$).

\begin{eqnarray*}
P ( O \mid \lambda ) &=& \sum_{\forall S} {P(O \mid S, \lambda) P(S
  \mid \lambda)} \\ &=& \sum_{s_1, s_2,..s_T}{ \pi_{b \rightarrow s_1}
  e_{s_1} (o_1) a_{s_1 \rightarrow s_2} e_{s_2} (o_2 | o_1)
  ... a_{s_T-1 \rightarrow s_T} e_{s_T} (o_T | o_{T-1})}
\end{eqnarray*}  

 

\subsubsection{Recursion relations to calculate $P ( O  \mid \lambda)$}

Let, \\ \indent {$q$ $=$ query TR interval $(|q| = n_{q})$ indexed by
  $i$}\\ \indent {$r$ $=$ upper-bounded TR sequence $(|r| = n_{r})$
  indexed by $j$}\\ \indent {$f_S(i, j)$ $=$ sum of probability of all
  pairwise alignments uptil $(i, j)$ ending in state S, where $i = 1,
  ..., n_q$ and $j = 1, ..., n_r$ }\\

The initialization conditions are (here, Begin is the initial
state):\\ \indent {$f_M(0, 0)$ $=$ $a_{Begin \rightarrow
    M}$}\\ \indent {$f_X(0, 0)$ $=$ $a_{Begin \rightarrow
    X}$}\\ \indent {$f_Y(0, 0)$ $=$ $a_{Begin \rightarrow Y}$}\\

The elements of the transition probability matrix between the states =
Begin, Match (M), Insertion (X), Deletion (Y) and End (E) are defined
by $a_{S_l \rightarrow S_k}$.\\

The emission probability matrix for the Match state $(M)$, is given by
$e_{M} (q_i, r_j)$. Here, $q_i$ and $r_j$ belongs to the set of
symbols $W= \{A, C, T, G\}$ $\subset V $. Similarly, for the insertion
state $(X)$, the emission probability matrix is given by $e_{X} (q_i,
q_{i-1})$. The probability of emitting an observation $o_i =
\{\mhyphen:q_i\}$, is conditioned on the previous emitted base
$q_{i-1}$. A special construct of this pairHMM model is that the
emission probability when in the deletion state $(Y)$, $e_{Y} (r_j)$ =
1 (as the sequence r, given the putative true TR, is known by
definition). The recurrsion relation for the forward algorithm is:
\begin{eqnarray*}
f_M(i, j) &=& { e_{M} (q_i, r_j) ( a_{M \rightarrow M} f_M(i-1, j-1) +
  a_{X \rightarrow M} f_X(i-1, j-1) + a_{Y \rightarrow M} f_Y(i-1,
  j-1) )}
\end{eqnarray*}
\begin{eqnarray*}
f_X(i, j) &=& { e_{X} (q_i, q_{i-1}) ( a_{M \rightarrow X} f_M(i-1, j)
  + a_{X \rightarrow X} f_X(i-1, j) + a_{Y \rightarrow X} f_Y(i-1, j)
  )}
\end{eqnarray*}
\begin{eqnarray*}
f_Y(i, j) &=& { e_{Y} (r_j) ( a_{M \rightarrow Y} f_M(i, j-1) + a_{X
    \rightarrow Y} f_X(i, j-1) + a_{Y \rightarrow Y} f_Y(i, j-1) )}
\end{eqnarray*}

The forward algorithm was implemented in the logarithm space, as the
probability values for a long q or r sequences can not be represented
in the existing machine precision. This implementation has been
motivated by Eddy et al's work on HMMer 3 and by the log paper (cite
here). The algorithm has been tested for $|q| \geq$ 30 kbases.\\

\subsubsection{Expected value of TRs from forward probability matrix}

We estimate the expected TR multiplicity in the query TR interval
using the sum of alignment probabilities from the forward algorithm.
Let $P(O \mid j, \lambda)$ correspond the probability of an
observation sequence having a TR multiplicity of $j$ given the model
parameters.  Figure~\ref{fig:pairHMM} shows how we estimate this over
all relevant values of $j$ with some maximal value of $j$, $M$ (in
practice we use $M = 1.5|q|/|TR|$.  We run the forward algorithm with
the query sequence on the y-axis and an upper-bounded TR on the
x-axis.  For each $j$ we calculate sum the probabilities from the each
end state, $P(O \mid j, \lambda) = f_M(|q|, j) + f_X(|q|, j) +
f_Y(|q|, j)$.  We use these values to generate our relative
probabilities of observing a given TR multiplicity by $\frac{P(O \mid
  j, \lambda)}{\sum_k^M {P(O \mid k, \lambda)}}$ and the probabilities
as weights to inform our expected TR multiplicity, $E(Q_{TR}) =
\sum_{j=1}^M {\frac{P(O \mid j, \lambda)}{\sum_k^M {P(O \mid k,
      \lambda)}}j}$.  We also calculate the log-odds ratio with
respect to a random model~\cite{durbin1998}.

\subsubsection{Calculating lower bounds for TR size by eliminating non-repeat intervals from $Q_{TR}$}

$P ( O \mid \lambda)$ calculated by the forward algorithm provides a
basis to identify non-repeat intervals from the query TR interval $q$.

\begin{enumerate}
\item{ Calculate $max_{j = 1, ..., n_r}$ $P ( O \mid \lambda, j)_i$ for
  each index $i = 1, ..., n_q$ of $q$. These max probability values
  are stored in an array, of size $|q|$, defined by $maxProb$. Here,
  we sub-sample and store values for every $i \mod |TR|$. Further,
  we store the value of index $j$, where this max probability value is
  observed for each $i$. These index values are stored in the array
  $indexMax$. For instance, for $i = 4$, $maxProb$[$i$], will give the
  $max_{j = 1, ..., n_r}$ $P ( O \mid \lambda, j)_{i=4}$ and
  $indexMax$[$i$] will give the value of $j$ where this max is
  observed.}
\item{Calculate log-likelihood ratio $(llr)$. For calculating $(llr)$,
  we use the random model for pairHMM as defined by {\em durbin{
      (1998)}}. For each $i$, calculate the $llr_i$ = $max_{j = 1,
    ..., n_r}$ $P ( O \mid \lambda, j)_i$ - $P ( O \mid Random Model,
  j)_i$.}
\item{Deviation of $llr$ for each $i$. Calculate the deviation between
  $llr_i$ for each $i = 1, ..., n_q$ of $q$. These deviations are
  stored in diff$LLR$ array. For detecting any non-repeat interval
  (which may include, true insertions and random bases due to
  sequencing or previous TR identification step) in $q$, we require
  two criterions to be fulfilled. 
  \begin{enumerate}
  \item $llr_i > 0$, for each $i = 1,..., n_q$
  \item $llr_i - llr_{i-1} > threshold$, for each $i = 1,  ..., n_q$
  \end{enumerate}
  This results in a set of positions in $Q$ that correspond to likely TR 
  intervals, $Q_t$.
}
\item{The set of positions remaining corresponds to the random (non 
consensus TR) subintervals, $Q_{noise} = Q-Q_t$}
\end{enumerate}

An estimate for the lower bound is obtained by taking the expected multiplicity minus $|Q_{noise}|$.
In general, this serves as a conservative lower bound as the expected multiplicity should 
already under-value noise sequences.  A more accurate bound is obtained by re-running the 
pairHMM on each contiguous disjoint subinterval of bases derived from $Q_t$ and summing their expected values.

The revised probability distribution for sampling the TR multiplicity is obtained by drawing samples from individual discrete distributions and summing the obtained vector of samples, element-wise. 
{\bf The following is not required....
Let, $J_1, J_2,...,J_p$ be the random variables representing the TR multiplicity for each disjoint subinterval in the set $Q_t$. Let $F_{J_k}$ be the cumulative distribution function, $CDF$ for $J_k$. Then, $F_{J_k}(j_\alpha) = \sum_{\beta \leq \alpha} P_{J_k}(J_k = j_{\beta})$, where, $j_\alpha \in S = \{ j_1, j_2, ... ,j_m\}$ which is the sample space of the TR multiplicity values for the corresponding subinterval. As the subintervals in $Q_t$ are disjoint, the $CDF$, $F_{J_{Q_t}}$ for the joint random variable $J_{Q_t}$ $(=J_1 + J_2 + ... + J_p)$, should be = $\sum_{k \leq p} F_{J_k}$}

\subsection{TR allele calling}
%% A gaussian mixture model (GMM) based bayesian information content (BIC) is used to cluster the TR events.
%% \begin{enumerate}
%% \item{Data fitted with 1 component}
%% \item{Data fitted with 2 components}
%% \item{If the weight of any mixture is $<$ a threshold, $\tau_N$, the corresponding data
%%   is discarded and a new GMM is fit {\bf XXX Ajay, is this done for both clusters or for
%% each cluster independently?}}
%% \item{Anderson-darling test of normality is performed on the filtered dataset}
%% \item{The data is fit to a binomial distribution calculated on the weights {\bf (XXX Ajay is this simply counts?)
%%   providing some probability $b$}}
%% \item{If $b$ is less than the probability cutoff, $\tau_b$, the two mixture model is rejected}
%% \item{if $BIC_2$ < $BIC_1$ and Anderson-darling statistic is beyond a cirterion then the number of alleles=2
%%   is selected.  Else, number of alleles=1}
%% \end{enumerate}


%% \begin{algorithm}
%% \caption{GMM_TR $(D, n)$}
%% \begin{algorithmic}[1]
%% \STATE g1 = GMM (D, n) \textit{   \# run GMM with n components}
%% \If {g1.count

%% \IF {$ N_{m}.NormalProb < t_{N}$}
%% \RETURN $O$  \textit{    \# if $N_m$ fails neighbor threshold, terminate}
%% \ENDIF
%% \IF {$ N_{m}.GammaProb < t_{\gamma}$}
%% \STATE $O = O \cup N_m$ \textit{    \# if $N_m$ passes $t_{\gamma}$  threshold, it is significant}
%% \ENDIF
%% \RETURN MotifDetectPhase2 $(N \setminus N_m,t_N, t_{\gamma}, O)$ \textit{   \# get next most significant motif}
%% \end{algorithmic}
%% \end{algorithm}

Algorithm~\ref{alg:TrAlleles} describes how alleles are called.  We
assume the observed TR calls per read are normally normally
distributed around the true TR length for each allele.  We then use a
Gaussian Mixture Model (GMM) to fit the observed read distribution to
the ploidy of the genome of interest.  For diploid genomes, we allow
either 1 or 2 components in the model; corresponding to homozygous or
heterozygous alleles at each locus.  Note, that outliers are
inevitable in such analyses. To handle outlier TR estimations we
require that each component contain at least 2 reads.  If a component
represents a singleton value, we eliminate the value from the initial
set and iterate until a component with greater than 1 read is
observed.

We assume the null hypothesis is that there is only a single allele at
each locus.  We reject the null hypothesis under a specific set of
criteria.  First, we perform the Anderson-Darling {\bf EDF} test to
determine if the outlier removed read set is
normal~\cite{anderson1954}.  If the data appears non-normal using {\bf
  (the criterion, that calculated anderson-darling statistic value is
  greater than the critical value = 1.159 at the significance level of
  alpha = 0.005~\cite{D'Agostino1986})}. This is implemented using the
...  {\bf (scipy.stats function anderson, which returns the critical
  value for the test statistics. This critical value is modified by a
  correction term, 1.0 + 0.75/|D| + 2.25/(|D|*|D|), where |D| is the
  number of elements in the outlier removed read set.)}

Next, we confirm that the observed split of reads is possible under
the assumption that the reads should be partitioned across the reads
following a binomial distribution.  Note, that to accurately type a TR
event the reads must {\em span} the event in full.  Thus, the
probability of observing a read is lower if the TR is longer.  We
correct this using an empirical CDF of the read distribution, where
$S(l, \mathcal{R})$ corresponds to the survial probability of a read
of length $l$ given the set of reads $\mathcal{R}$.  Let, $T_1$ and
$T_2$ be the true TR sequence for alleles 1 and 2, respectively.  The
survival probability for reads spanning a given TR allele is
$S(|T|+2\epsilon, \mathcal{R})$, thus the probability of observing
alleles 1 or 2 are given by, $p_1 = \frac{ S(|T_1|+2\epsilon,
  \mathcal{R})}{ S(|T_1|+2\epsilon, \mathcal{R}) + S(|T_2|+2\epsilon,
  \mathcal{R})}$ and $1-p_1$.

Lastly, we compare bayesian information criterion (BIC) between the
one and two component GMM models.  The BIC attempts to determine which
of the models is a better fit to the data, by penalizing models with
more free parameters (in order to reduce the chance of
overfitting)~\cite{schwarz1978}.  We use the {\bf scikit-learns
  library's implementation and use the function GMM in 'Mixture'
  module} implementation.  In short {\bf this function defaults to a
  covariance type for the data to diagonal, and estimates the
  parameters for the gaussian distribution using
  expectation-maximization algorithms (yet to find references of
  papers/scikit-learn)}.  We run this on both the single and two
component models reject the null hypothesis if
$\textsc{Bic}(\textsc{Gmm}(D, 2)) < \textsc{Bic}(\textsc{Gmm}(D, 1))$.
%$\textsc{Bic}(\textsc{Gmm}$(D, 2)$) $<$ \textsc{Bic}(\textsc{Gmm}$(D, 1)$).

\begin{algorithm}
\caption{\textsc{TrAlleles} $(D, t_{R}, t_{a}, t_{b})$}
\label{alg:TrAlleles}
\begin{algorithmic}[1]
\STATE $R_1$ = \textsc{Gmm} $(D, 1)$ \textit{ \# run GMM with 1
  component} \STATE $R_2$ = \textsc{Gmm} $(D, 2)$ \textit{ \# run GMM
  with 2 components} \WHILE {$R_2[2] < t_R$} \STATE $D = R_2[1]$
\textit{ \# assume smaller component is outlier, set $D$ to larger
  component} \STATE $R_1$ = \textsc{Gmm} $(D, 1)$ \STATE $R_2$ =
\textsc{Gmm} $(D, 2)$ \ENDWHILE \IF {\textsc{Anderson}$(D) > t_{a} \;
  \wedge \;$\textsc{BinCdf} $(|D|, |R_2[1]|) > t_b \; \wedge \;
  $\textsc{Bic}($R_2$) $<$ \textsc{BIC}($R_1$)} \RETURN
\textsc{Quiver}$(R_2[1])$, \textsc{Quiver}$(R_2[1])$ \ELSE \RETURN
\textsc{Quiver}$(D)$ \ENDIF
\end{algorithmic}
\end{algorithm}

The final output of this algorithm is the set of reads corresponding
to each component.  A consensus sequence is then generated using the
Quiver algorithm~\cite{quiver} separately for the set of reads in each
component.  This consensus sequence represents the final TR calls for
each allele.



\section{Results}
\subsection{Assessing accuracy on a per-read basis}

Assessing accuracy on a per-read basis is a challenging task.
Specifically, in the context of complicated TRs it is challenging to
assign a single read any particular TR count without ensuring it the
estimate is biased because of a true deviation in the underlying
sequence, because of sequencing error, or because of errors in the
algorithm.  The Pacbio platform has the ability to sequence the same
molecule multiple times via the SMRTBell construct~\cite{travers2010}.
By comparing the deviation in our individual calls for the TRs
spanning CCS reads versus the consensus sequence generated by the
platform, we can get a better estimate of the expected variability in
each of our approaches.

{\bf (Ajay I want to say something like: Figure XXX shows the
  deviation of various prediction methods.  As we can see though all
  methods are fairly robust at long period sizes, the pairHMM is
  substantially more accurate for smaller value)}




\clearpage
\bibliography{pacmonstr} \bibliographystyle{plain}

\end{document}
