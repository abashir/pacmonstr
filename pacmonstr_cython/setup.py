## usage: python setup.py build_ext --inplace
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

setup(
    cmdclass = {'build_ext': build_ext},
    ext_modules =  [Extension("trFinderFuncs", ["trFinderFuncs.pyx"], include_dirs=[numpy.get_include()]), Extension("eventFilter", ["eventFilter.pyx"], include_dirs=[numpy.get_include()])]
)
