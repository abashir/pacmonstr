# cython: profile=True
# cython: wraparound=False
# cython: boundscheck=False
# cython: cdivision=True

AUTHOR = 'PENDLM02'

import numpy as np
cimport numpy as np

ctypedef np.float_t DTYPE_FLOAT_t
ctypedef np.int_t DTYPE_t
ctypedef np.uint16_t dtype_tuint

cdef inline int int_min(int a, int b): return a if a <= b else b
cdef inline int int_max(int a, int b): return a if a >= b else b

def Scoring( np.ndarray[DTYPE_FLOAT_t] proj1, np.ndarray[DTYPE_FLOAT_t] proj2, int L):
    cdef np.ndarray[DTYPE_FLOAT_t, ndim = 1] results = np.zeros(L,dtype=float)
    cdef int pos = 0
    for pos from 0 < pos <= L:
        if proj1[pos] - proj2[pos] > 0:
            results[pos] = proj1[pos] / (proj2[pos] + 1)
    return results

cdef int basePos (char nuc):
    cdef char* dna ='ATGCSWRYKMBVHDN'
    cdef int val = 0
    cdef int index = 0
    while nuc != dna[index]:
        index += 1
    return index

#since checking the maxValue of the previous row across [1:] is essential but slow, direct C implementation is provided here using memoryview function
#using start, this function generalizes to perform max and max of [1:]
cpdef int npmax ( int [:] arr,int start):
    cdef int curMax = 0
    I = arr.shape[0] #length of list
    cdef unsigned int i
    for i in range(start , I):
        if arr[i] > curMax:
            curMax = arr[i]
    return curMax

#same as above, but for argmax
cpdef int npargmax ( int [:] arr, int start):
    cdef int curMax = 0
    cdef int curArgmax = 0
    I = arr.shape[0] #length of list
    cdef unsigned int i
    for i in range(start , I):
        if arr[i] > curMax:
            curMax = arr[i]
            curArgmax = i
    return curArgmax

def generateScoringTableTR(char* localSeq, char* globalSeq,  int llen, int glen, int indelP, np.ndarray[DTYPE_t, ndim=2] dnamatrix ):
    cdef int i = 0
    cdef int g_pos, l_pos
    cdef int prevMax, prevArgMax
    cdef int am
    cdef int val
    cdef long indelP_L, glen_L
    cdef np.ndarray[DTYPE_t, ndim=1] localInd = np.zeros(llen, dtype='i8')
    cdef np.ndarray[DTYPE_t, ndim=1] globalInd = np.zeros(glen, dtype='i8')
    cdef double T = (dnamatrix[0,0] * 0.9 + 0.1 * indelP ) * glen

    cdef np.ndarray[DTYPE_t, ndim=2] S = np.zeros((llen + 1, glen + 1),dtype='int64')
    cdef np.ndarray[DTYPE_t, ndim=2] P = np.zeros((llen + 1, glen + 1),dtype='int64')

    cdef np.ndarray[DTYPE_t, ndim=1] vals = np.zeros(5, dtype='int64')
    cdef int minScorePossible = 2*indelP*llen

    # Compute Base Pos Indexing ONCE per sequence
    for i from 0 <= i < llen:
        localInd[i] = basePos(localSeq[i])
    for i from 0 <= i < glen:
        globalInd[i] = basePos(globalSeq[i])


    #initialize first row to penality values
    for g_pos from 1  <= g_pos < glen+1:#repeat
        S[0,g_pos] = S[0,g_pos-1] + indelP
        P[0,g_pos] = 1

    #fill out rest of the matrix
    for l_pos from 1 <= l_pos < llen + 1:#longer sequence down
        for g_pos from 0 <= g_pos < glen + 1:#shorter sequence across
            if g_pos == 0:
                prevMax = npmax(S[l_pos-1],1)
#                prevMax = np.max(S[l_pos-1,1:])
                prevArgMax = 1 + npargmax(S[l_pos-1],1)
#                prevArgMax = 1+np.argmax(S[l_pos-1,1:])
                vals[0] = prevMax - <int> T
                vals[1] = S[l_pos-1,0]
                # Check to see if we should wrap back to a previous repeat
                if vals[0] > vals[1]:
                    S[l_pos,g_pos] = vals[0]
                    P[l_pos,g_pos] = prevArgMax
                else:
                    S[l_pos,g_pos] = vals[1]
                    P[l_pos,g_pos] = -1

            elif g_pos == 1:
                prevMax = npmax(S[l_pos-1],1)
#                prevMax = np.max(S[l_pos-1][1:])
                prevArgMax = 1 + npargmax(S[l_pos-1],1)
#                prevArgMax = 1+np.argmax(S[l_pos-1][1:])
                vals[0] = prevMax+dnamatrix[globalInd[g_pos-1],localInd[l_pos-1]]
                vals[1] = S[l_pos-1,g_pos]+indelP
                vals[2] = S[l_pos,g_pos-1]+indelP
                vals[3] = S[l_pos-1,g_pos-1]+dnamatrix[globalInd[g_pos-1],localInd[l_pos-1]]
                vals[4] = S[l_pos,0]

                S[l_pos,g_pos]= npmax(vals,0)
                am = 0
                for i from 0 <= i < 5:
                    val = vals[i]
                    if val == S[l_pos,g_pos]:
                        am = i

                if am == 0:
                    P[l_pos,g_pos] = prevArgMax
                else:
                    P[l_pos,g_pos] = -1*am

            else:

                vals[1] = S[l_pos-1,g_pos]+indelP
                vals[2] = S[l_pos,g_pos-1]+indelP
                vals[3] = S[l_pos-1,g_pos-1]+dnamatrix[globalInd[g_pos-1],localInd[l_pos-1]]
                vals[4] = S[l_pos,0]
                vals[0] = <int> minScorePossible
                S[l_pos,g_pos]= npmax(vals,0)
                P[l_pos,g_pos]= -1*(npargmax(vals,0))
    return S,P

#systematization of the norming step. Takes two vectors QT and TT. Tests whether 0 in TT. If yes, then norm using (1+P(QT))/(1+P(TT)) else uses simple P(QT)/P(TT) 
#This function (I think) is somewhat duck typed in the framework of pacmonstr. It could be used for all normalizations, even though the resulting matrices are
# handled differently for different event types.
def NormProj( np.ndarray[DTYPE_FLOAT_t] QT, np.ndarray[DTYPE_FLOAT_t] TT, int L):
    cdef unsigned int i = 0
    cdef unsigned int flag = 0
    cdef np.ndarray[DTYPE_FLOAT_t, ndim = 1] results = np.zeros(L,dtype="float")
    if 0 in TT:
        for i from 0 < i <= L:
            results[i] = ( ( 1 + QT[i] ) / ( 1 + TT[i] ) ) - 1
        return results
    else:
        for i from 0 < i <= L:
            results[i] = QT[i] / TT[i]
        return results

def compressMatrix (np.ndarray[dtype_tuint, ndim=2] DP, int scale, int s0, int s1):
    cdef np.ndarray[dtype_tuint, ndim=2] newDP = np.zeros((int(s0/int_max(1,scale)), int(s1/int_max(1,scale))),dtype='uint16')
    cdef int new_s0 = int(s0/int_max(1,scale))
    cdef int new_s1 = int(s1/int_max(1,scale))
    cdef unsigned int i = 0
    cdef unsigned int j = 0
    cdef unsigned int maxval = 0
    for i from 0 <= i < new_s0:
        for j from 0 <= j < new_s1:
            for iprime from i*int_max(1,scale) <= iprime < i*int_max(1,scale)+int_max(1,scale):
                for jprime from j*int_max(1,scale) <= jprime < j*int_max(1,scale) + int_max(1,scale):
                    if DP[iprime,jprime] > maxval:
                        maxval = DP[iprime,jprime]
            newDP[i, j] = maxval
            maxval = 0
            #newDP[i,j] = np.max(DP[i*scale:i*scale+scale,j*scale:j*scale+scale])
    return newDP

#This is actually implemented directly in numpy and is therefore not necessary
def C_MinMapThread(np.ndarray[DTYPE_t, ndim=2] matrixForward, np.ndarray[DTYPE_t, ndim=2] matrixReverse, int width, int height):
    cdef np.ndarray[DTYPE_t, ndim=2] matrixOut = np.zeros((width,height), dtype='int64')
    for i from 0 <= i < width:
        for j from 0 <= j < height:
            matrixOut[i,j] = int_min(matrixForward[i,j] , matrixReverse[i,j] )
    return matrixOut

cdef int InexactKmerMatchChar_C(char* kmerA, char* kmerB, int L, int posA, int posB, int toleratedMismatch):#tolerated mismatch is a count of mismatches
    cdef unsigned int position
    cdef int mismatch = 0
    #for position in range (L):
    for position from 0 <= position < L:
        if kmerA[position+posA] != kmerB[position+posB]:
            mismatch += 1
            if mismatch > toleratedMismatch:
                return 0
    return L - mismatch

# Aseq = y-axis sequence
# Bseq = x-axis sequence
def CompareSeqPairRef (char* Aseq, char* Bseq, int La, int Lb, int window, int TolMismatch, int SeedThresh, int prevWindow):
    cdef np.ndarray[dtype_tuint, ndim=2] DP = np.zeros((La, Lb), dtype='uint16')#the type should be small, but must be at least as large as the windowSize**2.
    cdef unsigned int w = 0
    cdef int mscore_i = 0
    cdef unsigned int apos = 0
    cdef unsigned int bpos = 0
    cdef int laiter = (La-window+1)
    cdef int lbiter = (Lb-window+1)
    cdef int prevSum = 0
    cdef unsigned int i = 0
    cdef unsigned int j = 0
    for apos from 0 <= apos < laiter:
        for bpos from 0 <= bpos < lbiter:
            mscore_i = InexactKmerMatchChar_C(Aseq, Bseq, window, apos, bpos, TolMismatch)
            if mscore_i == window:
                for w from 0 <= w < window:
                    DP[apos+w, bpos+w] += mscore_i
            elif mscore_i > 0:
                for i from 0 <= i < int_min(apos, prevWindow):
                    for j from 0 <= j < int_min(bpos, prevWindow):
                        prevSum += DP[apos - i,bpos - j]
                if prevSum > SeedThresh:
                    for w from 0 <= w < window:
                        DP[apos+w,bpos+w] += mscore_i
                prevSum = 0
    return DP
