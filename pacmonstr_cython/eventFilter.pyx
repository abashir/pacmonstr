# cython: profile=True
# cython: wraparound=False
# cython: boundscheck=False

import numpy as np
cimport numpy as np

ctypedef np.int_t dtype_ti

def eventFilter(char* qseq, char* refseq, np.ndarray[dtype_ti, ndim=1] qarray, np.ndarray[dtype_ti, ndim=1] rarray, int n ):
    cdef int currMismatches = -1
    cdef int maxMismatches = 0
    cdef int maxStartIndex = 0
    cdef int maxEndIndex = 0
    cdef int currStartIndex = 0
    cdef int qpos = -1
    cdef int rpos = -1
    cdef int match = 3
    cdef int i
    for i from 1 <= i < n:
        qpos = qarray[i]
        rpos = rarray[i]
        if rpos != -1 and qpos != -1:
            if refseq[rpos] == qseq[qpos]: #here there be indexing issuels
                match = -1
            else:
                match = 3
        else:
            match = 3
        if currMismatches > 0:
            currMismatches += match
        else:
            currMismatches = match
            currStartIndex = i
        if currMismatches > maxMismatches:
            maxMismatches = currMismatches
            maxStartIndex = currStartIndex
            maxEndIndex = i
        
    return maxEndIndex - maxStartIndex
