# PACmonSTR README

Of particular note to the user is the means by which the test.binned_anchors file is derived.
To generate this file, the user must first run <a href="https://tandem.bu.edu/trf/trf.html">Tandem Repeat Finder</a>,
generating the standard output file (an example of which is included in the toy data set).
Additionally, a blasr m5 file must be generated for the same reads. These two files together are combined
to generated the test.binned_anchors file. In the next push to this repository, I will include a script that
takes in tandem .m5 and .trf files and returns the appropriately formatted .binned_anchors file.