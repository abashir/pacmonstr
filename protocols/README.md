This folder will be used to keep a collection of protocol pipelines for various
applications of the pacmonstr novel structural variation finding tool.
The current protocols that have already been outlined and at this point
may or may not have been included in this folder yet are as follows:

1. Genome wide SV finding given corrected reads and a reference.

2. Identification of SNPs and SVs in PCR amplicons of a heterogeneous mixture, 
    followed by total correlation of all variants of any kind. 
    Several notes:
      i.  Because it is PCR based, reference projection vectors will be memoized in a python
          dictionary keyed on a (start,stop) tuple.
     ii.  Because of the fact that there is currently no way to represent larger type SVs
          in the PacBio correlatedVariants.py script, a terribly artificial work around is
          used, whereby Ns are added to mask barcodes on either end of the refSeq if req'd &  
          a random 40-mer sequence is generated and appended to all reads and the refSeq. A
          single indicator base is then added depending on whether a SV is present or absent
          (noting that this only works if the affine gap opening penalty is greater than the
          mismatch penalty). These single bases are then followed by the same random 40-mer,
          which will force either match or mismatch score to be used for this surrogate base
          pair. If there is an admixture of reads with a collection of 20 different types of 
          SVs (tandem duplications, for example) then this array will have 20 different such
          surrogate bases.
    iii.  
