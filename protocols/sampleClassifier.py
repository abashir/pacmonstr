#!/usr/bin/env python -o

##
# The aim of this script is to take in a read set and a single reference sequence. From this refSeq,
##the script will generate a set of modified reads with appended SV indicator arrays according to 
# their particular combination of SVs. This script employs memoization of reference projection vectors
##in order to speed up the process. Arguments are a refSeq fasta file, a fasta file filled with ccs reads and 
# a string describing the types of events to be detected. Currently, only Td are detected.
##
# 
##
# 
##
# 
##
# 
#####


import os
import sys
import argparse
from collections import Counter
from classes.ProjectEventGeneral import EventDetector as ed
import matplotlib.pyplot as plt
"""
try:
    from pbcore.io.FastaIO import SimpleFastaReader as sfr
except:
    from pbcore.io.FastaIO import FastaReader as sfr
"""

from classes.model.Sequence import revcomp as rc

parser = argparse.ArgumentParser(description = "A structural variant screener. m5 alignment file of reads to reference comes in, two fastas go out. One fasta contains all the SV+ reads, the other the SV- reads.")

parser.add_argument( "-m5", help="Specify an m5 file with all sequences mapped to a single reference sequence." )
parser.add_argument( "-PV", "--permittedVariants", default = 'Td',help="A string containing the types of permitted structural variants. The classifier will restrict its search to the the types of variants in this string. By default it is restricted to tandem duplications (Td). " )
args = parser.parse_args()

#a set of available event types to introduce into sequences
#Accepted input format is catted event types, as : TdSCtDITPtPtiPdPdiV
#def parseVariantString( stringIn):
#    types = set()
#    event = ''
#    for char in args.PV:
#        if char.upper() == char:
#            if event == '':
#                pass
#            else:
#                types.add(event)
#            event = char
#        else:
#            event += char
#    return list(types)

#####
# memoization dicts
# keyed to (start,stop) coords in ref. holds appropriate projection vectors
FTFTs = {}
RTFTs = {} # for inversion identification

eventSet = set()
eventCounter = Counter()
readEvents= {}
#####
# helper functions

# argument "offset" corrects for the fact that reads do not always start at precisely the same position in the reference.
def FindEventInHMM(HMMsignal, offset=0):
    events = []
    start = -1
    for pos in xrange(len(HMMsignal)):
        if HMMsignal[pos] == 1:
            if start == -1:
                start = pos
            else:
                pass
        else:
            if start == -1:
                pass
            else:
                events.append((start+offset,pos+offset))
                start = -1
    return events

for entry in open(args.m5):
    parsedEntry = dict(zip(['qname','qseqlength','qstart','qend','qstrand','tname','tseqlength','tstart','tend','tstrand','score','nMatch','nMismatch','nIns','nDel','MapQV','seq1','bars','seq2'], entry.split()))
    parsedEntry['seq1'] = parsedEntry['seq1'].replace('-','')
    parsedEntry['seq2'] = parsedEntry['seq2'].replace('-','')
    parsedEntry['qseqlength'] = int( parsedEntry['qseqlength'] )
    parsedEntry['qstart'] = int( parsedEntry['qstart'] )
    parsedEntry['qend'] = int( parsedEntry['qend'] )
    parsedEntry['tseqlength'] = int( parsedEntry['tseqlength'] )
    parsedEntry['tstart'] = int( parsedEntry['tstart'] )
    parsedEntry['tend'] = int( parsedEntry['tend'] )
    parsedEntry['score'] = int( parsedEntry['score'] )
    if parsedEntry['tstrand'] == '-':
        parsedEntry['seq1'] = rc(parsedEntry['seq1'] )
        parsedEntry['seq2'] = rc(parsedEntry['seq2'] )
    projection = ed(parsedEntry['seq1'] , parsedEntry['seq2'])
    #memoizeable?
    if (parsedEntry['tstart'],parsedEntry['tend']) in FTFTs:
        print 're-using previous FTFT'
        projection.FTFT = FTFTs[(parsedEntry['tstart'],parsedEntry['tend'])]
#A series of ifs here can be used to detect a variety of different kinds of events, noting that 
# a second memoization hash is instantiated above (RTFTs) for inversion detection.
    HMM = projection.DuplicationPredict()
    if (parsedEntry['tstart'],parsedEntry['tend']) not in FTFTs:
        FTFTs[(parsedEntry['tstart'],parsedEntry['tend'])] = projection.FTFT
    readEvents[parsedEntry['qname']] = set(FindEventInHMM(HMM,parsedEntry['tstart']))
    #unique events are currently strictly defined. At some later point, heirarchical clustering will have to be done on ITDs to 
    #compensate for read fidelity, silent mutations and other fuzziness.
    [eventSet.add(event) for event in readEvents[parsedEntry['qname']]] #store list of unique events in reference space
    for event in readEvents[parsedEntry['qname']]: #store list of unique events in reference space
         eventCounter[event] += 1 
    if 1 in HMM:
        projection.buildSeqPairProjection(projection.querySeq,projection.refSeq,imagename='ITD_%s' % parsedEntry['qname'].replace('/','-'))
        print len(FTFTs)
#        print eventSet
        print eventCounter
        print ''.join(map(str,HMM))
    else:
        

#Generate unique appendable strings for each sequence
"""
appendages = {}
for entry in sfr(orientedReadFN):
    entryUniques = eventSet.intersect(readEvents[entry.name])
"""

#Write modified reads out with appended strings.











#At the end, a 'sed' command can be automatically generated which can then be applied to the generated .gff. It will 
# correct all the surrogate variant 'SUB' positions so that their entries read as full 'DUP' entries in the correct positions.


    

        
    
                      



