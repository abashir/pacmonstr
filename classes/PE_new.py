#!/usr/bin/env python -o

#from pbcore.io.FastaIO import FastaReader as sfr
#import numpy as np
from sets import Set
from matplotlib import pyplot as plt
import random, time
from collections import Counter
#dok_matrix is treated like a normal matrix S[i,j] += something and can be queried in identical fashion to my old code


# List projector - a slightly different approach using sparse matrices and python hashes to represent the entire matrix, with m x n searching being done with a hamming distance trie instead of a set of full O( m n ) matrices.
# 0. parameters:
#    kmer length
#    tol mismatch
#    prevWindow
#
# 1. kmer list for reference.
# 2. kmer list for query.

# 4. Find in/exact matches that meet seed criteria. This is done using the trie, accepting only words below a hamming cut off. Because this search is O(k) after trie construction, this is used to find exact matches as well as inexact ones (hence the lack of step 3).

query = ''.join([random.choice('ACGT') for x in range(10000)])
target = query #''.join([random.choice('ACGT') for x in range(100)])
windowSize = 10
mismatch = 1
chainParameter = 4
chainSumThreshold = 100

t_kmersList = []
t_kmers = {}

for t_pos in range(len(target) - windowSize + 1):
    kmer = query[t_pos:t_pos + windowSize]
    t_kmersList.append(kmer)
    if kmer not in t_kmers:
        t_kmers[kmer] = [(t_pos,t_pos + windowSize)]
    else:
        t_kmers[kmer].append((t_pos,t_pos + windowSize))

q_kmers = [query[q_pos:q_pos + windowSize] for q_pos in range(len(query) - windowSize + 1)]

####THIS COULD USE A NICE CLEAN UP SO THAT THE SEARCH AND RECURSIVE SEARCH ARE PART OF THE CLASS AND SO THAT THE INSTANTIATION ISN'T HARD WIRED INTO THOSE FUNCTIONS
class TrieNode:
    def __init__(self):
        self.word = None
        self.children = {}
    def insert( self, word ):
        node = self
        for letter in word:
            if letter not in node.children: 
                node.children[letter] = TrieNode()
            node = node.children[letter]
        node.word = word

trie = TrieNode()
for kmer in Set(t_kmers.keys()):
	trie.insert(kmer)

# The search function returns a list of all words that are less than the given
# maximum distance from the target word
def search( word, maxCost ):
    # build first row
    currentRow = range( len(word) + 1 )
    results = []
    # recursively search each branch of the trie
    for letter in trie.children:
        searchRecursive( trie.children[letter], letter, word, currentRow, results, maxCost )
    return results

# This recursive helper is used by the search function above. It assumes that
# the previousRow has been filled in already.
def searchRecursive( node, letter, word, previousRow, results, maxCost ):
    columns = len( word ) + 1
    currentRow = [ previousRow[0] + 1 ]
    # Build one row for the letter, with a column for each letter in the target
    # word, plus one for the empty string at column 0
    for column in xrange( 1, columns ):
        if word[column - 1] != letter:
            replaceCost = previousRow[ column - 1 ] + 1
        else:                
            replaceCost = previousRow[ column - 1 ]
        currentRow.append( replaceCost )
    # if the last entry in the row indicates the optimal cost is less than the
    # maximum cost, and there is a word in this trie node, then add it.
    if currentRow[-1] <= maxCost and node.word != None:
        results.append( (node.word, currentRow[-1] ) )
    # if any entries in the row are less than the maximum cost, then 
    # recursively search each branch of the trie
    if min( currentRow ) <= maxCost:
        for letter in node.children:
            searchRecursive( node.children[letter], letter, word, currentRow, 
                results, maxCost )
######

# example: search( kmer,1) will yield all kmer matches within hamming distance of 1 in the trie.


#first find exact matches

#for posy, kmer in enumerate(q_kmers):
#    if kmer in t_kmers:
#        for entry in t_kmers[kmer]:
#            print entry[0],posy
#            for pos in range(windowSize):
#                print entry[0]+pos
#                sparseArray[(entry[0]+ pos, posy + pos)] += windowSize
#at this point, the exactMatch dotplot is completed

from scipy import *
from scipy.sparse import * # enables 'plt.spy(coo, cmap = cm.binary)'

#find in/exact match substrings. 
start = time.time()
#sparseArray = Counter()
S = dok_matrix((len(query),len(target)),float32)
for posy, kmer in enumerate(q_kmers):
    hits = search(kmer,mismatch)
    positions = [ (t_kmers[hit[0]],hit[1]) for hit in hits]
    for kmer_matches in positions:
        for match in kmer_matches[0]:
            #print match,kmer_matches[1]
            for pos in range(windowSize):
                if kmer_matches[1] <= 2:#exact match
                    #sparseArray[(match[0] + pos, posy+pos)] += (windowSize - kmer_matches[1])
                    S[match[0] + pos, posy + pos] += (windowSize - kmer_matches[1])
import matplotlib.cm as cm
print 'foo'
plt.spy(S,marker=',', cmap = cm.binary) #pixel plot of sparse array
plt.show()

print time.time() - start

                else:
                    pass
                    prefixSum = 0
                    #for pos in range(windowSize):#conditional inexact match
                        #find whether the sparse array contains any values in the prevwindow as in old algorithm
                        #sum them
                        #are they significant enough to permit an inexact match?
    





