#!/usr/bin/env python
""" Description of the functionality of this script """

import sys, os, time
import optparse, logging
import numpy as np
from scipy import *
from scipy.sparse import * # enables 'plt.spy(coo, cmap = cm.binary)'
from matplotlib import pyplot as plt
import matplotlib.cm as cm
from model.Sequence import revcomp

np.set_printoptions(threshold=np.nan)
class PE_trie:
    def __init__( self ):
        self.__parseArgs( )
        self.__initLog( )

    def __parseArgs( self ):
        """Handle command line argument parsing"""
        
        usage = "%prog [--help] [options] query target"
        parser = optparse.OptionParser( usage=usage, description=__doc__ )

        parser.add_option( "-l", "--logFile", help="Specify a file to log to. Defaults to stderr." )
        parser.add_option( "-d", "--debug", action="store_true", help="Increases verbosity of logging" )
        parser.add_option( "-i", "--info", action="store_true", help="Display informative log entries" )
        parser.add_option( "-p", "--profile", action="store_true", help="Profile this script, dumping to <scriptname>.profile" )
        parser.add_option( "-w", "--windowSize", help=" window size")
        parser.add_option( "-n", "--name", help="Name for this sequence (used for storing plot and projection images)")
        parser.add_option( "-m", "--mismatch", help=" allowed mismatches in window")
        parser.add_option( "-c", "--chainParameter", help=" chain Paramater")
        parser.add_option( "-t", "--chainSumThreshold", help=" chain  sum threshold")


        parser.set_defaults( logFile=None, debug=False, info=False, profile=False, windowSize = 10, mismatch=1, name = None,
                             chainParameter=4, chainSumThreshold=50)
        
        self.opts, args = parser.parse_args( )

        if len(args) != 2:
            parser.error( "Expected a 2 arguments." )

        self.query = args[0]
        self.query = self.query.upper()
        self.target = args[1]
        self.target = self.target.upper()
        self.name = self.opts.name
        if self.name:
            self.name = self.name.replace('/','_')
        self.windowSize = int(self.opts.windowSize)
        self.mismatch = int(self.opts.mismatch)
        self.chainParameter = int(self.opts.chainParameter)
        self.chainSumThreshold = int(self.opts.chainSumThreshold)


    def __initLog( self ):
        """Sets up logging based on command line arguments. Allows for three levels of logging:
        logging.error( ): always emitted
        logging.info( ): emitted with --info or --debug
        logging.debug( ): only with --debug"""

        logLevel = logging.DEBUG if self.opts.debug else logging.INFO if self.opts.info else logging.ERROR
        logFormat = "%(asctime)s [%(levelname)s] %(message)s"
        if self.opts.logFile != None:
            logging.basicConfig( filename=self.opts.logFile, level=logLevel, format=logFormat )
        else:
            logging.basicConfig( stream=sys.stderr, level=logLevel, format=logFormat )
                                                                 
    def run( self ):
        """Executes the body of the script."""
    
        logging.info("Log level set to INFO")
        logging.debug("Log Level set to DEBUG")
        
        # ref_v_ref pass
        #print 'ref-ref'
        Sref = self.performPass(self.target,self.target)

        # do forward pass
        #print 'query-ref'
        Sf = self.performPass (self.query, self.target)
        
        # do reverse pass (currently indexing off by k)
        #print 'rc(query)-ref'
        Src = self.performPass (revcomp(self.query), self.target, flip=True)
         
        # merge plots
        #self.plotSparseArray(Sf, Src,self.name)
        
        # generate projections
        SrefProjection = self.SparseProjection(Sref)
        SfProjection = self.SparseProjection(Sf)
        SrcProjection = self.SparseProjection(Src)

        return np.array( [SfProjection , SrefProjection])
        # merge projections
#        self.plotProjections(SfProjection, SrcProjection, self.name)
    
    def performPass ( self, query, target, flip=False):
        # generate kmer list for query
        q_list = self.generateSequenceKmerList (query)
        if flip:
            q_list = q_list[::-1]

        # build hash on target
        t_dict = self.generateSequenceDict (target)

        # build trie from target
        t_trie = self.buildTrieFromKmerSet (set(t_dict.keys()))
        
        # build the sparse array
        sparse_array = self.fillSparseArray (query, target, q_list, t_dict, t_trie)
        return sparse_array
        
    def plotSparseArray (self, Sf, Src, name):
        #cmap = mcolors.ListedColormap(['b','r'])
        plt.clf()
        plt.spy(Sf,marker=',', color='b') #pixel plot of sparse array
        plt.spy(Src,marker=',', color='r') #pixel plot of sparse array
#        plt.savefig(name+'_matrix.png')
#        plt.show()
    
    def SparseProjection(self, S):
        """ Uses the scipy dok sparse arrays as input, and returns projections 
        along the reference sequence for both the F_F pass (normal alignment detection) 
        and the RC_F pass (inversion detection).  """
        SProjection = np.ones(len(self.target),dtype='uint16')
        for key in S.keys():
            SProjection[key[1]] +=S[key]
        return SProjection
   

    def plotProjections (self, SfProjection, SrcProjection, name):
        plt.clf()
        plt.plot(SfProjection)
        plt.plot(SrcProjection)
#        plt.savefig(name+'_projections.png')

    def fillSparseArray (self, query, target, q_kmers, t_kmers, t_trie):
        #sparseArray = Counter()
        S = dok_matrix((len(query),len(target)),uint16)
        height,width = S.shape
        for posy, kmer in enumerate(q_kmers[:-10]):
            hits = t_trie.search(kmer,self.mismatch) #[('ACCACGT',0),('ACCAAGT',1)..]
            positions = [ (t_kmers[hit[0]],hit[1]) for hit in hits]# [([12,116],0) ,([44,158],1)...]  nb. these are now just lists of start positions
            for kmer_matches in positions:# a kmermatch looks like ((44,158),1)
                for match in kmer_matches[0]:
                    if kmer_matches[1] == 0:
                        for pos in range(self.windowSize):
                            #sparseArray[(posy+ pos, match[0]+pos)] += (windowSize - kmer_matches[1])
                            S[posy + pos, match + pos] += self.windowSize
                    elif self.testArrayArea(S, posy , match, self.chainParameter ,self.chainSumThreshold):#check imperfect match conditions
                        for pos in range(self.windowSize):
                            S[posy + pos, match + pos] += (self.windowSize - kmer_matches[1])
        return S

    def testArrayArea( self, S , row , column , indel , minThreshold):
        """ Takes in a position in a sparse array. Sums up values in that array defined
        by the square originating at (x - indel , y - indel). If sum < threshold then returns True
        which triggers inclusion of that kmer's scores to the sparse array"""
        score = 0
        x = range(-indel,0)
        keys = filter( lambda x: x[0] > 0 and x[1] > 0, [ ( i+row,j+column) for i in x for j in x])
        for key in keys:
            if key in S:
                score += S[key]
                if score >= minThreshold:
                    return True
        return False

    def buildTrieFromKmerSet (self, kmerSet):
        trie = TrieNode()
        for kmer in kmerSet:
            trie.insert(kmer)
        return trie

    def generateSequenceDict (self, seq):
        """ Generates a dictionary where keys are
        kemrs and values are a list of start positions"""
        t_kmers = {}
        for t_pos in range(len(seq) - self.windowSize + 1):
            kmer = seq[t_pos:t_pos + self.windowSize]
            t_kmers.setdefault(kmer, []).append(t_pos)
        return t_kmers

    def generateSequenceKmerList (self, seq):
        """ generates a list of kmers"""
        return [seq[q_pos:q_pos + self.windowSize] for q_pos in range(len(seq) - self.windowSize + 1)]

class TrieNode:
    def __init__(self):
        self.word = None
        self.children = {}

    def insert( self, word ):
        node = self
        for letter in word:
            if letter not in node.children: 
                node.children[letter] = TrieNode()
            node = node.children[letter]
        node.word = word

    # The search function returns a list of all words that are less than the given
    # maximum distance from the target word
    def search( self, word, maxCost ):
        # build first row
        currentRow = range( len(word) + 1 )
        results = []
        # recursively search each branch of the trie
        for letter in self.children:
            searchRecursive( self.children[letter], letter, word, currentRow, results, maxCost )
        return results

# This recursive helper is used by the search function above. It assumes that
# the previousRow has been filled in already.
def searchRecursive( node, letter, word, previousRow, results, maxCost ):
    columns = len( word ) + 1
    currentRow = [ previousRow[0] + 1 ]
    # Build one row for the letter, with a column for each letter in the target
    # word, plus one for the empty string at column 0
    for column in xrange( 1, columns ):
        if word[column - 1] != letter:
            replaceCost = previousRow[ column - 1 ] + 1
        else:                
            replaceCost = previousRow[ column - 1 ]
        currentRow.append( replaceCost )

    # if the last entry in the row indicates the optimal cost is less than the
    # maximum cost, and there is a word in this trie node, then add it.
    if currentRow[-1] <= maxCost and node.word != None:
        results.append( (node.word, currentRow[-1] ) )

    # if any entries in the row are less than the maximum cost, then 
    # recursively search each branch of the trie
    if min( currentRow ) <= maxCost:
        for letter in node.children:
            searchRecursive( node.children[letter], letter, word, currentRow, 
                             results, maxCost )
        
if __name__ == "__main__":
    app = PE_trie()
    if app.opts.profile:
        import cProfile
        cProfile.run( 'app.run()', '%s.profile' % sys.argv[0] )
    sys.exit( app.run() )
