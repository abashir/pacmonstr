#!/usr/bin/env python
""" Description of the functionality of this script """

import sys, os, re
import numpy as np
from classes.model.Sequence import revcomp as rc
#import matplotlib.pyplot as plt
from sklearn import hmm
eps =  np.finfo(np.float).eps

def blockFinderHelper(hmmIn, scale):
    start = -1
    end = 0
    position = 0
    events = []
    for x in hmmIn:
        if x == 1 and start == -1:
            start = position
        if x == 0 and start != -1:
            end = position
            events.append((int(start/scale),int(end/scale)))
            start = -1
        position += 1
    if start != -1 and len(events) > 0:
        events.append((int(start/scale),int(position/scale)))
    return events


class EventDetector:
    def __init__( self, querySeq, refSeq, window=10):
        self.window = int(window)

        self.windowsquared = float(self.window**2)
        self.kernel = np.array([self.window for x in range(self.window)]) #different kernel shapes might facilitate detection of different types of margins?

        self.querySeq = querySeq
        self.query_kmerList = np.chararray(len(self.querySeq) - self.window + 1,itemsize=self.window) 
        self.query_kmers = {}
        self.query_rc_kmerList = np.chararray(len(self.querySeq) - self.window + 1,itemsize=self.window) 

        self.refSeq = refSeq
        self.ref_kmerList= np.chararray(len(self.refSeq) - self.window + 1,itemsize=self.window) 
        self.ref_kmers = {}
        self.ref_rc_kmerList = np.chararray(len(self.refSeq) - self.window + 1,itemsize=self.window) 

#initialization of kmer lists and dictionaries
        #generate reference kmerList and kmer dict w/ values = positions where kmer is found in reference
        for pos in xrange(len(self.refSeq) - self.window + 1):
            kmer = self.refSeq[pos:pos + self.window]
            self.ref_kmerList[pos] = kmer
            self.ref_rc_kmerList[pos] = rc(kmer)
            self.ref_kmers.setdefault(kmer,[]).append(pos)
        #generate reference kmerList and kmer dict w/ values = positions where kmer is found in reference
        for pos in xrange(len(self.querySeq) - self.window + 1):
            kmer = self.querySeq[pos:pos + self.window]
            self.query_kmerList[pos] = kmer
            self.query_rc_kmerList[pos] = rc(kmer)
            self.query_kmers.setdefault(kmer,[]).append(pos)
            
#raw count vector inilialization
        
        self.Ref = np.zeros(len(self.refSeq)) #self-reference vector
        self.Refrc = eps+ np.ones(len(self.refSeq)) #inversion self-reference  vector
        self.Dup = np.zeros(len(self.refSeq)) #duplication vector
        self.Inv = eps+ np.zeros(len(self.refSeq)) #inversion vector

        self.Query = np.zeros(len(self.querySeq)) #FQFQ vector 
        self.Qrc = np.zeros(len(self.querySeq)) #RQFQ inversion vector in query space
        self.DupQ = np.zeros(len(self.querySeq)) #previously known as Ins
        self.InvQ = np.zeros(len(self.querySeq)) 
        #composite vectors

#        self.Del = eps+ np.zeros(len(self.refSeq)) #deletion vector
        self.Ins = np.zeros(len(self.querySeq)) #insertion vector

#iterate over reference sequence
        for position in xrange(len(self.ref_kmerList)):
            fwd_kmer = self.ref_kmerList[position]
            rev_kmer = rc(fwd_kmer)
            positions = self.ref_kmers[fwd_kmer]
            self.Ref[position:position+self.window] += len(positions) * self.kernel
            if rev_kmer in self.ref_kmers:
                positions = self.ref_kmers[rev_kmer]
                self.Refrc[position:position+self.window] += len(positions) * self.kernel
            if rev_kmer in self.query_kmers:
                positions = self.query_kmers[rev_kmer]
                self.Inv[position:position+self.window] += len(positions) * self.kernel
            if fwd_kmer in self.query_kmers:
                positions = self.query_kmers[fwd_kmer]
                self.Dup[position:position+self.window] += len(positions) * self.kernel

#iterate over query sequence
        for position in xrange(len(self.query_kmerList)):
            fwd_kmer = self.query_kmerList[position]
            positions = self.query_kmers[fwd_kmer]
            self.DupQ[position:position+self.window] += len(positions) * self.kernel
            if fwd_kmer in self.ref_kmers:
                positions = self.ref_kmers[fwd_kmer]
                self.Ins[position:position+self.window] += len(positions) * self.kernel
            rev_kmer = rc(fwd_kmer)
            if rev_kmer in self.ref_kmers:
                positions = self.ref_kmers[kmer]
                self.InvQ[position:position+self.window] += len(positions) * self.kernel

#normed count vectors
        self.Ref = self.Ref / self.windowsquared
        self.Refrc = self.Refrc / self.windowsquared
        self.Dup = self.Dup / self.windowsquared
        self.Inv = self.Inv / self.windowsquared

        self.DupQ = self.DupQ / self.windowsquared # QVQ
        self.Ins = self.Ins / self.windowsquared

        self.norm_InvQ = self.Query / self.windowsquared
        self.norm_DupQ = (( 1 + self.Ins) / (1 + self.DupQ)) -1
        
        self.norm_Dup = (( 1 + self.Dup) / (1 + self.Ref) ) - 1   #duplication vector
        self.norm_Inv = (( 1 + self.Inv) / (1 + self.Refrc) ) - 1  #inversion vector
        self.norm_Del = self.norm_Inv + self.norm_Dup #deletion vector 
        self.norm_Ins = (( 1 + self.Ins) / (1 + self.InvQ) ) - 1   #duplication vector

#event finding
        #Events in the reference domain
        self.Dup_hidden_states = np.array(self.hmmBlock(4 * self.norm_Dup,0,1),dtype=np.uint16)
        self.Del_hidden_states = np.array(self.hmmBlock(4 * self.norm_Del,0,-1),dtype=np.uint16)
        self.Inv_hidden_states = np.array(self.hmmBlock(4 * self.norm_Inv,0,1),dtype=np.uint16)

        hmmSIMPLEDEL = self.hmmBlock(4 * self.norm_Dup,0,-1)
        hmmDUPINVpre = hmmSIMPLEDEL < 1
        self.DupInv = np.bitwise_and(hmmDUPINVpre, self.Inv_hidden_states)
        
        #events in the query domain
        self.Ins_hidden_states = np.array(self.hmmBlock(4 * self.norm_Ins,0,-1),dtype=np.uint16)
        self.InvQ_hidden_states = np.array(self.hmmBlock(4 * self.norm_InvQ,0,1),dtype=np.uint16)
        self.DupQ_hidden_states = np.array(self.hmmBlock(4 * self.norm_DupQ,0,1),dtype=np.uint16)
        self.DelInv = np.bitwise_and(self.DupQ_hidden_states,self.InvQ_hidden_states)

#        self.DupInv_hidden_states = np.array(self.hmmBlock(4 * self.norm_Del,0,1),dtype=np.uint16) #Identifies location in reference where a sequence is duplicated and inverted IN THE REFERENCE. In order to find out where the sequence has then been inserted, a second step will have to be done involving projection on the query.

        self.norm_Ins = self.InvQ + self.DupQ #insertion vector    

        self.Ins_events = []
        for event in re.finditer(r'1+',''.join(map(str,self.Ins_hidden_states))):
            self.Ins_events.append([event.start(),event.end()])

    def hmmBlock ( self, X ,normState,eventState):
        from sklearn import hmm
        startprob = np.array([0.99, 0.01])
        transmat = np.array([[0.9995, 0.0005], [0.00005, .99995]])
        model = hmm.GaussianHMM (2, 'spherical', startprob, transmat)
#        m_means = np.array([[.5], [-.5]])
        m_means = np.array([[normState], [eventState]])
        m_covars = np.array([[1],[1]])
        #m_covars = np.tile(np.identity(1), (2, 1, 1))
        model.means_ = m_means
        model.covars_ = m_covars
        revX = np.array([X]).transpose()
#        print revX.shape
        hidden_states = model.predict(revX)
        
#        print " ".join(map(str, X))
#        print "".join(map(str, hidden_states))
#        return hidden_states
        return hidden_states

if __name__ == "__main__":
    app = InversionDetector(seq1.upper(), seq2.upper())
    #if app.opts.profile:
    #    import cProfile
    #    cProfile.run( 'app.run()', '%s.profile' % sys.argv[0] )
    app.Predict()   
    
 
