#!/usr/bin/env python
""" Description of the functionality of this script """

import sys, os, time
import optparse, logging
import numpy as np
from scipy import *
from scipy.sparse import * # enables 'plt.spy(coo, cmap = cm.binary)'
import matplotlib.pyplot as plt
from sklearn import hmm
from copy import deepcopy

class SVPairHMM:
    def __init__( self ):
        self.__parseArgs( )
        self.__initLog( )

    def __parseArgs( self ):
        """Handle command line argument parsing"""
        
        usage = "%prog [--help] [options] input_file"
        parser = optparse.OptionParser( usage=usage, description=__doc__ )

        parser.add_option( "-l", "--logFile", help="Specify a file to log to. Defaults to stderr." )
        parser.add_option( "-d", "--debug", action="store_true", help="Increases verbosity of logging" )
        parser.add_option( "-i", "--info", action="store_true", help="Display informative log entries" )
        parser.add_option( "-p", "--profile", action="store_true", help="Profile this script, dumping to <scriptname>.profile" )
        parser.add_option( "-w", "--windowSize", help=" window size")
        parser.add_option( "-n", "--name", help="Name for this sequence (used for storing plot and projection images)")
        parser.add_option( "-m", "--mismatch", help=" allowed mismatches in window")
        parser.add_option( "-c", "--chainParameter", help=" chain Paramater")
        parser.add_option( "-t", "--chainSumThreshold", help=" chain  sum threshold")
        parser.add_option( "--plot", action="store_true", help=" turn on plotting")
        parser.add_option( "--middle", action="store_true", help=" assume sv is in middle for simulation only")
        parser.add_option( "--sim", action="store_true", help="data is from simulation, assume a structure for file names to report back stats table")
        parser.add_option( "--sv_len_cut", help="sv length cutoff to report on")

        parser.set_defaults( logFile=None, debug=False, info=False, profile=False, windowSize = 5, mismatch=1, name = None,
                             chainParameter=4, chainSumThreshold=50, plot=False, middle=False, sv_len_cut = 0)
        
        self.opts, args = parser.parse_args( )

        if len(args) != 1:
            parser.error( "Expected a 1 argument." )

        self.inFN = args[0]
        self.name = self.opts.name
        if self.name:
            self.name = self.name.replace('/','_')
        self.windowSize = int(self.opts.windowSize)
        self.mismatch = int(self.opts.mismatch)
        self.chainParameter = int(self.opts.chainParameter)
        self.chainSumThreshold = int(self.opts.chainSumThreshold)
        self.plotFlag = self.opts.plot
        self.middleFlag = self.opts.middle
        self.sv_len_cut = int(self.opts.sv_len_cut)


    def __initLog( self ):
        """Sets up logging based on command line arguments. Allows for three levels of logging:
        logging.error( ): always emitted
        logging.info( ): emitted with --info or --debug
        logging.debug( ): only with --debug"""

        logLevel = logging.DEBUG if self.opts.debug else logging.INFO if self.opts.info else logging.ERROR
        logFormat = "%(asctime)s [%(levelname)s] %(message)s"
        if self.opts.logFile != None:
            logging.basicConfig( filename=self.opts.logFile, level=logLevel, format=logFormat )
        else:
            logging.basicConfig( stream=sys.stderr, level=logLevel, format=logFormat )

    def readTRLine (self, line):
        ll = line.strip().split()
        q, t, rep = ll[0:3]
        qreplen = int(ll[3])
        llr_diffs = map(float, ll[4:4+qreplen])
        llrs = map(float, ll[4+qreplen:])
        return t, llr_diffs, llrs

    def readTRLine2 (self, line):
        ll = line.strip().split()
        name, qreplen, freq = ll[0:3]
        qreplen = int(qreplen)
        llr_diffs = map(float, ll[3:3+qreplen])
        llrs = map(float, ll[3+qreplen:])
        return name, llr_diffs, llrs

    def run( self ):
        """Executes the body of the script."""
    
        logging.info("Log level set to INFO")
        logging.debug("Log Level set to DEBUG")

        counter = 0
        countDict = {}
        if self.opts.sim:
            if self.opts.middle:
                print "period period_str freq sv_size true_sv_position true_sv_position_end predict_sv_position predict_sv_position_end hmm_sv_predictions min_overlap"
            else:
                print "period period_str freq sv_size true_sv_position predict_sv_position hmm_sv_predictions min_overlap"
        else:
            print "read_id sv_count tr_interval_length sv_blocks"
        f = open (self.inFN)
        for line in f.xreadlines():
            counter += 1
            logging.info("line number: %i" %( counter))
            if self.opts.sim:
                t, llr_diffs, llrs = self.readTRLine2 (line)
            else:
                t, llr_diffs, llrs = self.readTRLine2 (line)

            if self.opts.sim:
                tlist = t.split("_")
                p, f, sv = map(int, [tlist[3], tlist[5], tlist[7]])
                ptrue = int(float(tlist[11]))

                pstr = "%i-%i" %(p-5,p-1)
                countDict.setdefault(p, {})
                countDict[p].setdefault(f, []) 
                if p*f > 0 and sv > 0:
                    rev_sv = sv
                    hidden_states = self.plotProjectionSimple(llr_diffs, llrs, rev_sv, t, plot=self.plotFlag)
                    true_sv = len(llrs)-sv
                    count, pos_tups = self.checkHiddenStatesSimple (hidden_states)
                
                    pos, pos_end = -1,-1
                    if len(pos_tups) > 0:
                        pos, pos_end = pos_tups[0]
                

                    true_reg_len = len(llrs)-sv
                    true_sv_insertion_point = true_reg_len/2                                    
                    true_sv_end_point = true_sv_insertion_point + sv
                                    
                    overlap_ratio = 0.0
                    if self.opts.middle:
                        if (pos <= true_sv_end_point and pos_end >= true_sv_insertion_point):
                            overlap_min = max(pos, true_sv_insertion_point)
                            overlap_max = min(pos_end, true_sv_end_point)
                            max_interval = max(pos_end-pos, true_sv_end_point-true_sv_insertion_point)
                            overlap_ratio = float(overlap_max-overlap_min)/max_interval
                        print ptrue, pstr, f, sv, true_sv_insertion_point, true_sv_end_point, pos, pos_end, min(count, 2), overlap_ratio
                    else:
                        if pos <= true_sv:
                            overlap_ratio = sv/float(len(llrs)-pos)
                        else:
                            overlap_ratio = float(len(llrs)-pos)/float(len(llrs)-true_sv) 
                        print ptrue, pstr, f, sv, true_sv, pos, min(count, 2), overlap_ratio
                    sys.stdout.flush()
            else:
                logging.info("line in file")
                hidden_states = self.plotProjectionSimple(llr_diffs, llrs)
                count, pos_tups = self.checkHiddenStatesSimple (hidden_states)
                if count > 0 and max(map(lambda x: x[1]-x[0], pos_tups)) > self.sv_len_cut:
                    print "%s %i %i %s" %(t, count, len(hidden_states), ",".join(map(lambda x: "%i-%i" %(x[0], x[1]), pos_tups)))
             

    def checkHiddenStatesSimple (self, hidden_states):
        count = 0
        oneFlag = False
        pos_tups = []
        pos, pos_end = -1, -1
        for i in range(len(hidden_states)):
            if hidden_states[i] == 1:
                if oneFlag:
                    pos_end = i
                    continue
                else:
                    oneFlag = True
                    pos, pos_end = i, i
                    count += 1
            else:
                if oneFlag:
                    pos_tups.append((pos, pos_end))
                oneFlag = False
        if oneFlag:
            pos_tups.append((pos, pos_end))
        return count, pos_tups


    def plotProjectionSimple (self, llr_diffs, llrs, sv=None, titlestr=None, plot=False):
        hidden_states = self.hmmBlockSimple (llr_diffs)
        if plot:
            svpos = len(llr_diffs)-sv
            plt.plot(llr_diffs, label="LLR diffs")
            plt.plot(5* hidden_states, label="predicted states")
            plt.axvline(sv)
            plt.axvline(svpos)
            plt.legend(loc=2)
            plt.title(titlestr)
            plt.show()
            plt.clf()
        return hidden_states
            

    def hmmBlockSimple ( self, X ):
        s = np.median(X)
        m = np.mean(X)
        
        startprob = np.array([0.99, 0.01])
        transmat = np.array([[0.99999999995, 0.00000000005], [0.00000000005, .99999999995]])
        model = hmm.GaussianHMM (2,'diag', startprob, transmat,n_iter=1000)

        m_means = np.array([[s], [-0.2]]) #[s], [-0.2] works good except in the case when SV~10base pairs
        m_covars = np.array([[2],[2]]) #param4: 2,2; prev 2,2

        model.means_ = m_means
        model.covars_ = m_covars
        revX = np.array([X]).transpose()
        hidden_states = model.predict(revX)        
        return hidden_states

        
if __name__ == "__main__":
    app = SVPairHMM()
    if app.opts.profile:
        import cProfile
        cProfile.run( 'app.run()', '%s.profile' % sys.argv[0] )
    sys.exit( app.run() )
