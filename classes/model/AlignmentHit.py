from model.Sequence import revcomp as rc

class AlignmentHit:
    def __len__( self ):
        return self.query_end - self.query_start
    
    def __init__( self ):
        self.query_length, self.query_id, self.query_start, \
            self.query_end, self.query_strand = 0, None, 0, 0, None
        self.target_length, self.target_id, self.target_start, \
            self.target_end,self.target_strand = 0, None, 0, 0, None
        self.score = 0
        self.aligned = ""
        self.alignedQuery = ""
        self.alignedTarget = ""
    
    def __str__( self ):
        return 'agar: %d %d %s %d %d %s %s %d %d %s %d' % \
            ( self.query_length, self.target_length, \
              self.query_id, self.query_start, self.query_end, \
              self.query_strand, self.target_id, self.target_start, \
              self.target_end, self.target_strand, self.score )

    def revcomp( self ):
        self.aligned = self.aligned[::-1]
        self.alignedQuery = rc(self.alignedQuery)
        self.alignedTarget = rc(self.alignedTarget)
        if self.target_strand == 1:
            self.target_strand = 0
        else:
            self.target_strand = 1
    
    def targetPosToIndex (self, pos):
        curr_pos = self.target_start
        target = self.alignedTarget
        if self.target_strand == 1:
            target = target[::-1]
        index = 0
        while curr_pos < pos:
            if target[index] != "-":
                curr_pos += 1
            index += 1
        return index


    def targetPosToIndices (self, spos, epos):
        curr_pos = self.target_start
        target = self.alignedTarget
        if self.target_strand == 1:
            target = target[::-1]
        index = 0
        sindex = 0
        eindex = 0
        while curr_pos < epos:
            if curr_pos == spos:
                sindex = index
            if target[index] != "-":
                curr_pos += 1
            index += 1
        return sindex, index


