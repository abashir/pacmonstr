#!/usr/bin/env python -o

#PEE_pipeline.y 
'''a wrapper for the ProjectEventExact.py script so that it can be
serially run on sequences. '''

import os , re , time
from classes.ProjectEventExact import EventDetector as ed
import sys,subprocess, ast, shlex, re , os
from model.Sequence import revcomp as rc
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.cm as cm

#import pacmonstr_cython.trFinderFuncs as trf

chromosome = str(sys.argv[1])
outFolder = sys.argv[2]
m5In = open(sys.argv[3])

refSizes = '''chr1 249250621
chr2 243199373
chr3 198022430
chr4 191154276
chr5 180915260
chr6 171115067
chr7 159138663
chr8 146364022
chr9 141213431
chr10 135534747
chr11 135006516
chr12 133851895
chr13 115169878
chr14 107349540
chr15 102531392
chr16 90354753
chr17 81195210
chr18 78077248
chr19 59128983
chr20 63025520
chr21 48129895
chr22 51304566
chrX 155270560
chrY 59373566
chrmtDNA 16569'''.split('\n')

for line in refSizes:
    lineSplit = line.split()
    if chromosome == lineSplit[0]:
        maxChrLen = int(lineSplit[1])

#maxChrLen = 255000000
results = [np.arange(maxChrLen,dtype=np.int),
           np.zeros(maxChrLen,dtype=np.uint16),
           np.zeros(maxChrLen,dtype=np.uint16),
           np.zeros(maxChrLen,dtype=np.uint16),
           np.zeros(maxChrLen,dtype=np.uint16),
           np.zeros(maxChrLen,dtype=np.uint16),
           np.zeros(maxChrLen,dtype=np.uint16)]
insertionList = []

m5Fields = ['qname','qseqlength','qstart','qend','qstrand','tname','tseqlength','tstart','tend','tstrand','score','nMatch','nMismatch','nIns','nDel','MapQV','seq1','bars','seq2']
timeRunningOut = 0
start = time.time()

"""
def compressMatrix (m, scale=10):
    s = m.shape
    newM = np.zeros((s[0]/scale, s[1]/scale))
    s2 = newM.shape
    for i in range (s2[0]):
        for j in range (s2[1]):
            newM[i][j] = np.mean(m[i*scale:i*scale+scale,j*scale:j*scale+scale])
    return newM
"""

for line in m5In:
    lineSplit = line.split()
    entry = dict(zip(m5Fields,lineSplit))
    if timeRunningOut == 1:
        print >> sys.stderr,entry['qname']
        continue
    if len(entry) == 19:# and not os.path.exists('%s%s' % (outFolder, sys.argv[3].split('/')[-1].replace('.m5','.dat'))): #don't reprocess sequences that have already been done in case of failure.
        entry['qseqlength'] = int(entry['qseqlength'])
        entry['qstart'] = int(entry['qstart'])
        entry['qend'] = int(entry['qend'])
        entry['tstart'] =int(entry['tstart'])
        entry['tend'] = int(entry['tend'])
        strippedQuery = entry['seq1'].replace('-','')
        strippedRef = entry['seq2'].replace('-','')
	if entry['tstrand'] == '-':
            strippedQuery = rc(strippedQuery)
            strippedRef = rc(strippedRef)
#        if len(strippedQuery) < 5000:
#            continue #read length cutoff
        a = ed(strippedQuery,strippedRef,window=10)
#        for_1_vs_for_2 = trf.CompareSeqPairRef(strippedQuery, strippedRef, len(strippedQuery), len(strippedRef),20,1,10,2)
#        rc_1_vs_rc_2 = trf.CompareSeqPairRef(rc(strippedQuery), rc(strippedRef), len(strippedQuery), len(strippedRef),20,1,10,2)[::-1,::-1]
#        matrix1 = (for_1_vs_for_2 + rc_1_vs_rc_2)/2.0
#        matrix1 = np.array(compressMatrix(matrix1)>0,dtype = int)
#        for_1_vs_for_2 = trf.CompareSeqPairRef(rc(strippedQuery), strippedRef, len(strippedQuery), len(strippedRef),20,1,10,2)
#        rc_1_vs_rc_2 = trf.CompareSeqPairRef(strippedQuery, rc(strippedRef), len(strippedQuery), len(strippedRef),20,1,10,2)[::-1,::-1]
#        matrix2 = (for_1_vs_for_2 + rc_1_vs_rc_2)/2.0
#        matrix2 = np.array(compressMatrix(matrix2)>0.0,dtype = int) * 2
#        matrix2 = matrix2[::-1]
#        plt.imshow(matrix1+matrix2, cmap = cm.binary, extent=[entry['tstart'],entry['tend'],entry['qend'],entry['qstart']] )
#        plt.show()
        DUP = a.Dup_hidden_states
        INV = a.Inv_hidden_states
        DEL = a.Del_hidden_states
        DUPINV = a.DupInv_hidden_states
        INS = a.Ins_hidden_states
#        plt.plot(a.norm_Dup,'r')
#        plt.plot(DUP,'b')
#        plt.show()
#        plt.plot(a.norm_Inv,'r')
#        plt.plot(INV,'b')
#        plt.show()
#        plt.plot(a.norm_Del,'r')
#        plt.plot(DEL,'b')
#        plt.show()
        rlen = len(strippedRef)
        results[1][entry['tstart']:entry['tstart']+rlen] += DUP
        results[3][entry['tstart']:entry['tstart']+rlen] += DEL
        results[2][entry['tstart']:entry['tstart']+rlen] += INV
        results[4][entry['tstart']:entry['tstart']+rlen] += DUPINV
        results[5][entry['tstart']:entry['tstart']+rlen] += INS
        results[6][entry['tstart']:entry['tstart']+rlen] += 1 
#        print len(strippedQuery) * len(strippedRef) , time.time() - start

outfn = '%s%s' % (outFolder, sys.argv[3].split('/')[-1].replace('.m5','.dat'))

print outfn
outFile = open(outfn,'w')
touchedList = np.nonzero(results[-1])[0]

for pos in touchedList:
    outseq = '%s %i %i %i %i %i %i\n' % (chromosome,pos,results[1][pos],results[2][pos],results[3][pos],results[4][pos],results[5][pos],results[6][pos])
    outFile.write(outseq)

outFile.close()

