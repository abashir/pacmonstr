#!/usr/bin/env python -o

#PEG wrapper QVR,RVR,COV version
'''a wrapper for the ProjectEventsGeneral.py script so that it can be
serially run on sequences. Because this script isn't compatible
with a module import, it has to be run instead using the
subprocess module, then the resulting stdout string must
be parsed into a list object.'''

import os , re 
import ProjectEventGeneral as PEG
import sys,subprocess, ast, shlex, re , os
from model import Sequence as seq

chromosome = str(sys.argv[1])
outFolder = sys.argv[2]
m5In = open(sys.argv[3])

#outFolder = '/scratch/pendlm02/temp/'
#outFolder = '/sc/orga/scratch/pendlm02/na12878/chr1/reads/'
bases = {}

spaces = re.compile(r'\s+')
m5Fields = ['qname','qseqlength','qstart','qend','qstrand','tname','tseqlength','tstart','tend','tstrand','score','nMatch','nMismatch','nIns','nDel','MapQV','seq1','bars','seq2']

for line in m5In:
    lineSplit = line.split()
    entry = dict(zip(m5Fields,lineSplit))
    if len(entry) == 19 and not os.path.exists(outFolder+entry['qname'].replace('/','_').split('.')[0] +'.dat'): #don't reprocess sequences that have already been done in case of failure.
        print entry['qname']
        entry['qseqlength'] = int(entry['qseqlength'])
        entry['qstart'] = int(entry['qstart'])
        entry['qend'] = int(entry['qend'])
        entry['tstart'] =int(entry['tstart'])
        entry['tend'] = int(entry['tend'])
        strippedQuery = entry['seq1'].replace('-','')
        strippedRef = entry['seq2'].replace('-','')
	if entry['tstrand'] == '-':
            strippedQuery = seq.revcomp(strippedQuery)
            strippedRef = seq.revcomp(strippedRef)
        PE = PEG.InversionDetector(strippedQuery,strippedRef, window=10 , mismatch = 0)
        results = PE.DeletionPredict()
        for pos, scores in enumerate(results[0]):        
            absPos = (chromosome, pos+entry['tstart'])
            if absPos not in bases:
                bases[absPos] = [0,0,0] # [QVR,RVR.COV]
            bases[absPos][0] += scores[0]
            bases[absPos][2] += 1

#print '%s%s' % (outFolder, sys.argv[3].split('/')[-1].replace('.m5','.dat'))

outFile = open('%s%s' % (outFolder, sys.argv[3].split('/')[-1].replace('.m5','.dat')),'w')
basesList = []
for key, value in bases.items():
	basesList.append([key[0],key[1],value[0],value[1],value[2]])

basesList.sort(key=lambda x: x[1])

for entry in basesList:
	outFile.write('%s:%i %i %i\n' % tuple(entry))

outFile.close()

