#implements the ITD finder vs. reference by refProjection.
# takes as attributes:
# 1. q: query sequence (i.e., sequencing data)
# 2. t: target sequence (i.e. a reference sequence to which the query will be aligned)
# 3. coords(optional) - alignment coordinates in q and t as ((qstart,qend),(tstart,tend)). If not included, alignment is assumed to be over the full length of both sequences.
# 4. refProj (optional) - refProj object, so that the class can be used to iterate over a whole set of sequences without having to regenerate the reference projection for each new sequence.
#
# Subclasses:
# 1. refProjector - u
# 
#
# Contains methods:
# 1. Project - generates a self-projection for reference if there isn't one. Then generates a dotplot matrix, t projection and normalized projection 
#              for the target/query pair.
#
# 2. Attributes:
# name (string) - name of query sequence.
# query (sequence) - sequencing data to be examined.
# target (sequence) - reference sequence used for comparison.
# window (int) - size of window considered in generating dot matrix.
# mismatch (int) - number of tolerated mismatches in a given window during matrix generation.
# refProj (numpy array) - this is a refProjector object that contains a reference projection that can be passed to Projector objects 
#                         in the typical case where there is one reference sequence and many queries.
# matrix (numpy 2D array) - this is the t/q matrix generated under above described conditions.
# projection (numpy array) - projection of self.matrix onto the reference sequence
# normProjection (numpy array) - considering the refProj object either generated or passed to the Projector object, this is the reference
#                                normalized projection of the query onto the reference.
# ITDwindows (list of tuples) - this is a list of tuples, each is a putative ITD based on a simple screen.
# [option argument]
# m4 file 

# qcoords (tuple) [optional] - If actual query is a substring of a larger query sequence, these are its coords. Used for genome scale inputs.
# tcoords (tuple) [optional] - If actual target is a substring of a larger target sequence, these are its coords. Used for genome scale inputs.
# returns:
# [queryID,((TRSTART1,TREND1),(TRSTART2,TREND2),...,(TRSTARTn,TRENDn))] as locations in the query sequences

__all__ = ["Projector",
           "refProjector"]

import numpy as np
import sys
import trFinderFuncs as trf
from model.Sequence import revcomp as rc
from matplotlib import pyplot as plt

epsilon = sys.float_info.epsilon

'''
    A class that takes in a .fasta file of sequences
    and iterates through sequencing data, returning a dictionary with:

        keys =  sequence names
        values = a list of tuple (IDTstart,ITDstop) of putative ITDs

    This class optionally takes in an .m4 blasr output file from alignment
    of fasta file to a reference and subsets both each query and the reference
    to generate dotplots that only consider relevant substrings in the reference.

    requires the PYTHONPATH to include smrtanalysis and the path to the custom_io package:

    export PYTHONPATH=/opt/pacbio/smrtanalysis-2.0.1//analysis/lib:/opt/pacbio/smrtanalysis-2.0.1//analysis/lib/python2.7:/home/mattp/git_repos/rrna/scripts/

startTime = time.time()
ITDReader('017500_1.fasta','/home/mattp/References/FLT-3_trunc.fasta','017500_1.m4')
print time.time() - startTime



    arguments:
    fasta
    Input data in fasta format

    refseq
    Reference sequence file, should currently only contain a single reference sequence.
    Will be extended to contain many sequences.

    m4
    m4 blasr alignment of fasta to refseq without header

EXAMPLE OF USAGE:

a = ITDReader('ccs.fasta','ref.fasta','blasr_results.m4')

for entry in a:
    

'''
"""
def MinMapThread(a , b):
	La , Wa = a.shape
	Lb , Wb = b.shape
	out = np.zeros((La,Wa))
	if La != Lb or Wa != Wb:
		print 'Inputs not the same shape.'
		exit()
	for i in range(La):
		for j in range(Wa):
			out[i,j] = min(a[i,j],b[i,j])
	return out
"""
def MinMapThread(a ,b):
	La , Wa = a.shape
	Lb , Wb = b.shape
	if La != Lb or Wa != Wb:
		print 'Inputs not the same shape.'
		exit()
	out = trf.C_MinMapThread(np.array(a,dtype='int'), np.array(b,dtype='int'),La,Wa)
	return out


class ITDReader():
    def __init__(self, fasta , refseq, m4 = None):
        from pbcore.io.FastaIO import FastaReader as sfr
        from custom_io.ReadMatcherIO import parseRm4 as Rm4
        from sets import Set
        import sys
        self.seqDict = {}
        self.refDict = {} #will write to accommodate multiple references
	self.refMaps = {} # for compounding reads onto a larger reference
        self.m4Dict = {} #dict of read hit objects indexed by name
        self.posOut = open( fasta.split('/')[-1].replace('.fasta','_pos.fasta'),'w')
        self.negOut = open( fasta.split('/')[-1].replace('.fasta','_neg.fasta'),'w')
        self._fastafilehandle = sfr(fasta)
        for entry in self._fastafilehandle:
            self.seqDict[entry.name] = entry.sequence
        self._reffilehandle = sfr(refseq)
        for entry in self._reffilehandle:
            self.refDict[entry.name] = entry.sequence
	    self.refMaps[entry.name] = np.zeros(len(entry.sequence),dtype="float64")
	    self.refMaps[entry.name].fill(epsilon)
        if m4 != None:
            self._m4file = Rm4(m4)
            for entry in self._m4file:
                self.m4Dict[entry.query_id] = entry
        self.seqSet = Set(self.seqDict.keys())
        self.m4Set = Set(self.m4Dict.keys())
        if not self.seqSet.issuperset(self.m4Set):#test whether all m4 entries have sequences in the fasta file. nb that they won't be equivalent if any seqs don't map to the reference sequence
            print 'This m4 does not correspond to this fasta file. Try adding -noSplitSubreads to blasr command before running ITDReader if you feel you have received this message in error.'
            sys.exit()
        global memoizeList
        memoizeList = {}
	counter = 0
        for key, values in self.m4Dict.items():
            self._curQuerySeq = self.seqDict[values.query_id] #pick read seq
            self._curTarSeq = self.refDict[values.target_id] #pick reference that it maps to
            self._q = self._curQuerySeq[values.query_start  : values.query_end]  #substring in query
            self._t = self._curTarSeq[values.target_start : values.target_end]  #substring in target (i.e. refSeq)
            #coordinates are in the plus strand for both seqs. if target is reported in minus, slice both then flip the query.
            #This makes it so that reference is always in plus and query is always plus cf. query.
            if values.target_strand != values.query_strand:
                self._q = rc(self._q)
            if abs(values.query_end - values.query_start) > 0.00 * len(self._curTarSeq):#if a seq isn't at least 00% as long as its ref, discard it...perhaps screen by blasr score?
                if (values.target_start, values.target_end) in memoizeList and memoizeList[(values.target_start, values.target_end)][0] > 5:
                    ITDproject = Projector(key , self._q , self._t, 10 , 2, refProj = memoizeList[(values.target_start, values.target_end)][1])
                else:
                    ITDproject = Projector(key , self._q , self._t, 10 , 2)
		    print self._q,self._t,(values.query_start, values.query_end) , (values.target_start, values.target_end)
		    if (values.target_start, values.target_end) not in memoizeList:
			    memoizeList[(values.target_start, values.target_end)] = [ 0 , 0 ]
                ITDproject.Project()
                memoizeList[(values.target_start, values.target_end)][0] += 1
		if memoizeList[(values.target_start, values.target_end)][0] > 5:
			memoizeList[(values.target_start, values.target_end)][1] = ITDproject.rProj
		for position in range(values.target_start, values.target_end):
			self.refMaps[values.target_id][position] = self.refMaps[values.target_id][position] + (float(ITDproject.projection[position - values.target_start])/(epsilon+float(ITDproject.rProj[position - values.target_start])))
		plt.clf()
		plt.plot(self.refMaps[values.target_id])
		plt.show()
#		plt.savefig('read_%i.png' % counter)
		counter += 1
			
'''
                if len( ITDproject.ITDwindows ) > 0:
                    self.posOut.write('>%s\n%s\n' % (key , self._q))
                else:
                    self.negOut.write('>%s\n%s\n' % (key , self._q))
'''

class Projector():
    import numpy as np
    match = 5
    mismatch = -15
    indelP = -15
    ITDintervalP = 7
    DNAMatrix=np.array([
            [match,mismatch,mismatch,mismatch,-4, 1, 1,-4,-4, 1,-4,-1,-1,-1,-2],
            [mismatch,match,mismatch,mismatch,-4, 1,-4, 1, 1,-4,-1,-4,-1,-1,-2],
            [mismatch,mismatch, match,mismatch, 1,-4, 1,-4, 1,-4,-1,-1,-4,-1,-2],
            [mismatch,mismatch,mismatch, match, 1,-4,-4, 1,-4, 1,-1,-1,-1,-4,-2],
            [-4,-4, 1, 1,-1,-4,-2,-2,-2,-2,-1,-1,-3,-3,-1],
            [1, 1,-4,-4,-4,-1,-2,-2,-2,-2,-3,-3,-1,-1,-1],
            [1,-4, 1,-4,-2,-2,-1,-4,-2,-2,-3,-1,-3,-1,-1],
            [-4, 1,-4, 1,-2,-2,-4,-1,-2,-2,-1,-3,-1,-3,-1],
            [-4, 1, 1,-4,-2,-2,-2,-2,-1,-4,-1,-3,-3,-1,-1],
            [1,-4,-4, 1,-2,-2,-2,-2,-4,-1,-3,-1,-1,-3,-1],
            [-4,-1,-1,-1,-1,-3,-3,-1,-1,-3,-1,-2,-2,-2,-1],
            [-1,-4,-1,-1,-1,-3,-1,-3,-3,-1,-2,-1,-2,-2,-1],
            [-1,-1,-4,-1,-3,-1,-3,-1,-3,-1,-2,-2,-1,-2,-1],
            [-1,-1,-1,-4,-3,-1,-1,-3,-1,-3,-2,-2,-2,-1,-1],
            [-2,-2,-2,-2,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]],dtype='i8')
    def __init__(self, queryID , query , target , window, mismatch, refProj = None, qcoords = None, tcoords = None):
        self.name = queryID
        self.window = window
        self.mismatch = mismatch
        if qcoords == None:
            self.query = query
        else:
            self.query = query[qcoords[0]:qcoords[1]]
        if tcoords == None:
            self.target = target
            self.tcoords = (0 , len(target) )
        else:
            self.target = target[tcoords[0]:tcoords[1]]
            self.tcoords = tcoords
        self.tlength = len(self.target)
        self.qlength = len(self.query)
        if refProj == None:
            print 'generating a reference projection.'
            refobj = refProjector(self.target,self.window,self.mismatch)
            self.rProj = refobj.projection
        elif len(refProj) != len(self.target): #if query matched only part of the target sequence. Gen proj, but w/o remaking ref matrix
            print 'Reusing previous reference projection, sliced'
            self.rProj = refProj[tcoords[0]:tcoords[1]]
        else:
            print 'Reusing previous projection, unsliced'
            self.rProj = np.array( map(lambda x: max ( x , epsilon) , refProj))
#        self.rProj = map( lambda x: max( x , epsilon), self.rProj)
#        print self.rProj
    def Project(self):
        self.matrix1 = trf.CompareSeqPairRef(self.query,self.target,self.qlength,self.tlength,self.window,self.mismatch,2,2)
        self.matrix2 = trf.CompareSeqPairRef(rc(self.query),rc(self.target),self.qlength,self.tlength,self.window,self.mismatch,2,2)[::-1,::-1]
        self.matrix = MinMapThread(self.matrix1,self.matrix2)
        self.projection = np.array (map ( lambda x: max( x, epsilon) , self.matrix.sum(axis=0))) # because w/ window =10 , the first  values are all zero
        self.normProjection = self.projection / self.rProj #at this point, a normalized projection has been generated
        self.ITDwindows = []
        Lp = len(self.normProjection)
        i = 0
        while i < Lp:
            if self.normProjection[i] > 1.0:
                windowStart = i
                while i < Lp and  self.normProjection[i] > 1.0:
                    i += 1
                if max(self.normProjection[windowStart:i])>1.6:#a window must max above 1.6 to be included
                    self.ITDwindows.append((windowStart,i))
            i += 1
'''            
        for ITDstart , ITDstop in self.ITDwindows:
            self.repeat = self.target[ITDstart:ITDstop] #pull out reference sequence for a putative repeat
            print self.repeat
            self._S , self._P = trf.generateScoringTableTR( self.query , self.repeat , len(self.query), len(self.repeat) , self.indelP ,self.DNAMatrix )
            self._seqA,self._seqB,self._matchString,self._rtrace,self._scores = traceBackWrapTR(self._S,self._P , self.query, self.repeat)
            print self._scores

    def traceBackWrapTR(S,P, gseq, lseq): #implementation of Durbin repeat recurrence
        q, r = S.shape # q = rowMax | r = colMax
        row = q-1
        col = 0
        seqA = []
        seqB = []
        match = []
        scores = []
        rtrace = []
        ITD = 0
        currScore = False
        endRow, endCol = -1, -1
        while row != 0:
            rtrace.append(ITD)
            print row, col, P[row], seqA, seqB, match
            if P[row][col] >= 0:#wrap around 
                #print "in first if"
                if col == 0: # >>> initialize new score for a new TR
                    seqA.append(lseq[row-1])
                    seqB.append("*")
                    match.append(' ')
                else: # >>> end score for current TR
                    seqA.append(lseq[row-1])
                # need to look at current base as match before modifying col
                    seqB.append(gseq[col-1])
                    match.append('|')
                    scores.append((currScore-S[row][col], row, col, endRow, endCol))
                    ITD += 1
                col = P[row][col]
                row += -1
                currScore = S[row][col]
                endRow = row
                endCol = col
            else:
                if P[row][col] == -1:#deletion in repeat
                    row += -1
                    seqA.append(lseq[row])
                    if col == 0:
                        seqB.append("*")
                    else:
                        seqB.append("-")
                    match.append(' ')
                elif P[row][col] == -2:#insertion in repeat
                    col  += -1
                    seqB.append(gseq[col])
                    seqA.append("-")
                    match.append(' ')
                elif P[row][col] == -3:#match/mismatch
                    row += -1
                    col += -1
                    seqA.append(lseq[row])
                    seqB.append(gseq[col])
                    match.append('|')
                    if col == 0:
                        scores.append((currScore-S[row][col], row, col, endRow, endCol))
                        ITD += 1
                elif P[row][col] == -4:#match/mismatch
                    col = 0
                    row += -1
                    seqA.append(lseq[row])
                    seqB.append(".")
                    match.append(' ')
                    if col == 0:
                        scores.append((currScore-S[row+1][col+1], row+1, col+1, endRow, endCol))
                        ITD += 1
        seqA, seqB, match = seqA[::-1], seqB[::-1], match[::-1]
        scores.sort(lambda x,y: cmp(y[0],x[0]))
        return "".join(seqA), "".join(seqB), "".join(match),  "".join(rtrace), scores

    def _glocalTraceBack (S, P, localSeq, globalSeq):#once an ITD has been found, it should possibly be glocally aligned back to the reference 
            col = len(globalSeq) 
            row = np.argmax(S[:,col])
            col = col
            localSeqOut = []
            globalSeqOut = []
            matchOut = []
            while col > 0:
                    if P[row,col] == 0:
                            localSeqOut.append('-')
                            globalSeqOut.append(globalSeq[col-1])
                            matchOut.append(' ')
                            col += -1
                    if P[row,col] == 1:
                            localSeqOut.append(localSeq[row-1])
                            globalSeqOut.append('-')
                            matchOut.append(' ')
                            row += -1
                    if P[row,col] == 2:
                            localSeqOut.append(localSeq[row-1])
                            globalSeqOut.append(globalSeq[col-1])
                            if localSeq[row-1] == globalSeq[col-1]:
                                    matchOut.append('|')
                            else:
                                    matchOut.append(' ')
                            row, col = row-1, col-1
            return ''.join(localSeqOut[::-1]), ''.join(matchOut[::-1]),''.join(globalSeqOut[::-1])
'''
#TODO - re-align reference window against query, returning acc. Ajay's input format:
#         #QueryId target_St target_End tR_St tR_End tR_Period tR_CopyNum tR_Seq PrefixSeq SuffixSeq

class refProjector( Projector ):
    def __init__(self , target, window, mismatch):
        self.target = target
        self.query = target
        self.tlength = len(self.target)
        self.qlength = len(self.target)
        self.window = window
        self.mismatch = mismatch
        # last two arguments here are preseed threshold and size. We keep them small presently.
        self.matrix1 = trf.CompareSeqPairRef(self.target,self.target,self.tlength,self.tlength,self.window,self.mismatch,2,2)
	self.rc_target = rc(self.target)
        self.matrix2 = trf.CompareSeqPairRef(self.rc_target,self.rc_target,self.tlength,self.tlength,self.window,self.mismatch,2,2)[::-1,::-1]
	self.matrix = MinMapThread(self.matrix1,self.matrix2)
        self.projection = self.matrix.sum(axis=0)
                    
#    [ ] TODO: THERE IS A MORE ELEGANT SOLUTION TO THE BOUNDARIES THAN USING EPSILON.
#    MUCH CLEANER NOW WITH ITD MIN(FORWARD, REVERSE)
