#!/usr/bin/env python -o

#PEE_pipeline.y 
'''a wrapper for the ProjectEventExact.py script so that it can be
serially run on sequences. '''

import os , re , time, sys
sys.path.append("..")
from ProjectEventExact import EventDetector as ed
import numpy as np
from multiprocessing import Pool
import pysam
from pbcore.io.FastaIO import FastaReader
taskcmd = "taskset -p -c 0-63 %d" % os.getpid()
print >>sys.stderr, taskcmd
os.system(taskcmd)
import bisect

#import pacmonstr_cython.trFinderFuncs as trf

#chromosomes = sys.argv[1].split(",")
stepsize = int(sys.argv[1])
chromlist = sys.argv[2].split(",")
fafn = sys.argv[3]
threads = int(sys.argv[4])
intervalsToScreen = sys.argv[5]
bamIn = sys.argv[6]

positiveSelectIntervals = False
if len(sys.argv) > 7:
    if sys.argv[7] != "0":
        positiveSelectIntervals = True


def readIntervalsToDict (fn):
    intervalDict = {}
    with open (fn) as f:
        for l in f:
            ll = l.strip().split()
            intervalDict.setdefault(ll[0], []).append((int(ll[1]), int(ll[2])))
    for k, v in intervalDict.items():
        v.sort(lambda x,y: cmp(x[0],y[0]))
        print >>sys.stderr, k, "added %i intervals" %(len(v))
    return intervalDict

intervalDict = readIntervalsToDict(intervalsToScreen)

samfile = pysam.Samfile( bamIn, "rb" )
fastaDict = {}
for entry in FastaReader(fafn):
    ename = entry.name.split()[0]
    if not ename in chromlist and chromlist[0] != "all":
        continue
    print >>sys.stderr, entry.name
    #refseq = entry.sequence
    fastaDict[ename] = entry.sequence.upper()
sys.stderr.flush()


def ror_helper (ar, qseq, refseq, tstart):
    aligned_pairs = ar.aligned_pairs
            # look at first aligned base
    ap = aligned_pairs[0]
    qpos, rpos = ap
            # check to see it is a match/mismatch
    if rpos != None and qpos != None:
        if refseq[rpos] == qseq[qpos]:
            match = -1
        else:
            match = 3
    else:
        match = 3
            # set up DP initialization
    currMismatches = match
    maxMismatches = 0
    maxStartIndex = 0
    maxEndIndex   = 0
    currStartIndex = 0
    # iterate through alignment
    # calculate maximal mismatch interval
    # note, use prior on accuracy
    # - mismatches = 4, matches = -1
    # i.e. if a region has 25% error it will have 
    # positive score
    for i in range(1,len(aligned_pairs)):
        ap = aligned_pairs[i]
        qpos, rpos = ap
        if rpos != None and qpos != None:
            if refseq[rpos] == qseq[qpos]:
                match = -1
            else:
                match = 3
        else:
            match = 3
        if currMismatches > 0:
            currMismatches += match
        else:
            currMismatches = match
            currStartIndex = i
        if currMismatches > maxMismatches:
            maxMismatches = currMismatches
            maxStartIndex = currStartIndex
            maxEndIndex = i

    print >>sys.stderr, maxEndIndex-maxStartIndex, len(aligned_pairs)
    return maxEndIndex - maxStartIndex
            # return largest block
            #print qseq
            #print rseq
  
def ror_helper2 (ar, start, results, refseq):
    qstart, qend = ar.qstart, ar.qend
    tstart, tend = ar.aend-ar.alen, ar.aend
    results[5][tstart-start:tend-start] += 1
    #for tpos in range(tstart, tend):
    #    results[5][tpos-start] += 1
            #fullqseq = ar.seq
            ### MP - these are the substrings for event detection
    qseq = ar.seq[qstart:qend+1]
    rseq = refseq[tstart:tend+1]
    return qstart, qend, tstart, tend, qseq, rseq

def runOnRegionOld(region_str):
    #global samfile
    #global fastaDict
    #results = [np.arange(size,dtype=np.int),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16)]
    #insertionList = []
    #print >>sys.stderr, "working on %s" %(region_str)
    #chrom, start, end = region_str.split("_")
    #refseq = fastaDict[chrom]
    #start, end = int(start), int(end)
    #subsize = end-start + 50000
    #results = [np.arange(subsize,dtype=np.int),np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16)]
    #try:
    #bamIter = samfile.fetch(chrom, start, end)
    #except:
    #    print >>sys.stderr, "%s does not seem to exit in bam" %(region_str)
    #    return
    #readcounter = 0
    #seenFlag = False
    #arlist = []
    #for ar in bamIter: 
    #    if not ar.is_secondary:
    #arlist.append(ar)
    #        range(1,100000)
    for i in range (1000000000):
        a = i+1
    #return arlist

def runOnRegion(region_str):
    global samfile
    global fastaDict
    global intervalDict
    global positiveSelectIntervals
    #results = [np.arange(size,dtype=np.int),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16)]
    insertionList = []
    print >>sys.stderr, "working on %s" %(region_str)
    chrom, start, end = region_str.split("_")
    refseq = fastaDict[chrom]
    start, end = int(start), int(end)
    subsize = end-start + 50000
    results = [np.arange(subsize,dtype=np.int),np.zeros(subsize,dtype=np.uint16), \
               np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16), \
               np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16), \
               np.zeros(subsize,dtype=np.uint16)]
    try:
        bamIter = samfile.fetch(chrom, start, end)
    except:
        print >>sys.stderr, "%s does not seem to exist in bam" %(region_str)
        return
    readcounter = 0
    seenFlag = False
    if not (positiveSelectIntervals):
        intervals = intervalDict[chrom]
    else:
        intervals = []
    for ar in bamIter: 
        if not ar.is_secondary:
            # account for overlapping intervals - makes sure you don't double read counts
            if not(positiveSelectIntervals) and ar.aend-ar.alen < start:
                continue
            print ar.qname
            qstart, qend = ar.qstart, ar.qend
            tstart, tend = ar.aend-ar.alen, ar.aend

            ttup = (tstart, tend)
            bisect_index = bisect.bisect_left(intervals, ttup)
            bisect_flag = False
            while bisect_index < len(intervals) and intervals[bisect_index][0] < tend:
                istart, iend = intervals[bisect_index]
                if istart < tend and iend > tstart:
                    bisect_flag = True
                    break
            if bisect_flag:
                print >>sys.stderr, "overlapped interval %s: %s %i %i" %(ar.qname, chrom, tstart, tend)
                continue
            readcounter += 1

            results[5][tstart-start:tend-start] += 1
            #for tpos in range(tstart, tend):
            #    results[5][tpos-start] += 1
                    #fullqseq = ar.seq
                    ### MP - these are the substrings for event detection
            qseq = ar.seq[qstart:qend+1]
            rseq = refseq[tstart:tend+1]

            # get list of aligned postions
            # MATT CYTHON THIS STUFF!
            #qstart, qend, tstart, tend, qseq, rseq = ror_helper2 (ar, start, results, refseq)
            #mismatchwindow = ror_helper(ar, qseq, refseq, tstart)
            aligned_pairs = ar.aligned_pairs
            # look at first aligned base
            ap = aligned_pairs[0]
            qpos, rpos = ap
                    # check to see it is a match/mismatch
            if rpos != None and qpos != None:
                if refseq[rpos] == qseq[qpos]:
                    match = -1
                else:
                    match = 3
            else:
                match = 3
                    # set up DP initialization
            currMismatches = match
            maxMismatches = 0
            maxStartIndex = 0
            maxEndIndex   = 0
            currStartIndex = 0
            # iterate through alignment
            # calculate maximal mismatch interval
            # note, use prior on accuracy
            # - mismatches = 4, matches = -1
            # i.e. if a region has 25% error it will have 
            # positive score
            for i in range(1,len(aligned_pairs)):
                ap = aligned_pairs[i]
                qpos, rpos = ap
                if rpos != None and qpos != None:
                    if refseq[rpos] == qseq[qpos]:
                        match = -1
                    else:
                        match = 3
                else:
                    match = 3
                if currMismatches > 0:
                    currMismatches += match
                else:
                    currMismatches = match
                    currStartIndex = i
                if currMismatches > maxMismatches:
                    maxMismatches = currMismatches
                    maxStartIndex = currStartIndex
                    maxEndIndex = i
        
            #print >>sys.stderr, maxEndIndex-maxStartIndex, len(aligned_pairs)
            mismatchwindow = maxEndIndex - maxStartIndex

            if mismatchwindow < 100 :
                continue
            
            print >>sys.stderr, region_str, ar.qname, readcounter

            a = ed(qseq,rseq,window=8)

            DUP = a.Dup_hidden_states
            INV = a.Inv_hidden_states
            DEL = a.Del_hidden_states
            DUPINV = a.DupInv_hidden_states
            qPosDict = {}
            if len(a.Ins_events) > 0:
                prev_rpos = -1
                for ap in aligned_pairs:                        
                    #print >>sys.stderr, "ap: %s %s" %(ar.qname, str(ap))
                    qpos, rpos = ap
                    if rpos == None:
                        rpos = prev_rpos
                    if qpos != None: 
                        qPosDict[qpos] = rpos
                    prev_rpos = rpos
                minqpos = min(qPosDict.keys())
                maxqpos = max(qPosDict.keys())
                for event in a.Ins_events:
                    event_qstart = max(minqpos, event[0]-50)
                    event_qend = min(maxqpos, event[1]+50)
                    event_tstart = qPosDict[event_qstart]
                    event_tend = qPosDict[event_qend]
                    insertionList.append((ar.qname,event[0],event[1], chrom, event_tstart, event_tend))
                    results[5][event_tstart:event_tend] += 1
                    seenFlag = True
            rlen = len(rseq)
            results[1][tstart-start:tstart+rlen-start] += DUP
            results[2][tstart-start:tstart+rlen-start] += INV
            results[3][tstart-start:tstart+rlen-start] += DEL
            results[4][tstart-start:tstart+rlen-start] += DUPINV
            seenFlag = True
    if not seenFlag:
        return
    print >>sys.stderr, "writing results list for %s" %(region_str)
    sys.stderr.flush()
    outFile = open("%s_results.txt" %(region_str), 'w')    
    for pos in range (subsize):
        dup, dupinv, inv, delete, cov = 0, 0, 0, 0, 0
        if pos % 1000000 == 0:
            print >>sys.stderr, pos
            sys.stderr.flush()
        dup     = results[1][[pos]]
        inv     = results[2][[pos]]
        delete  = results[3][[pos]]
        dupinv  = results[4][[pos]]
        ins     = results[5][[pos]]
        cov     = results[6][[pos]]
        if sum([dup, inv, delete, dupinv]) > 0:
            #print >>sys.stderr, "pos %i has hit" %(pos)
            outseq = '%s %i %i %i %i %i %i %i\n' % (chrom,pos+start,dup, inv, delete, dupinv, ins, cov)
            outFile.write(outseq)
            outFile.flush()
    
    outFile.close()    
    insertions_outfile = open('%s_insertions.txt' % (region_str), 'w')
    for event in insertionList:
        insertions_outfile.write('%s %i %i %s %i %i\n' % tuple( event))
    insertions_outfile.close()
    return 
    #return results

def buildRegionStrs (fastaDict, threads, step):
    region_strs = []
    for fname, fseq in fastaDict.items():
        if "_" in fname:
            continue
        fsize = len(fseq)
        #step = fsize / max(1, (threads - 1))        
        steps = fsize/step + 1
        for i in range(steps):
            region_strs.append("%s_%i_%i" %(fname, i*step, (i+1)*step-1))
    print >>sys.stderr, region_strs
    return region_strs

def printResultsList (regionstrs):
    chrs = set()
    for region_str in regionstrs:
        chrs.add(region_str.split("_")[0])
    for chrom in chrs:
        os.system("cat %s_*results.txt | sort -nk2 > %s_mergedresults.sorted.txt" %(chrom, chrom))
        os.system("cat %s*insertions.txt | sort -k1 > %s_mergedinsertions.sorted.txt" %(chrom, chrom))        
        os.system("rm %s*results.txt %s*insertions.txt" %(chrom, chrom))

if positiveSelectIntervals:
    region_strs = []
    for chrom in intervalDict:
        for interval in intervalDict[chrom]:
            region_strs.append("%s_%i_%i" %(chrom, interval[0], interval[1]))
else:
    region_strs = buildRegionStrs(fastaDict, threads, stepsize)
myPool = Pool(threads)
resultsList = myPool.map(runOnRegion, region_strs)

#for region_str in region_strs:
#    runOnRegion(region_str)
printResultsList (region_strs)

