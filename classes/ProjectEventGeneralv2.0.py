#!/usr/bin/env python
""" Description of the functionality of this script """

import sys, os
import numpy as np
import pacmonstr_cython.trfinderFuncs as trf
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from model.Sequence import revcomp as rc
import matplotlib.pyplot as plt
from sklearn import hmm

epsilon = sys.float_info.epsilon

#scale value is only for use when compressing giant images and keeping coordinate systems consistent between actual dotplot and compressed dotplot
# for use yere, scale should always be 1, since the goal ultimately is to generate coordinates in the reference, not in an image.
def blockFinderHelper(hmmIn, scale = 1):
    start = -1
    end = 0
    position = 0
    events = []
    for x in hmmIn:
        if x == 1 and start == -1:
            start = position
        if x == 0 and start != -1:
            end = position
            events.append((int(start/scale),int(end/scale)))
            start = -1
        position += 1
    if start != -1 and len(events) > 0:
        events.append((int(start/scale),int(position/scale)))
    return events

class EventDetector:
    def __init__( self, querySeq, refSeq, alignData='', window=10, mismatch=4, indelBand=10, bandThreshold=40):#a sensible bandThreshold depends on window size
        self.alignData = -1 # figure out the best way to capture this
        self.querySeq = querySeq    
        self.refSeq = refSeq
        self.window, self.mismatch = window, mismatch
        self.windowSquared = self.window * self.window
        # indelband is the band around position in matrix 
        # (i.e. upstream window +/- 2 along the upstream diagonal)
        # bandThreshold = the sum of values to permit an inexact match
        self.indelBand, self.bandThreshold = indelBand, bandThreshold

        #flattened matrices on axis=0, so that they don't have to be recalculated if multiple events are being sought per read
        self.RTFT = None 
        self.FTFT = None 
        self.FQFT = None 
        self.RQFT = None 
        self.FQFQ = None
        self.RQFQ = None

        #normalized outputs, pre-hmm
        self.normDel = None
        self.normDup = None
        self.normInv = None
        self.normDupInv = None
        self.normIns = None
        
    def hmmBlock ( self, X ,normRatio,eventRatio):
# This block may need to be incorporated at some point
#   in the future if hyper1 operations are used for normalization, otherwise
#   ignore:
#        minRatio = min(normRatio, eventRatio)
#        maxRatio = max(normRatio, eventRatio)
#        onesMatrix = np.ones(Xraw.shape)
#        minMat = minRatio*onesMatrix
#        maxMat = maxRatio*onesMatrix
#        X = np.minimum(Xraw, maxMat)
#        X = np.maximum(X, minMat)
        startprob = np.array([0.99, 0.01])
        transmat = np.array([[0.9999999995, 0.0000000005], [0.0005, .9995]])
        
        model = hmm.GaussianHMM (2, 'spherical', startprob, transmat)
#        m_means = np.array([[.5], [-.5]])
        m_means = np.array([[normRatio], [eventRatio]])
        m_covars = np.array([[1],[1]])
        #m_covars = np.tile(np.identity(1), (2, 1, 1))
        model.means_ = m_means
        model.covars_ = m_covars
        revX = np.array([X]).transpose()
#        print revX.shape
        hidden_states = model.predict(revX)
        
#        print " ".join(map(str, X))
#        print "".join(map(str, hidden_states))
        return blockFinder(hidden_states)

    def DeletionPredict ( self , hmmFlag = True):
        """Predict location of deletion in reference domain"""
        # NEW STEPS:
        # 1. generate duplication matrix FQFR
        # 2. generate inversion matrix 
        # 3. add together and dump into hmm

        # Steps:
        # 1: get query/ref projection
        # a.) forward orientation
        if self.normDel == None:
            if self.normDup == None:
                if self.FQFT == None:
                    self.FQFT = self.buildSeqPairProjection (self.querySeq, self.refSeq)
                # 2: get ref/ref projection
                if self.FTFT == None:
                    self.FTFT = self.buildSeqPairProjection (self.refSeq, self.refSeq)
        # 3: get normalized projections
                self.normDup = ((1+self.FQFT) / (1+self.FTFT)) -1  #at this point, a normalized projection has been generated
            if self.normInv == None:
                if self.RQFT == None:
                    self.RQFT = self.buildSeqPairProjection (rc(self.querySeq), self.refSeq)
        # b.) reverse orientation
                if self.RTFT == None:
                    self.RTFT = self.buildSeqPairProjection (rc(self.refSeq), self.refSeq)
                self.normInv =  ((1+self.RQFT) / (1+self.RTFT)) -1
            self.normDel = self.normDup + self.normInv #at this point, a normalized projection has been generated

        # 4a: Return event intervals instead of hmm vector, as previously.
        # Deletions are already located in reference domain, no conversion necessary 
        if hmmFlag:
            hidden_states = self.hmmBlock (4 * self.normDel, 0.0, -1.0)
            return blockFinder(hidden_states)                
        # 4b: return QVR and RVR
        else:
            return [self.FQFT , self.FTFT]

    def DuplicationPredict ( self , hmmFlag = True):
        """Predict location of duplication in reference domain"""
        """ Spits out QVR,RVR vector pair"""
        # Steps:
        # 1: get query/ref projection
        # a.) forward orientation
        if self.normDup == None:
            if self.FQFT == None:
                self.FQFT = self.buildSeqPairProjection (self.querySeq, self.refSeq)
        # 2: get ref/ref projection
            if self.FTFT == None:
                self.FTFT = self.buildSeqPairProjection (self.refSeq, self.refSeq)
        # 3: get normalized projections
            self.normDup = ((1+self.FQFT) / (1+self.FTFT)) -1  #at this point, a normalized projection has been generated
        
        # 4a: Compare forward_q_norm to reverse_q_norm, identify window in which the signals alternate
        if hmmFlag:                    
            hidden_states = self.hmmBlock (4.0 * self.normDup , 0.0 , 1.0) 
            return blockFinder(hidden_states)
        else:
            return [self.FQFT , self.FTFT]

    # because InsertedInvertedProximalSubstringPredict is just too long
    def IIPS_Predict ( self , hmmFlag = True): 
        """Predict location of duplication in reference domain"""
        """ Spits out QVR,RVR vector pair"""
        # Steps:
        # 1: get query/ref projection
        # a.) forward orientation
        if self.normDup == None:
            if self.FQFT == None:
                self.FQFT = self.buildSeqPairProjection (self.querySeq, self.refSeq)
        # 2: get ref/ref projection
            if self.FTFT == None:
                self.FTFT = self.buildSeqPairProjection (self.refSeq, self.refSeq)
        # 3: get normalized projections
            self.normDup = ((1+self.FQFT) / (1+self.FTFT)) -1  #at this point, a normalized projection has been generated
        # compute self.normInv
            if self.normInv == None:
                if self.RQFT == None:
                    self.RQFT = self.buildSeqPairProjection (rc(self.querySeq), self.refSeq)
        # b.) reverse orientation
                if self.RTFT == None:
                    self.RTFT = self.buildSeqPairProjection (rc(self.refSeq), self.refSeq)
                self.normInv =  ((1+self.RQFT) / (1+self.RTFT)) -1


        if hmmFlag:                    
            hidden_states = self.hmmBlock (4.0 * self.normDup , 0.0 , 1.0) 
            return blockFinder(hidden_states)
        else:
            return [self.FQFT , self.FTFT]

    def InsertionPredict ( self , hmmFlag = True):
        """Predict location of insertions in query. Unlike other detectors, this uses query sequence domain."""
        # Steps:
        # 1: get query/ref projection
        # a.) forward orientation
        FQFT = self.buildSeqPairProjection (self.refSeq, self.querySeq, projectionAxis=1)
        # 2: get ref/ref projection
        FTFT = self.buildSeqPairProjection (self.refSeq, self.refSeq, projectionAxis=1)
        RQFT = self.buildSeqPairProjection (rc(self.querySeq), self.querySeq, projectionAxis=1)
        RTFT = self.buildSeqPairProjection (rc(self.refSeq), self.refSeq, projectionAxis=1)
        # 3: get normalized projections
        self.normIns =( (( 1 + FQFT) / (1+ FTFT)) - 1/  (( 1 + RQFT) / (1+ RTFT)) - 1)

        # 4a: Get hmm boundaries of insertion
        if hmmFlag:
            hidden_states = self.hmmBlock (4 * self.normIns, 1.0, 0.0)
            queryDomainResults = blockFinder(hidden_states)

        else:
            print >>sys.stderr, "HMM Flag set to false . . .but haven't implemented this yet"
            raise

    def InversionPredict( self , hmmFlag = True ):
        """Predict location of inversion in reference domain"""

        #new steps:
        #1. windowSquared normalized projection on axis=0 of RQFT: self.RQFT
        #2. windowSquared normalized projection on axis=0 of RTFT: self.RTFT
        #3. ((1+INV) / (1+REFrc)) -1 ] {0,1}
        # Steps:
        # 1: get query/ref projection
        # a.) forward orientation
        if self.normInv == None:
            if self.FQFT == None:
                self.FQFT = self.buildSeqPairProjection (self.querySeq, self.refSeq)
        # b.) reverse orientation
            if self.RQFT == None:
                self.RQFT = self.buildSeqPairProjection (rc(self.querySeq), self.refSeq) 
        # 2: get ref/ref projection
            if self.RTFT == None:
                self.RTFT = self.buildSeqPairProjection (rc(self.refSeq), self.refSeq) 
        # 3: get normalized projections
#        normProjectionForward = ((1+self.FQFT) / (1+ self.RTFT) ) -1 
#        normProjectionReverse = ((1+self.RQFT) / (1+self.RTFT)) -1
        Inv1 = ((1+self.RQFT) / (1+self.RTFT)) -1
        
        # 5: Compare forward_q_norm to reverse_q_norm, identify window in which the signals alternate
        
        # 6: return HMM signal vector
        if hmmFlag:
            #for_minus_rev = normProjectionForward - normProjectionReverse
            hidden_states = self.hmmBlock (4.0*Inv1, 0.0, 1.0)
#            hidden_states = self.hmmBlock (4.0*for_minus_rev, 0.0, 1.0)
            return blockFinder(idden_states)
        else:
            print >>sys.stderr, "HMM Flag set to false . . .but haven't implemented this yet"
            raise

    #projected axis option added. 0 = on seq1, 1 = on seq2
    def buildSeqPairProjection (self, seq1, seq2, imagename=None, projectedAxis=0):
        # Steps:
        # 1: get query/ref projection
        #  a.) get dot plot for query vs. reference forward
        forward = trf.CompareSeqPairRef(seq1, seq2, len(seq1), len(seq2), self.window, self.mismatch, self.bandThreshold, self.indelBand)
        #  b.) get dot plot for query vs. reference reverse complement
        reverse = trf.CompareSeqPairRef(rc(seq1), rc(seq2), len(seq1), len(seq2), self.window, self.mismatch, self.bandThreshold, self.indelBand)[::-1,::-1]
        # 2: merge forward and reverse complement by minimum
        #matrix = MinMapThread(for_1_vs_for_2, rc_1_vs_rc_2)
        # 3.) project onto refernce coordinate system (correct for zero values iwth epsilon)
        if imagename == None and (projectedAxis == 1 or projectedAxis == 0):
            return (np.array(np.minimum(forward, reverse).sum(axis=projectedAxis), dtype='f8')+epsilon) / self.windowSquared  
        if imagename != None:
            print >>sys.stderr, "image being printed"
            cdiv = 600
            rlen = len(seq1)#q
            qlen = len(seq2)#r
            cfact = max(int(rlen/cdiv),1)
            matrix1= trf.compressMatrix(np.minimum(forward, reverse),cfact,qlen,rlen)
            plt.imshow(matrix1,cmap = cm.Blues, alpha=0.5)
            matrix2a = trf.CompareSeqPairRef(rc(seq1), seq2, len(seq1), len(seq2),self.window, self.mismatch, self.bandThreshold, self.indelBand)
            matrix2b = trf.CompareSeqPairRef(seq1, rc(seq2), len(seq1), len(seq2),self.window, self.mismatch, self.bandThreshold, self.indelBand)[::-1,::-1]
            matrix2 = trf.compressMatrix(np.minimum(matrix2a,matrix2b)[::-1],cfact,qlen,rlen)
            matrix2 = np.ma.masked_where(matrix2 < self.window, matrix2)
            plt.imshow(matrix1,cmap = cm.Blues)
            plt.imshow(matrix2,cmap = cm.Reds)
            plt.savefig(imagename+".png")

if __name__ == "__main__":
    app = EventDetector(seq1.upper(), seq2.upper())
    #if app.opts.profile:
    #    import cProfile
    #    cProfile.run( 'app.run()', '%s.profile' % sys.argv[0] )
#    app.InversionPredict()
#    app.DeletionPredict(
#    app.InsertionPredict()
    #app.DuplicationPredict()   
    pass
    
 
