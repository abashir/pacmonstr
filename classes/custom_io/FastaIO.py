#!/usr/bin/env python
""" Simple script to partition pacbio barcodes into seperate fasta files """

__all__ = ['FastaStreamReader','FastaReader','FastaEntry','FastqReader','FastqEntry']

import sys, os
import optparse, logging
from itertools import *

def getFileHandle(filenameOrFile, mode):
    """
    Given a filename not ending in ".gz", open the file with the
    appropriate mode.

    Given a filename ending in ".gz", return a filehandle to the
    unzipped stream.

    Given a file object, return it unless the mode is incorrect--in
    that case, raise an exception.
    """
    assert mode in ("r", "w")

    if isinstance(filenameOrFile, str):
        if filenameOrFile.endswith(".gz"):
            return gzip.open(filenameOrFile, mode)
        else:
            return open(filenameOrFile, mode)
    elif isFileLikeObject(filenameOrFile):
        return filenameOrFile
    else:
        raise Exception("Invalid type to getFileHandle")

class FastaStreamReader:
    """Useful for parsing fasta streams as opposed to files"""
    DEFAULT_DELIMITER = '>'
    
    def __init__(self, iterator):
        self.iterator = iterator
        self.delimiter = FastaStreamReader.DEFAULT_DELIMITER

    def setDelimiter(self, delimiter):
        self.delimiter = delimiter

    def __iter__( self ):
        name = ''
        seqBuffer = []
        for line in self.iterator:
            if len(line)<1: continue
            if line[0]=='#' and self.delimiter!='#': continue
            line = line.rstrip()
            if len(line)==0: continue
            if line[0]==self.delimiter:
                if len(seqBuffer)>0:
                    yield FastaEntry( name, "".join(seqBuffer) )
                seqBuffer = []
                if len(line)==1:
                    name = ''
                else:
                    name = line[1:]
            else:
                seqBuffer.append( line )
        if len(seqBuffer)>0:
            yield FastaEntry( name, "".join(seqBuffer) )


class FastaReader:
    
    def __init__( self, fileName ):
        self.fileName = fileName
        if not os.path.exists( self.fileName ):
            sys.stderr.write( "Can't find file %s\n" % fileName )
            raise IOError, "FastaIO: can't find file %s" % fileName
        self.infile = open( self.fileName, 'r' )
        self.streamReader = FastaStreamReader(self.infile)

    def __iter__(self):
        return self.streamReader.__iter__()

    def setDelimiter(self, delimiter):
        self.streamReader.setDelimiter(delimiter)

    def close(self):
        self.infile.close()

class FastaEntry:
    """Storage class for modeling a named sequence stored in a FASTA file.
    Supports 'extended-FASTA' notation for key-value annotations."""
    def __init__( self, name, sequence ):
        self.sequence = sequence
        self.raw_name = name
        self.__processAnnotations( name )

    def getAnnotation(self,key):
        if key in self._annotations:
            return self._annotations[key]
        return None

    def getTag(self):
        if len(self._annotations)==0:
            return self.name
        tag = '%s|%s' % ( self.name, \
            '|'.join(['%s=%s'%(k,v) for k,v in self._annotations.iteritems()] ) )
        return tag

    def __processAnnotations(self, tag):
        """Processes extended syntax for entry names of the form
        >readName|key1=value1|key2=value2|..."""
        self._annotations = {}
        if tag.find('|')<0:
            self.name = tag
            return
        pairs = tag.split( '|' )
        self.name = pairs[0]
        for pair in pairs[1:]:
            if '=' not in pair:
                self.name = '%s|%s' % ( self.name, pair )
                continue
            values = pair.split('=')
            if len(values)==2:
                self._annotations[ values[0] ] = values[1]
        # revert to traditional model if this tag doesn't have kv pairs
        if len(self._annotations)==0:
            self.name = tag

    def __str__( self ):
        buffer = []
        buffer.append( ">" + self.getTag() )
        buffer.append( str(self.sequence) )
        return os.linesep.join(buffer)
    
    def subseq(self, seqRange, name=None):
        if not name:
            name = "%s_%i_%i" % (self.name, seqRange.getStart(), seqRange.getEnd())
        return FastaEntry(name, self.sequence[seqRange.getStart():seqRange.getEnd()] )

class FastqReader:
    """
    Parser for FASTQ input from a filename or file-like object
    """
    def __init__(self, f):
        self.file = getFileHandle(f, "r")

    def __iter__( self ):
        qualFlag = False
        seqBuffer = []
        qualBuffer = []
        name = ""
        for line in self.file:
            line = line.rstrip()
            if len(line)==0: continue
            if line[0]=="@" and len(seqBuffer) == len(qualBuffer):
                if len(seqBuffer)>0:
                    yield FastqEntry( name, "".join(seqBuffer), "".join(qualBuffer) )
                qualFlag = False
                seqBuffer = []
                qualBuffer = []
                if len(line)==1:
                    name = ''
                else:
                    name = line[1:]
            elif (line[0] == "+" and len(qualBuffer) == 0 and not qualFlag):
                qualFlag = True
            else:
                if (qualFlag):
                    qualBuffer.append( line )
                else:
                    seqBuffer.append( line )
        if len(seqBuffer)>0:
            yield FastqEntry( name, "".join(seqBuffer), "".join(qualBuffer) )


class FastqEntry:
    def __init__( self, name, sequence, quality ):
        self.name = name
        self.sequence = sequence
        self.quality = quality

    def __str__( self ):
        buffer = []
        buffer.append( "@" + self.name )
        buffer.append( self.sequence )
        buffer.append( "+" + self.name )
        buffer.append( self.quality )
        return os.linesep.join(buffer)

    @property
    def asciiQvs(self):
        assert self.quality.dtype == np.uint8
        return np.minimum(33 + self.quality, 126).tostring()

    def subseq(self, seqRange, name=None):
        if not name:
            name = "%s_%i_%i" % (self.name, seqRange.start, seqRange.end)
        return FastqEntry(name, self.sequence[seqRange.start : seqRange.end],
                          self.quality[seqRange.start : seqRange.end])
    def trim(self, trim):
        return self.subseq(Range(trim, len(self.sequence) - trim))

