#!/usr/bin/env python -o

#PEE_pipeline.y 
'''a wrapper for the ProjectEventExact.py script so that it can be
serially run on sequences. '''

import os , re , time, sys
sys.path.append("..")
from ProjectEventExact import EventDetector as ed
import numpy as np
from multiprocessing import Pool
import pysam
from pbcore.io.FastaIO import FastaReader
from pacmonstr_cython.eventFilter import eventFilter

#import pacmonstr_cython.trFinderFuncs as trf

#chromosomes = sys.argv[1].split(",")
stepsize = int(sys.argv[1])
chromlist = sys.argv[2].split(",")
fafn = sys.argv[3]
threads = int(sys.argv[4])
bamIn = sys.argv[5]

samfile = pysam.Samfile( bamIn, "rb" )
fastaDict = {}
for entry in FastaReader(fafn):
    ename = entry.name.split()[0]
    if not ename in chromlist and chromlist[0] != "all":
        continue
    print >>sys.stderr, entry.name
    #refseq = entry.sequence
    fastaDict[ename] = entry.sequence.upper()
sys.stderr.flush()

def ror_helper (ar, qseq, rseq, tstart):
    aligned_pairs = ar.aligned_pairs
            # look at first aligned base
    qarray = np.array(map(lambda x: -1 if x[0] == None else x[0], aligned_pairs), dtype="i8")
    rarray = np.array(map(lambda x: -1 if x[1] == None else int(x[1])-tstart, aligned_pairs), dtype="i8")
    mismatchwindow = eventFilter (qseq, rseq, qarray, rarray, len(aligned_pairs))
    print >>sys.stderr, mismatchwindow, len(aligned_pairs)
    return mismatchwindow

def runOnRegion(region_str):
    global samfile
    global fastaDict
    #results = [np.arange(size,dtype=np.int),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16)]
    insertionList = []
    print >>sys.stderr, "working on %s" %(region_str)
    chrom, start, end = region_str.split("_")
    refseq = fastaDict[chrom]
    start, end = int(start), int(end)
    subsize = end-start + 50000
    results = [np.arange(subsize,dtype=np.int),np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16),np.zeros(subsize,dtype=np.uint16)]
    try:
        bamIter = samfile.fetch(chrom, start, end)
    except:
        print >>sys.stderr, "%s does not seem to exit in bam" %(region_str)
        return
    readcounter = 0
    seenFlag = False
    for ar in bamIter: 
        if not ar.is_secondary:
            if ar.aend-ar.alen < start:
                continue
            readcounter += 1
            #print >>sys.stderr, region_str, ar.qname, readcounter
            qstart, qend = ar.qstart, ar.qend
            tstart, tend = ar.aend-ar.alen, ar.aend
            results[5][tstart-start:tend-start] += 1
            fullqseq = ar.seq
            ### MP - these are the substrings being matched
            qseq = ar.seq[qstart:qend]
            rseq = refseq[tstart:tend]
            # get list of aligned postions
            # MATT CYTHON THIS STUFF!
   
            mismatchwindow = ror_helper(ar, qseq, rseq, tstart)
            if mismatchwindow < 100 :
                continue

            print >>sys.stderr, region_str, ar.qname, readcounter
            a = ed(qseq,rseq,window=8)

            DUP = a.Dup_hidden_states
            INV = a.Inv_hidden_states
            DEL = a.Del_hidden_states
            DUPINV = a.DupInv_hidden_states
            for event in a.Ins_events:
                insertionList.append([ar.qname,event[0],event[1]])
                seenFlag = True
            rlen = len(rseq)
            results[1][tstart-start:tstart+rlen-start] += DUP
            results[2][tstart-start:tstart+rlen-start] += INV
            results[3][tstart-start:tstart+rlen-start] += DEL
            results[4][tstart-start:tstart+rlen-start] += DUPINV
                #results[5][tstart+pos-start] += 1
            seenFlag = True
    if not seenFlag:
        return
    print >>sys.stderr, "writing results list for %s" %(region_str)
    sys.stderr.flush()
    outFile = open("%s_results.txt" %(region_str), 'w')    
    for pos in range (subsize):
        dup, dupinv, inv, delete, cov = 0, 0, 0, 0, 0
        if pos % 1000000 == 0:
            print >>sys.stderr, pos
            sys.stderr.flush()
        dup     = results[1][[pos]]
        inv     = results[2][[pos]]
        delete  = results[3][[pos]]
        dupinv  = results[4][[pos]]
        cov     = results[5][[pos]]
        if sum([dup, inv, delete, dupinv]) > 0:
            #print >>sys.stderr, "pos %i has hit" %(pos)
            outseq = '%s %i %i %i %i %i %i\n' % (chrom,pos+start,dup, inv, delete, dupinv, cov)
            outFile.write(outseq)
            outFile.flush()
    
    outFile.close()    
    insertions_outfile = open('%s_insertions.txt' % (region_str), 'w')
    for event in insertionList:
        insertions_outfile.write('%s %i %i\n' % tuple( event))
    insertions_outfile.close()
    return 
    #return results

def buildRegionStrs (fastaDict, threads, step):
    region_strs = []
    for fname, fseq in fastaDict.items():
        if "_" in fname:
            continue
        fsize = len(fseq)
        #step = fsize / max(1, (threads - 1))        
        steps = fsize/step + 1
        for i in range(steps):
            region_strs.append("%s_%i_%i" %(fname, i*step, (i+1)*step-1))
    print >>sys.stderr, region_strs
    return region_strs

def printResultsList (regionstrs):
    chrs = set()
    for region_str in regionstrs:
        chrs.add(region_str.split("_")[0])
    for chrom in chrs:
        os.system("cat %s_*results.txt | sort -nk2 > %s_mergedresults.sorted.txt" %(chrom, chrom))
        os.system("cat %s*insertions.txt | sort -k1 > %s_mergedinsertions.sorted.txt" %(chrom, chrom))        
        os.system("rm %s*results.txt %s*insertions.txt" %(chrom, chrom))

#region_strs = buildRegionStrs(fastaDict, threads, stepsize)
#myPool = Pool(threads)
#resultsList = myPool.map(runOnRegion, region_strs)

for region_str in region_strs:
    runOnRegion(region_str)
printResultsList (region_strs)

#output = runOnRegion("chr22_19999000_20000000")
#resultsList = [output]
#resultsMerged = [np.arange(size,dtype=np.int),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16),np.zeros(size,dtype=np.uint16)]
'''
outFile = sys.stdout

print >>sys.stderr, "writing mergedlist"
sys.stderr.flush()

#for pos in range (19000000, size):
for pos in range (size):
    dup, dupinv, inv, delete, cov = 0, 0, 0, 0, 0
    if pos % 1000000 == 0:
        print >>sys.stderr, pos
        sys.stderr.flush()
    for i in range(len(resultsList)):
        result = resultsList[i]
        dup     += result[1][[pos]]
        inv     += result[2][[pos]]
        delete  += result[3][[pos]]
        dupinv  += result[4][[pos]]
        cov     += result[5][[pos]]
    if sum([dup, inv, delete, cov]) > 0:
        #print >>sys.stderr, "pos %i has hit" %(pos)
        outseq = '%s %i %i %i %i %i %i\n' % (chromosome,pos,dup, inv, delete, dupinv, cov)
        outFile.write(outseq)
        outFile.flush()

outFile.close()
'''
