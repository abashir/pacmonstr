#!/usr/bin/env python -o

#PEG_pipeline.y HMM version
'''a wrapper for the PE.py script so that it can be
serially run on sequences. Because this script isn't compatible
with a module import, it has to be run instead using the
subprocess module, then the resulting stdout string must
be parsed into a list object.'''

import os , re ,time
import classes.ProjectEventGeneral as PEG
import sys,subprocess, ast, shlex, re , os
from model import Sequence as seq
import numpy as np

chromosome = str(sys.argv[1])
outFolder = sys.argv[2]
m5In = open(sys.argv[3])

#outFolder = '/scratch/pendlm02/temp/'
#outFolder = '/sc/orga/scratch/pendlm02/na12878/chr1/reads/'
size = 255000000
data = [np.arange(size),np.zeros(size),np.zeros(size)]
touched = np.zeros(size,dtype = int)

spaces = re.compile(r'\s+')
m5Fields = ['qname','qseqlength','qstart','qend','qstrand','tname','tseqlength','tstart','tend','tstrand','score','nMatch','nMismatch','nIns','nDel','MapQV','seq1','bars','seq2']
start = time.time()
timeRunningOut = 0

for line in m5In:
    lineSplit = line.split()
    entry = dict(zip(m5Fields,lineSplit))
    if timeRunningOut == 1:
        print >> sys.stderr,entry['qname']
        continue
    if len(entry) == 19 and not os.path.exists('%s%s' % (outFolder, sys.argv[3].split('/')[-1].replace('.m5','.dat'))): #don't reprocess sequences that have already been done in case of failure.
        entry['qseqlength'] = int(entry['qseqlength'])
        entry['qstart'] = int(entry['qstart'])
        entry['qend'] = int(entry['qend'])
        entry['tstart'] =int(entry['tstart'])
        entry['tend'] = int(entry['tend'])
        strippedQuery = entry['seq1'].replace('-','')
        strippedRef = entry['seq2'].replace('-','')
	if entry['tstrand'] == '-':
            strippedQuery = seq.revcomp(strippedQuery)
            strippedRef = seq.revcomp(strippedRef)
#        if len(strippedQuery) < 4000:
#            continue #read length cutoff
        PE = PEG.EventDetector(strippedQuery,strippedRef, window=8 , mismatch = 1 )
        results = PE.DuplicationPredict()
        for pos in xrange(len(strippedRef)):
            data[1][entry['tstart']+pos] = results[pos] #HMM predicted duplication state
            data[2][entry['tstart']+pos] += 1 #coverage
            touched[pos+entry['tstart']+1] += 1
        if time.time() - start > 35700: #if running out of time, give 5 minutes to write remaining sequence names to stderr and to write results to file
            print >> sys.stdout, 'running out of time'
            timeRunningOut = 1

#print '%s%s' % (outFolder, sys.argv[3].split('/')[-1].replace('.m5','.dat'))

outFile = open('%s%s' % (outFolder, sys.argv[3].split('/')[-1].replace('.m5','.dat')),'w')
basesList = []

touchedList = np.nonzero(touched > 0)[0]

for pos in touchedList:
    outFile.write('%s:%i %i %i\n' % (chromosome,pos,data[1][pos],data[2][pos]))

outFile.close()

