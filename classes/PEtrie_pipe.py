#!/usr/bin/env python -o

'''a wrapper for the PE.py script so that it can be
serially run on sequences. Because this script isn't compatible
with a module import, it has to be run instead using the
subprocess module, then the resulting stdout string must
be parsed into a list object.'''

import sys,subprocess, ast, shlex

m5In = open(sys.argv[1])


'''the m5 will be parsed into both query and reference sequences as well as a reference indexed positions.'''

command = 'python ~/git_repos/pacmonstr/classes/PE.py %s  %s -w 10 -m 2' % (query, target)

cmd = shlex.split(command)

out,err = subprocess.Popen(cmd,stdout=PIPE, stderr=PIPE).communicate()

#results = [QVR,RVR] to be mapped to positions according to those listed in the m5 above
results = ast.literal_eval(out.replace('[ ','[').replace('\n','').replace('  ',' ').replace(' ',','))


